# Note

1. 将AuthContext变量import进来(再使用useContext(AuthContext))就可以使用当前用户信息，包括用户信息以及用户是否登录(以此来根据用户权限进行条件渲染)
2. 登录注册之后都需要使用loadUser(token)函数来加载当前用户信息,token为返回的用户令牌
