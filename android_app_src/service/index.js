import shopService from "./shop.service";
import authService from "./auth.service";
import userService from "./user.service";
import spotService from "./spot.service";
import adminService from './admin.service';
import spotAdminService from './spotAdmin.service';
import shopAdminService from './shopAdmin.service';
import travelService from './travel.service'


export {
  authService,
  shopService,
  userService,
  spotService,
  adminService,
  spotAdminService,
  shopAdminService,
  travelService,
}
