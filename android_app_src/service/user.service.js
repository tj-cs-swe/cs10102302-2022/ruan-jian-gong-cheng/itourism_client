import http from "./request";

/**
 * @description 修改密码
 * @param data old_password: 旧密码 new_password: 新密码 check_password: 确认新密码
 * @returns Promise res.content: ture
 */
const changePassword = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/mine/change-password', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 编辑信息
 * @param data age: 年龄 gender: 性别
 * @returns Promise res.content:用户的个人信息
 */
const editProfile = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/mine/edit', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取我的订单
 * @param {number} page 请求的页数
 * @param {number} size 该页的大小
 * @returns 
 */
const getMyOrder = (page, size) => {
  return new Promise((resolve, reject) => {
    http.get(`/mine/goods/orders/${page}/${size}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取我的票
 * @param {number} page 请求的页数
 * @param {number} size 该页的大小
 * @returns 
 */
const getMyTicket = (page, size) => {
  return new Promise((resolve, reject) => {
    http.get(`/mine/tickets/orders/${page}/${size}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 用户上传头像
 * @param {object} data base64: 图片的base64编码
 * @returns
 */
const uploadAvatar = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/mine/avatar/upload', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 用户退票
 * @param {number} id 票务id
 * @returns
 */
 const returnTicket = (id) => {
  return new Promise((resolve, reject) => {
    http.get(`/spot/return_ticket/${id}`, id).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 用户完成支付
 * @param {number} id 订单id
 * @returns
 */
 const payOrder = (id) => {
  return new Promise((resolve, reject) => {
    http.get(`/shop/pay/${id}`, id).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 用户完成订单
 * @param {number} id 订单id
 * @returns
 */
 const finishOrder = (id) => {
  return new Promise((resolve, reject) => {
    http.get(`/shop/finish/${id}`, id).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 用户退货
 * @param {number} id 订单id
 * @returns
 */
 const refund = (id) => {
  return new Promise((resolve, reject) => {
    http.get(`/shop/refund/${id}`, id).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

const userService = {
  editProfile,
  changePassword,
  getMyOrder,
  getMyTicket,
  uploadAvatar,
  returnTicket,
  payOrder,
  finishOrder,
  refund
}

export default userService;