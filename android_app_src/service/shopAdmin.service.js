import http from './request'

/**
 * @description 商家编辑商铺信息
 * @param {object} data  apifox
 * @returns 
 */
 const editShop = (data) => {
  return new Promise((resolve, reject) => {
    http.post(`/shop/edit`, data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 商家向店铺添加商品
 * @param {object} data apifox
 * @returns 
 */
const addItem = (data) => {
  return new Promise((resolve, reject) => {
    http.post(`/shop/goods/add`, data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 商家编辑商品信息
 * @param {object} data  apifox
 * @returns 
 */
const editItem = (data) => {
  return new Promise((resolve, reject) => {
    http.post(`/shop/goods/edit`, data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 商家删除商品
 * @param {number} id 商品id 
 * @returns 
 */
const deleteItem = (id) => {
  return new Promise((resolve, reject) => {
    http.delete(`/shop/goods/delete/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取店铺订单列表
 * @param {number} page 页面
 * @param {number} size 大小
 * @param {number} id 店铺id
 * @returns 
 */
const getOrderList = (page, size, id) => {
  return new Promise((resolve, reject) => {
    http.get(``).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取订单详情
 * @param {number} id 订单id
 * @returns 
 */
const getOrderInfo = (id) => {
  return new Promise((resolve, reject) => {
    http.get(``).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 商家订单发货
 * @param {number} id 订单id
 * @returns 
 */
const deliverOrder = (id) => {
  return new Promise((resolve, reject) => {
    http.post(`business/order/deliver/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

const shopAdminService = {
  editShop,
  addItem,
  editItem,
  deleteItem,
  deliverOrder,
  getOrderList,
  getOrderInfo
}

export default shopAdminService;