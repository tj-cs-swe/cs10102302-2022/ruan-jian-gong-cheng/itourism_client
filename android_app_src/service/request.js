import axios from "axios";

const http = axios.create({
  baseURL: 'http://124.71.194.255:8086',
  timeout: 1000
})

http.interceptors.request.use((config) => {
  // console.log('[request url]:', config.baseURL + config.url);
  return config;
}, (err) => {
  // console.log('[request error]:', err);
  return Promise.reject(err);
});

http.interceptors.response.use((res) => {
  // console.log('[response data]:');
  // for(let a in res.data){
  //   console.log(`${a}:type(${typeof res.data[a]})`);
  // }
  return res.data;
}, (err) => {
  console.log('[response error]:', err);
  return Promise.reject(err.response.data);
})

export default http;