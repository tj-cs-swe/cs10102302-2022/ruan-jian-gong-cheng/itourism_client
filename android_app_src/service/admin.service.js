import http from './request'

/**
 * @description 系统管理员添加景区信息
 * @param data name:景区名称  figure:景区图片  introduction:景区介绍 
 * @returns 
 */
const addSpot = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/admin/spot/add', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 管理员删除景区
 * @param id 景区id 
 * @returns 
 */
const deleteSpot = (id) => {
  return new Promise((resolve, reject) => {
    http.delete(`/admin/spot/delete/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 系统管理员获取景区列表
 * @param {number} page 页
 * @param {number} size 大小
 * @returns 
 */
const getSpotList = (page, size) => {
  return new Promise((resolve, reject) => {
    http.get(`/spot/list/${page}/${size}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * 
 * @param {number} page  页
 * @param {number} size  大小
 * @returns 
 */
const getUserList = (page, size) => {
  return new Promise((resolve, reject) => {
    http.get(`/admin/permission/list/${page}/${size}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * 
 * @param {object} data  user_id: 用户id permissions_id 权限id manage_id 待分配id
 * @returns 
 */
const permissionUser = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/admin/permission/add', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

const adminService = {
  addSpot,
  deleteSpot,
  getSpotList,
  getUserList,
  permissionUser
}

export default adminService;