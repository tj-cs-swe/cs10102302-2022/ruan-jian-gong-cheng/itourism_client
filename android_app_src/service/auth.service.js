import http from "./request";

/**
 * @param data = {
 *    username: ,
 *    password: ,
 *    email: , 
 *    captcha: ,
 * }
 * @returns Promise
 */
const signUp = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/auth/register', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err)
    })
  })
}

/**
 * @param data = {
 *    username: ,
 *    password: ,
 *    key: ,
 *    captcha: ,
 * }
 * @returns Promise
 */
const signIn = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/auth/login', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}
/**
 * @returns Promise 
 */
const getVerifyImg = () => {
  return new Promise((resolve, reject) => {
    http.get('/auth/captcha').then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * 
 * @param data = {
 *   email: , 邮箱
 * }
 * @returns 
 */
const getVerifyCode = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/auth/email', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * 
 * @param 
 * token(string): 用户登录之后服务器返回的token
 * @returns res.content: 用户信息（查看apifox）
 */
const loadUserInfo = (token) => {
  console.log(token);
  http.defaults.headers.common['Authorization'] = token;
  return new Promise((resolve, reject) => {
    http.get('/mine').then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

const signOut = () => {
  http.defaults.headers.common['Authorization'] = null;
}

/**
 * 
 * @param 
 * data(object):{
 *  email: 
 *  captcha:
 *  password:
 * }
 * @returns: Promise res.content:ture 
 */
const forgetPassword = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/auth/forget', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

const authService = {
  signUp,
  signIn,
  signOut,
  getVerifyImg,
  getVerifyCode,
  loadUserInfo,
  forgetPassword
}

export default authService;