import http from './request'

/**
 * @description 获取景点的音频
 * @param {number} id 音频id
 * @returns 
 */
const getAudio = (id) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/intro_audio/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取景点的音频列表
 * @param {number} id 景点id
 * @param {number} page 页面
 * @param {number} size 大小
 * @returns 
 */
const getAudioList = (id, page, size) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/intro_audio/list/${id}/${page}/${size}`).then(res => {
      resolve(res);
    }).catch(err => {
      resolve(err);
    })
  })
}

/**
 * @description 用户添加评论
 * @param {object} data 评论详情
 * @returns 
 */
const addComment = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/travel/comment/add', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 用户给评论点赞
 * @param {number} id 评论id
 * @returns 
 */
const likeComment = (id) => {
  return new Promise((resolve, reject) => {
    http.post(`/travel/comment/like/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 用户给评论点踩
 * @param {number} id 评论id
 * @returns 
 */
const dislikeComment = (id) => {
  return new Promise((resolve, reject) => {
    http.post(`/travel/comment/dislike/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 用户取消评论点赞
 * @param {number} id 评论id
 * @returns 
 */
const cancelLikeComment = (id) => {
  return new Promise((resolve, reject) => {
    http.post(`/travel/comment/undo-like/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 用户取消评论点踩
 * @param {number} id 评论id
 * @returns 
 */
const cancelDislikeComment = (id) => {
  return new Promise((resolve, reject) => {
    http.post(`/travel/comment/undo-dislike/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 用户删除自己的评论
 * @param {number} id 评论id
 * @returns 
 */
const deleteComment = (id) => {
  return new Promise((resolve, reject) => {
    http.delete(`/travel/comment/delete/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 用户评分
 * @param {number} id 评论id
 * @param {number} score 评分
 * @returns 
 */
const spotScore = (id, score) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/score/${id}/${score}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 用户上传文件
 * @param {object} data 文件数据
 * @returns 
 */
const uploadFile = (data) => {
  return new Promise((resolve, reject) => {
    http.post(`/travel/upload/image`, data, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      transformRequest: [function (data) {
        return data
      }]
    }).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取景点服务点列表
 * @param {number} page 页面
 * @param {number} size 大小
 * @returns 
 */
const getServicePointList = (spotId, page, size) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/service_point/list/${spotId}/${page}/${size}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取景点服务点详细信息
 * @param {number} servicePointId 景点服务点id
 * @returns 
 */
const getServicePointInfo = (servicePointId) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/service_point/${servicePointId}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取景点评论列表
 * @param {number} id 景点id
 * @param {number} page 页面
 * @param {number} size 大小
 * @returns 
 */
const getCommentList = (id, page, size) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/comment/${id}/${page}/${size}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取评论详情
 * @param {number} id 评论id
 * @returns 
 */
const getCommentInfo = (id) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/comment/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取景点游览路线列表
 * @param {number} id 景点id
 * @param {number} page 页面
 * @param {number} size 大小
 * @returns 
 */
const getRouteList = (id, page, size) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/tour_route/list/${id}/${page}/${size}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取游览路线信息
 * @param {number} id 
 * @returns 
 */
const getRouteInfo = (id) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/tour_route/get/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 下载音频
 * @param {number} id 音频id
 * @returns 
 */
const downloadAudio = (id) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/intro_audio/download/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取静态图片
 * @param {string} path 图片路径
 * @returns 
 */
const getImage = (path) => {
  return new Promise((resolve, reject) => {
    http.get(`/static/image/${path}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取景区所有景点
 * @param {number} spotId 景区id
 * @returns 
 */
const getServicePointAll = (spotId) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/service_point/all/${spotId}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

const travelService = {
  addComment,
  uploadFile,
  likeComment,
  cancelLikeComment,
  dislikeComment,
  cancelDislikeComment,
  deleteComment,
  getAudioList,
  getAudio,
  getServicePointList,
  getServicePointInfo,
  getCommentInfo,
  getCommentList,
  getRouteList,
  getRouteInfo,
  downloadAudio,
  getImage,
  getServicePointAll,
  spotScore
}

export default travelService;