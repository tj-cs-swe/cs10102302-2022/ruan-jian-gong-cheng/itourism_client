import http from "./request";

/**
 * @description 获取景区所有的商铺
 * @param id 景区的id 
 * @param page 页面
 * @param size 该页的大小
 * @returns Promise res 景区商铺的列表
 */
const getSceneShops = (id, page, size) => {
  return new Promise((resolve, reject) => {
    http.get(`/shop/scene/${id}/${page}/${size}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取商铺信息
 * @param {number} id 商铺的id
 * @returns 
 */
const getShop = (id) => {
  return new Promise((resolve, reject) => {
    http.get(`/shop/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取商铺所有的商品
 * @param id 商铺的id 
 * @param page 页面
 * @param size 该页的大小
 * @returns Promise res 商铺商品的列表
 */
const getShopItems = (id, page, size) => {
  return new Promise((resolve, reject) => {
    http.get(`/shop/store/${id}/${page}/${size}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取商品详细信息
 * @param {number} id 商品id
 * @returns 
 */
const getItems = (id) => {
  return new Promise((resolve, reject) => {
    http.get(`/shop/item/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 购买商品
 * @param {object} data 商品的id，商品的数量amount
 * @returns 
 */
const buyItems = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/shop/buy', data).then(res => {
      resolve(res);
    }).catch((err) => {
      reject(err);
    })
  })
}

const shopService = {
  getSceneShops,
  getShopItems,
  buyItems,
  getItems,
  getShop
}

export default shopService;