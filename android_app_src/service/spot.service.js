import http from './request'


/**
 * @description 购买景区的票
 * @param {object} data spotId: 景区id amount: 票的数量 idCard: 身份证 useTime: 预定时间 
 * @returns 
 */
const buyTicket = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/spot/buy', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取景区信息
 * @param id 景区ID 
 * @returns Promise res 景区信息
 */
const getSpotInfo = (id) => {
  return new Promise((resolve, reject) => {
    http.get(`/spot/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

const spotService = {
  buyTicket,
  getSpotInfo
}



export default spotService;