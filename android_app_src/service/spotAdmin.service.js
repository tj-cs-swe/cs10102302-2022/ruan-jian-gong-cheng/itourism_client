import http from './request'

/**
 * @description 景区管理员修改景区信息
 * @param {object} data id: 景区id name:景区名字 introduction: 景区介绍
 * figure 景区图片 leftticket 景区门票 ticketProce 景区票价
 * @returns 
 */
const editSpot = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/spot_admin/spot/edit', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 景区管理员添加景点服务点
 * @param {object} data 
 * @returns 
 */
const addServicePoint = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/spot_admin/service_point/add', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 景区管理员编辑景点服务点
 * @param {object} data 
 * @returns 
 */
const editServicePoint = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/spot_admin/service_point/edit', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 景区管理员删除景点服务点
 * @param {number} id 
 * @returns 
 */
const deleteServicePoint = (id) => {
  return new Promise((resolve, reject) => {
    http.delete(`/spot_admin/service_point/delete/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 添加商铺
 * @param {object} data 
 * @returns 
 */
const addShop = (data) => {
  return new Promise((resolve, reject) => {
    http.post('/spot_admin/shop/add', data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 删除商铺
 * @param {number} id 
 * @returns 
 */
const deleteShop = (id) => {
  return new Promise((resolve, reject) => {
    http.delete(`/spot_admin/shop/delete/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 删除评论
 * @param {number} id 评论id 
 * @returns 
 */
const deleteComment = (id) => {
  return new Promise((resolve, reject) => {
    http.delete(`/spot_admin/comment/delete/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 审核通过评论
 * @param {number} id 评论id
 * @returns 
 */
const agreeComment = (id) => {
  return new Promise((resolve, reject) => {
    http.get(`/spot_admin/comment/agree/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 置顶评论
 * @param {number} id 评论id
 * @returns 
 */
const upComment = (id) => {
  return new Promise((resolve, reject) => {
    http.post(`/spot_admin/comment/up/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 修改音频信息
 * @param {object} data 音频信息
 * @returns 
 */
const editAudio = (data) => {
  return new Promise((resolve, reject) => {
    http.post(`/spot_admin/intro_audio/edit`, data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 添加音频信息
 * @param {object} data 音频信息
 * @returns 
 */
const addAudio = (data) => {
  return new Promise((resolve, reject) => {
    http.post(`/spot_admin/intro_audio/add`, data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 删除音频
 * @param {number} id 音频id
 * @returns 
 */
const deleteAudio = (id) => {
  return new Promise((resolve, reject) => {
    http.delete(`/spot_admin/intro_audio/delete/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 上传音频数据
 * @param {object} data 音频文件数据
 * @returns 
 */
 const uploadAudio = (data) => {
  return new Promise((resolve, reject) => {
    http.post(`/spot_admin/upload`, data, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      transformRequest: [function (data) {
        return data
      }]
    }).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 添加游览路线
 * @param {object} data 游览路线
 * @returns 
 */
const addRoute = (data) => {
  return new Promise((resolve, reject) => {
    http.post(`/spot_admin/tour_route/add/`, data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 编辑游览路线
 * @param {object} data 
 * @returns 
 */
const editRoute = (data) => {
  return new Promise((resolve, reject) => {
    http.post(`/spot_admin/tour_route/edit`, data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 删除游览路线
 * @param {number} id 游览路线id
 * @returns 
 */
const deleteRoute = (id) => {
  return new Promise((resolve, reject) => {
    http.delete(`/spot_admin/tour_route/delete/${id}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

/**
 * @description 获取景点服务点列表
 * @param {number} page 页面
 * @param {number} size 大小
 * @returns 
 */
 const getServicePointList = (spotId, page, size) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/service_point/list/${spotId}/${page}/${size}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
 }

 /**
 * @description 获取景点服务点详细信息
 * @param {number} servicePointId 景点服务点id
 * @returns 
 */
const getServicePointInfo = (servicePointId) => {
  return new Promise((resolve, reject) => {
    http.get(`/travel/service_point/${servicePointId}`).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}


const spotAdminService = {
  editSpot,
  addShop,
  deleteShop,
  addServicePoint,
  editServicePoint,
  deleteServicePoint,
  upComment,
  agreeComment,
  deleteComment,
  editAudio,
  addAudio,
  deleteAudio,
  uploadAudio,
  addRoute,
  editRoute,
  deleteRoute,
  getServicePointList,
  getServicePointInfo
}

export default spotAdminService;