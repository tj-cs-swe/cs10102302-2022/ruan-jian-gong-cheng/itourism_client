import React from 'react'
import { createContext, useState, useEffect } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage';
import authService from '../service/auth.service';

const AuthContext = createContext();

function AuthContextProvider(props) {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);
  const [spotid, setSpotid] = useState(0);
  const [isExist, setIsExist] = useState(true);
  // load user and set user info
  const loadUser = async (token) => {
    await AsyncStorage.setItem('token', token);
    authService.loadUserInfo(token).then(res => {
      setUser(res.content);
    }).catch(async (err) => {
      setUser(null);
      await AsyncStorage.removeItem('token');
    })
  }

  // update user info
  const updateUser = (user) => {
    setUser(user);
  }

  const signOutUser = async () => {
    setUser(null);
    await AsyncStorage.removeItem('token');
    authService.signOut();
  }

  const updateSpotid = (id) => {
    setSpotid(id);
  }

  const updateIsExist = (ie) => {
    setIsExist(ie);
  }

  // get token and load user info at first
  useEffect(() => {
    async function loadLocalUser() {
      // get token from local
      const token = await AsyncStorage.getItem('token');
      setLoading(true);
      if (token) {
        authService.loadUserInfo(token).then(res => {
          setUser(res.content);
          setLoading(false);
        }).catch(async (err) => {
          setUser(null);
          setLoading(false);
          await AsyncStorage.removeItem('token');
        })
      } else {
        setLoading(false);
      }
    }
    loadLocalUser();
  }, [])

  return (
    <AuthContext.Provider
      value={{
        user,
        loading,
        spotid,
        isExist,
        loadUser,
        updateUser,
        signOutUser,
        updateSpotid,
        updateIsExist
      }}
    >
      {props.children}
    </AuthContext.Provider>
  )
}

export { AuthContext, AuthContextProvider }