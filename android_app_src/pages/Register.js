import React, { Component, useState } from 'react'
import { NativeBaseProvider, Text, Box, Input, Icon, Button, Pressable } from 'native-base'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Alert } from 'react-native'
import authService from '../service/auth.service'
import { AuthContext } from '../context/auth.context'

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //基本信息
      username: '',
      password: '',
      confirmPassword: '',
      email: '',
      identifyCode: '',
      isSent: false, //是否已发送验证码
      sentTime: 60,  //发送验证码间隔
      showPwd: false, //控制密码显示方式
      showCfPwd: false,
      passwordMsg: '',
      cfpasswordMsg: ''
    }
  }

  //邮箱格式正则表达式
  emailCode = /^([a-zA-Z\d])(\w|\-)+@[a-zA-Z\d]+(\.[a-zA-Z]{2,4}){1,2}$/;
  //密码强度正则表达式
  //弱密码：8-12个字符，至少1个大写字母，1个小写字母和1个数字，不包括其他字符
  lowPassword = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,12}$/;
  //中密码：8-12个字符，包含至少一个特殊字符
  midPassword = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,12}$/;
  //强密码：13-16个字符，包含至少一个特殊字符
  highPassword = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{13,16}$/;
  //密码输入提示
  helpPassword = '密码不得少于8位，且需包含大写字母、小写字母和数字';
  levelPassword = [this.helpPassword, '弱密码', '中密码', '强密码'];
  errorPassword = '不得包含非法字符！'
  diffPassword = '两次密码不一致！'

  static contextType = AuthContext;

  //点击空白失去焦点
  blurText = () => {
    this.refs.username.blur();
    this.refs.password.blur();
    this.refs.confirmPassword.blur();
    this.refs.email.blur();
    this.refs.identifyCode.blur();
  }

  //切换密码显示方式
  setShow = (choice) => {
    if (choice == 'pwd')
      this.setState({ showPwd: !this.state.showPwd });
    else
      this.setState({ showCfPwd: !this.state.showCfPwd });
  }

  //已发送验证码
  setIsSent = () => {
    if (this.emailCode.test(this.state.email)) {
      this.setState({ isSent: true });

      //显示倒计时
      let id = setInterval(
        () => {
          //获取原状态
          let sentTime = this.state.sentTime;
          //显示减一秒
          sentTime -= 1;
          if (sentTime <= 0) {  //重置计时
            sentTime = 60;
            this.setState({ isSent: false });
            //跳出循环
            clearInterval(id);
          }
          this.setState({ sentTime })
        }, 1000 //每隔一秒
      );

      const data = {
        email: this.state.email
      }
      authService.getVerifyCode(data).then(res => {
        Alert.alert('发送成功', '已发送验证码！')
      }).catch(err => {
        Alert.alert('发送失败', '请重新再试！')
      })
    } else {
      Alert.alert('发送失败', '邮箱格式有误！')
    }
  }

  getUsername = (text) => {
    this.setState({ username: text })
  }
  getPassword = (text) => {
    if (this.lowPassword.test(text)) {
      this.state.passwordMsg = this.levelPassword[1];
    }
    else if (this.midPassword.test(text)) {
      this.state.passwordMsg = this.levelPassword[2];
    }
    else if (this.highPassword.test(text)) {
      this.state.passwordMsg = this.levelPassword[3];
    }
    else if (text.search(' ') !== -1 || text.search('\\.') !== -1) {
      this.state.passwordMsg = this.errorPassword;
    }
    else {
      this.state.passwordMsg = this.levelPassword[0];
    }
    this.setState({ password: text })
  }
  getConfirmPassword = (text) => {
    if (text != this.state.password) {
      this.state.cfpasswordMsg = this.diffPassword;
    }
    else {
      this.state.cfpasswordMsg = ''
    }
    this.setState({ confirmPassword: text })
  }
  getEmail = (text) => {
    this.setState({ email: text })
  }
  getIdentifyCode = (text) => {
    this.setState({ identifyCode: text })
  }

  //解决组件销毁后设置state的问题
  componentWillUnmount() {
    this.setState = (state, callback) => {
      return;
    }
  }

  //确认注册
  confirmRegister = () => {
    // console.log(this.state.passwordMsg)
    if (this.state.username != '' && this.state.password != '' && this.state.confirmPassword != '' && this.state.email != '' && this.state.identifyCode != '') {
      if (this.state.passwordMsg != this.errorPassword && this.state.passwordMsg != this.levelPassword[0]) {
        if (this.state.confirmPassword == this.state.password) {
          if (this.emailCode.test(this.state.email)) {
            const data = {
              username: this.state.username,
              password: this.state.password,
              checkPassword: this.state.confirmPassword,
              email: this.state.email,
              captcha: this.state.identifyCode
            }
            // console.log(data)
            authService.signUp(data).then(res => {
              // 加载当前用户信息
              this.context.loadUser(res.content.token);
              const { navigate } = this.props.navigation;
              Alert.alert('注册成功', '返回主页！', [{ text: '确定', onPress: () => { navigate('Index'); } }])
            }).catch(err => {
              Alert.alert(err.message);
            })
          } else {
            Alert.alert('注册失败', '邮箱格式有误！')
          }
        } else {
          Alert.alert('注册失败', '两次密码输入不同！')
        }
      } else {
        Alert.alert('注册失败', '密码格式有误！')
      }
    } else {
      Alert.alert('注册失败', '输入信息不可为空！')
    }
  }

  render() {
    return (
      //用户名 密码 确认密码 邮箱 邮箱验证码
      <NativeBaseProvider>
        <Pressable flex={1} onPress={this.blurText}>
          <Box flex={1} alignItems="center" bg='#fff' justifyContent="center">
            <Text fontSize='38' fontWeight='bold' color='#000' position='relative' top='-20'>注册</Text>
            <Input  //用户名输入框
              ref='username'
              onChangeText={(text) => this.getUsername(text)}
              _light={{ bg: 'coolGray.100' }}
              _dark={{ bg: 'coolGray.800' }}
              placeholder='用户名'
              w='75%'
              size='xl'
              mb={'4'}
              InputLeftElement={<Icon as={<Ionicons name='person' />} size={5} ml='5' mr='3' color="muted.400" />}
            />

            <Input  //密码输入框
              ref='password'
              onChangeText={(text) => this.getPassword(text)}

              _light={{ bg: 'coolGray.100' }}
              _dark={{ bg: 'coolGray.800' }}
              placeholder='密码'
              w='75%'
              size='xl'
              mx={'4'}
              InputLeftElement={<Icon as={<Ionicons name='lock-closed' />} size={5} ml='5' mr='3' color="muted.400" />}
              type={this.state.showPwd ? 'text' : 'password'}
              InputRightElement={<Icon as={<Ionicons name={this.state.showPwd ? 'eye' : 'eye-off'} />} onPress={() => this.setShow('pwd')} size={5} ml='5' mr='3' color="muted.400" />}
            />
            <Text color='#666'>{this.state.passwordMsg}</Text>
            <Input  //确认密码输入框
              ref='confirmPassword'
              onChangeText={(text) => this.getConfirmPassword(text)}

              _light={{ bg: 'coolGray.100' }}
              _dark={{ bg: 'coolGray.800' }}
              placeholder='确认密码'
              w='75%'
              size='xl'
              mx={'4'}
              InputLeftElement={<Icon as={<Ionicons name='lock-closed-outline' />} size={5} ml='5' mr='3' color="muted.400" />}
              type={this.state.showCfPwd ? 'text' : 'password'}
              InputRightElement={<Icon as={<Ionicons name={this.state.showCfPwd ? 'eye' : 'eye-off'} />} onPress={() => this.setShow('cfpwd')} size={5} ml='5' mr='3' color="muted.400" />}
            />
            <Text color='#666'>{this.state.cfpasswordMsg}</Text>
            <Input  //邮箱输入框
              ref='email'
              onChangeText={(text) => this.getEmail(text)}
              _light={{ bg: 'coolGray.100' }}
              _dark={{ bg: 'coolGray.800' }}
              placeholder='邮箱'
              w='75%'
              size='xl'
              mb={'4'}
              InputLeftElement={<Icon as={<Ionicons name='mail' />} size={5} ml='5' mr='3' color="muted.400" />}
            />

            <Box alignItems="center" justifyContent="center">
              <Input  //验证码输入框+发送验证码
                ref='identifyCode'
                onChangeText={(text) => this.getIdentifyCode(text)}
                _light={{ bg: 'coolGray.100' }}
                _dark={{ bg: 'coolGray.800' }}
                placeholder='验证码'
                w='75%'
                size='xl'
                InputLeftElement={<Icon as={<Ionicons name='mail-open' />} size={5} ml='5' mr='3' color="muted.400" />}
                InputRightElement={<Button
                  padding='2'
                  bg='#58812F'
                  opacity={this.state.isSent ? '0.6' : '1.0'}
                  isDisabled={this.state.isSent ? true : false}
                  onPress={() => this.setIsSent()}
                >{this.state.isSent ? '        ' + this.state.sentTime.toString() + 's' : '发送验证码'}</Button>}
              />
            </Box>

            <Button
              position='relative'
              top='10'
              w='75%'
              bg='#58812F'
              h='12'
              size='lg'
              onPress={this.confirmRegister}
            >注册</Button>
          </Box>
        </Pressable>
      </NativeBaseProvider>
    );
  }
}
