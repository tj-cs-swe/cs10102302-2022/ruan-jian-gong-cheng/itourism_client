import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Input, Pressable, Select, Avatar } from 'native-base'
import { Alert } from 'react-native'
import spotService from '../../../service/spot.service'
import spotAdminService from '../../../service/spotAdmin.service'
import { AuthContext } from '../../../context/auth.context'
import { launchImageLibrary } from 'react-native-image-picker'

export default class ChangeSpotInfo extends Component {
  static contextType = AuthContext;
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      figure: '',
      introduction: '',
      leftTickets: '',
      name: '',
      ticketPrice: ''
    }
  }

  setInfo(res){
    this.setState({
      id: res.id,
      figure: res.figure,
      introduction: res.introduction,
      leftTickets: res.leftTickets,
      name: res.name,
      ticketPrice: res.ticketPrice
    })
  }

  componentDidMount() {
    spotService.getSpotInfo(this.context.user.adminSpot.id).then(res => {
      this.setInfo(res.content)
    }).catch(err => {
      alert(err.message)
    })
  }

  //点击空白失去焦点
  blurText = () => {
    this.refs.name.blur();
    this.refs.introduction.blur();
    this.refs.leftTickets.blur();
    this.refs.ticketPrice.blur();
  }

  getname = (text) => {
    this.setState({ name: text })
  }
  getintroduction = (text) => {
    this.setState({ introduction: text })
  }
  getleftTickets = (text) => {
    this.setState({ leftTickets: text })
  }
  getticketPrice = (text) => {
    this.setState({ ticketPrice: text })
  }

  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <Pressable flex={1} onPress={this.blurText}>
          <Flex direction='row'>
            <Text mt='16%' ml='10%' fontSize='xl'>宣传图:   </Text>
            <Avatar source={{
              uri: this.state.figure
            }} size='75' mb='10%' mt='12%' ml='5%' />
            <Button h='25%' mt='16%' ml='8%' onPress={() => {
              launchImageLibrary({
                mediaType: 'photo',
                includeBase64: true,
                selectionLimit: 1
              }, resPhoto => {
                if (resPhoto.didCancel) return;
                const resBase64 = 'data:image/jpeg;base64,' + resPhoto.assets[0].base64;
                this.setState({figure: resBase64})
              })
            }}>上传宣传图</Button>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>名称:   </Text>
            <Input mt='2%' h='75%' w='50%' ref='name' onChangeText={(text) => this.getname(text)} defaultValue={this.state.name}></Input>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>简介:   </Text>
            <Input mt='2%' h='75%' w='50%' ref='introduction' onChangeText={(text) => this.getintroduction(text)} defaultValue={this.state.introduction}></Input>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>余票:   </Text>
            <Input mt='2%' h='75%' w='50%' ref='leftTickets' onChangeText={(text) => this.getleftTickets(text)} defaultValue={(this.state.leftTickets).toString()}></Input>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>票价:   </Text>
            <Input mt='2%' h='75%' w='50%' ref='ticketPrice' onChangeText={(text) => this.getticketPrice(text)} defaultValue={(this.state.ticketPrice).toString()}></Input>
          </Flex>
          <Flex direction='row' mt='10%' ml='auto' mr='auto'>
            <Button onPress={() => {
              const data = {
                id: this.state.id,
                figure: this.state.figure,
                introduction: this.state.introduction,
                leftTickets: this.state.leftTickets,
                name: this.state.name,
                ticketPrice: this.state.ticketPrice
              }
              if (data.name.length === 0 || data.name.length > 15) {
                Alert.alert('名称不符合要求');
                return;
              }
              if (data.figure.length === 0) {
                Alert.alert('未添加图片');
                return;
              }
              if (data.introduction.length === 0 || data.introduction.length > 512) {
                Alert.alert('简介不符合要求');
                return;
              }
              if (!/^[1-9][0-9]*$/.test(data.leftTickets) || data.leftTickets.length === 0 || data.leftTickets.length > 6) {
                Alert.alert('请输入合法的余票数(1-999999)');
                return;
              }
              if (!/^[1-9][0-9]*$/.test(data.ticketPrice) || data.ticketPrice.length === 0 || data.ticketPrice.length > 6) {
                Alert.alert('请输入合法的票价(1-999999)');
                return;
              }
              spotAdminService.editSpot(data).then(res => {
                Alert.alert('修改成功', '返回景区信息页面', [{ text: '确定', onPress: () => { navigation.goBack(); } }])
              }).catch(err => {
                alert(err.message)
              })
            }} size='lg'> 确认修改 </Button>
          </Flex>
        </Pressable>
      </NativeBaseProvider >
    );
  }
}
