import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Avatar } from 'native-base'
import { AuthContext } from '../../../context/auth.context'
import spotService from '../../../service/spot.service'
import ChangeSpotInfo from './ChangeSpotInfo'

var iscleaned = false;

export default class SpotInfoManage extends Component {
  static contextType = AuthContext;
  constructor(props){
    super(props);
    this.state = {
      id: -1,
      figure: '',
      introduction: '',
      leftTickets: -1,
      name: '',
      ticketPrice: -1
    }
  }
  componentDidMount(){
    iscleaned = false;
    spotService.getSpotInfo(this.context.user.adminSpot.id).then(res => {
      if(!iscleaned && res.content){
        this.setState({
          id: res.content.id,
          figure: res.content.figure,
          introduction: res.content.introduction,
          leftTickets: res.content.leftTickets,
          name: res.content.name,
          ticketPrice: res.content.ticketPrice
        })
      }
    }).catch(err => {
      alert(err.message)
    })
  }
  componentDidUpdate(){
    spotService.getSpotInfo(this.context.user.adminSpot.id).then(res => {
      if(!iscleaned && res.content){
        this.setState({
          id: res.content.id,
          figure: res.content.figure,
          introduction: res.content.introduction,
          leftTickets: res.content.leftTickets,
          name: res.content.name,
          ticketPrice: res.content.ticketPrice
        })
      }
    }).catch(err => {
      alert(err.message)
    })
  }
  componentWillUnmount(){
    iscleaned = true;
  }
  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Text mt='16%' ml='10%' fontSize='2xl'>景区宣传图:</Text>
          <Avatar source={{
            uri: this.state.figure
          }}  size='75' mb='10%' mt='12%' ml='10%' />
        </Flex>
        <Flex direction='row'>
          <Text mb='2%' ml='10%' fontSize='2xl'>id:  {this.state.id}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2%' ml='10%' fontSize='2xl'>名称:  {this.state.name}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2%' ml='10%' fontSize='2xl'>简介:  {this.state.introduction}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2%' ml='10%' fontSize='2xl'>余票:  {this.state.leftTickets}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2%' ml='10%' fontSize='2xl'>票价:  {this.state.ticketPrice}</Text>
        </Flex>
        <Flex direction='row' mt='10%' ml='10%' mr='10%'>
          <Button size='lg' mr='auto' ml='auto' onPress={() => navigation.navigate('ChangeSpotInfo')}>修改信息</Button>
        </Flex>
      </NativeBaseProvider>
    );
  }
}
