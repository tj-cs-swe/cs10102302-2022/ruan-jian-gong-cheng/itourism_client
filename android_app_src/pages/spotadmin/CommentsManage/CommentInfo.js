import React, { Component } from 'react'
import { Alert } from 'react-native'
import { NativeBaseProvider, Button, Text, Flex, Image } from 'native-base'
import spotAdminService from '../../../service/spotAdmin.service';
import travelService from '../../../service/travel.service';
var iscleaned = false;

export default class CommentInfo extends Component {
  constructor(props) {
    super(props);
    this.commentId = this.props.route.params.id;
    this.state = {
      contain: '',
      time: '',
      serviceId: -1,
      userId: -1,
      top: 0,
      verified: 0
    }
  }
  componentDidMount() {
    iscleaned = false;
    travelService.getCommentInfo(this.commentId).then(res => {
      if (!iscleaned) {
        this.setState({
          contain: res.content.contain,
          time: res.content.time,
          serviceId: res.content.servicePoint.id,
          userId: res.content.user.id,
          top: res.content.top,
          verified: res.content.verified
        })
      }
    }).catch(err => {
      alert(err.message)
    })
  }
  componentWillUnmount() {
    iscleaned = true;
  }
  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Text mb='2' ml='10' fontSize='2xl'>id:  {this.commentId}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10' fontSize='2xl'>内容:  {this.state.contain}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10' fontSize='2xl'>评论时间:  {this.state.time}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10' fontSize='2xl'>景点id:  {this.state.serviceId}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10' fontSize='2xl'>用户id:  {this.state.userId}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10' fontSize='2xl'>是否置顶:  {this.state.top ? '是' : '否'}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10' fontSize='2xl'>是否审核:  {this.state.verified ? '是' : '否'}</Text>
        </Flex>
        <Flex direction='row' mt='10' ml='10' mr='10'>
          <Button size='lg' mr='auto' ml='auto' onPress={() => {
            Alert.alert('', '是否确认删除该评论', [{
              text: '确定', onPress: () => {
                spotAdminService.deleteComment(this.commentId).then(res => {
                  alert('删除成功！');
                }).catch(err => {
                  Alert.alert(err.message);
                });
              }
            }, { text: '取消', onPress: () => { return; } }]);
          }}>删除评论</Button>
          <Button size='lg' mr='auto' ml='auto' onPress={() => {
            spotAdminService.agreeComment(this.commentId).then(res => {
              alert('已审核！')
            }).catch(err => {
              Alert.alert(err.message);
            })
          }}>通过审核</Button>
          <Button size='lg' mr='auto' ml='auto' onPress={() => {
            spotAdminService.upComment(this.commentId).then(res => {
              alert('已置顶！')
            }).catch(err => {
              Alert.alert(err.message);
            })
          }}>评论置顶</Button>
        </Flex>
      </NativeBaseProvider>
    );
  }
}
