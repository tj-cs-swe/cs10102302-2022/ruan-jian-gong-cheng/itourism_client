import React, { Component, useState, useEffect } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Box, Select, HStack } from 'native-base'
import { Alert } from 'react-native'
import spotAdminService from '../../../service/spotAdmin.service';
import travelService from '../../../service/travel.service';

import CommentInfo from './CommentInfo';

var isEnd = false;
var isCleaned = false;

function CommentsList(props){
  const content = props.content;
  const navigation = props.navi;
  const commentslist = (content ? content.map((commentInfo) =>
    <Flex key={commentInfo.id}>
      <Flex bg='blue.200' direction='row'>
        <Flex position='absolute' ml='10%' px='10' pt='5' pb='5'>{commentInfo.id}</Flex>
        <Flex position='absolute' ml='40%' px='5' pt='5' pb='5'>{commentInfo.verified?'已审核':'未审核'}</Flex>
        <Button ml='68%' px='5' pt='5' pb='5' onPress={() => navigation.navigate('CommentInfo', {id: commentInfo.id})}>评论管理</Button>
      </Flex>
      <Flex pt='5'></Flex>
    </Flex>
  ) : null);
  return(
    <>{commentslist}</>
  );
}

export default class CommentsManage extends Component {
  constructor(props) {
    super(props);
    this.page = 0;
    this.size = 5;
    this.servicePointId = this.props.route.params.id;
    this.state = {
      content: []
    }
  }

  componentDidMount(){
    isCleaned = false;
    travelService.getCommentList(this.servicePointId, this.page, this.size).then(res => {
      isEnd = false;
      if(res.content){
        if(res.content.length < this.size)
          isEnd = true;
      }
      if(!isCleaned && res.content){
        this.setState({
          content: res.content
        })
      }
    }).catch(err => {
      alert(err.message)
    })
  }

  componentWillUnmount(){
    isCleaned = true;
  }

  render() {
    const { navigation } = this.props;
    const { goBack } = this.props.navigation;
    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Flex ml='10%' px='5' pt='5' pb='5'>评论id</Flex>
          <Flex ml='12%' px='6' pt='5' pb='5'>状态</Flex>
          <Flex ml='12%' px='5' pt='5' pb='5'>操作</Flex>
        </Flex>
        <Flex>
          <CommentsList content={this.state.content} navi={navigation}/>
        </Flex>
        <HStack space={5} justifyContent="center" mt='10'>
          <Button size='lg'
            onPress={() => {
              //需要重新发请求获取新一页景区列表
              if(this.page > 0){
                this.page -= 1;
                travelService.getCommentList(this.servicePointId, this.page, this.size).then(res => {
                  isEnd = false;
                  if(res.content){
                    if(res.content.length < this.size)
                      isEnd = true;
                  }
                  if(!isCleaned){
                    this.setState({
                      content: res.content
                    })
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >上一页</Button>
          <Button size='lg'
            onPress={() => {
              if(!isEnd){
                this.page += 1;
                travelService.getCommentList(this.servicePointId, this.page, this.size).then(res => {
                  isEnd = false;
                  if(res.content){
                    if(res.content.length < this.size)
                      isEnd = true;
                  }
                  if(!isCleaned){
                    this.setState({
                      content: res.content
                    })
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >下一页</Button>
        </HStack>
      </NativeBaseProvider>
    );
  }
}
