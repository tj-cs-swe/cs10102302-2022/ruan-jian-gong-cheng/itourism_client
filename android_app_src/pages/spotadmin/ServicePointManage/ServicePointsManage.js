import React, { Component, useState, useEffect, useContext } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Box, Select, HStack } from 'native-base'
import { Alert } from 'react-native'
import spotAdminService from '../../../service/spotAdmin.service';
import travelService from '../../../service/travel.service'
import { AuthContext } from '../../../context/auth.context';

import AddServicePoint from './AddServicePoint';
import CommentsManage from '../CommentsManage/CommentsManage';
import ServicePointInfo from './ServicePointInfo';

var isEnd = false;
var isCleaned = false;

function ServicePointsList(props){
  const navigation = props.navi;
  const context = useContext(AuthContext);
  const content = props.content;

  const servicePointsList = content.map((servicePointInfo) => (
    <Flex key={servicePointInfo.id.toString()}>
      <Flex bg='blue.200' direction='row'>
        <Flex position='absolute' ml='10%' pt='5' pb='5'>{servicePointInfo.id}</Flex>
        <Flex position='absolute' ml='30%' pt='5' pb='5'>{servicePointInfo.name}</Flex>
        <Button ml='65%' pt='5' pb='5' onPress={() => navigation.navigate('ServicePointInfo', {id: servicePointInfo.id})}>详情</Button>
        <Button ml='5%' pt='5' pb='5' onPress={() => navigation.navigate('CommentsManage', {id: servicePointInfo.id})}>评论</Button>
      </Flex>
      <Flex pt='5'></Flex>
    </Flex>
  ));
  return(
    <>{servicePointsList}</>
  );
}

export default class ServicePointsManage extends Component {
  constructor(props) {
    super(props);
    this.page = 0;
    this.size = 5;
    this.state = {
      content: []
    }
  }
  static contextType = AuthContext;
  componentDidMount(){
    isCleaned = false;
    
    travelService.getServicePointList(this.context.user.adminSpot.id, this.page, this.size).then(res => {
      if(res.content){
        isEnd = false;
        if(res.content.length < this.size)
          isEnd = true;
        if(!isCleaned && res.content){
          this.setState({
            content: res.content
          })
        }
      }
    }).catch(err => {
      alert(err.message)
    })
  }

  componentWillUnmount(){
    isCleaned = true;
  }


  render() {
    const { navigation } = this.props;
    const { goBack } = this.props.navigation;

    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Flex ml='10%' pt='5' pb='5'>id</Flex>
          <Flex ml='20%' pt='5' pb='5'>景点名</Flex>
          <Flex px='33%' pt='5' pb='5'>操作</Flex>
        </Flex>
        <Flex>
          <ServicePointsList content={this.state.content} navi={navigation}/>
        </Flex>

        
        <HStack space={5} justifyContent="center" mt='10'>
          <Button size='lg'
            onPress={() => {
              //需要重新发请求获取新一页景点列表
              if(this.page > 0){
                this.page -= 1;
                travelService.getServicePointList(this.context.user.adminSpot.id, this.page, this.size).then(res => {
                  if(res.content){
                    isEnd = false;
                    if(res.content.length < this.size)
                      isEnd = true;
                    if(!isCleaned && res.content){
                      this.setState({
                        content: res.content
                      })
                    }
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >上一页</Button>
          <Button size='lg'
            onPress={() => navigation.navigate('AddServicePoint')}
          >增添新景点</Button>
          <Button size='lg'
            onPress={() => {
              if(!isEnd){
                this.page += 1;
                travelService.getServicePointList(this.context.user.adminSpot.id, this.page, this.size).then(res => {
                  if(res.content){
                    isEnd = false;
                    if(res.content.length < this.size)
                      isEnd = true;
                    if(!isCleaned && res.content){
                      this.setState({
                        content: res.content
                      })
                    }
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >下一页</Button>
        </HStack>
        
      </NativeBaseProvider>
    );
  }
}
