import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Avatar } from 'native-base'
import { Alert } from 'react-native'
import spotAdminService from '../../../service/spotAdmin.service';

import ChangeServicePointInfo from './ChangeServicePointInfo';
import AudioManage from './AudioManage';

export default class ServicePointInfo extends Component {
  constructor(props) {
    super(props);
    this.servicePointId = this.props.route.params.id;
    this.state = {
      //临时数据
      name: '',
      figure: '',
      introduce: '',
      position: '',
      type: ''
    }
  }

  componentDidMount() {
    //通过props的id取到对应的景点信息
    spotAdminService.getServicePointInfo(this.servicePointId).then(res => {
      this.setState({
        name: res.content.name,
        figure: res.content.figure,
        introduce: res.content.introduce,
        position: res.content.position,
        type:res.content.type
      })
    }).catch(err => {
      alert(err.message)
    })
  }

  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Text mt='16%' ml='10%' fontSize='2xl'>景点缩略图:</Text>
          <Avatar source={{
            uri: this.state.figure
          }}  size='75' mb='10%' mt='12%' ml='10%' />
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>景点名称:  {this.state.name}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>景点简介:  {this.state.introduce}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>景点位置:  {this.state.position}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>景点种类:  {this.state.type}</Text>
        </Flex>
        <Flex direction='row' mt='10%' ml='10%' mr='10%'>
          <Button onPress={() => navigation.navigate('ChangeServicePointInfo', {id: this.servicePointId})} size='lg' mr='auto' ml='auto'>修改景点信息</Button>
          <Button onPress={() => navigation.navigate('AudioManage', {id: this.servicePointId})} size='lg' mr='auto' ml='auto'>音频管理</Button>
          <Button onPress={() => {
            Alert.alert("", "确认删除此景点？", [
              { text: '确认删除', onPress: () => {
                spotAdminService.deleteServicePoint(this.servicePointId).then(res => {
                  alert("成功删除！")
                  navigation.navigate('ServicePointsManage')
                }).catch(err => {
                  alert(err.message)
                })
              } },
              { text: '再想想', onPress: () => { console.log('no') } }
            ])
          }} size='lg' mr='auto' ml='auto'>删除景点</Button>
        </Flex>
      </NativeBaseProvider>
    );
  }
}
