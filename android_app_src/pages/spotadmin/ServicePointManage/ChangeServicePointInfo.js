import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Input, Pressable, Select, Avatar } from 'native-base'
import { Alert } from 'react-native'
import { launchImageLibrary } from 'react-native-image-picker'
import spotAdminService from '../../../service/spotAdmin.service'

export default class ChangeServicePointInfo extends Component {
  constructor(props) {
    super(props);
    this.servicePointId = this.props.route.params.id;
    this.state = {
      name: '',
      figure: '',
      introduce: '',
      position: '',
      type: ''
    }
  }

  componentDidMount() {
    //通过props的id取到对应的景点信息
    spotAdminService.getServicePointInfo(this.servicePointId).then(res => {
      this.setState({
        name: res.content.name,
        figure: res.content.figure,
        introduce: res.content.introduce,
        position: res.content.position,
        type: res.content.type
      })
    }).catch(err => {
      alert(err.message)
    })
  }

  //点击空白失去焦点
  blurText = () => {
    this.refs.name.blur();
    this.refs.introduce.blur();
    this.refs.position.blur();
    this.refs.type.blur();
  }

  getname = (text) => {
    this.setState({ name: text })
  }
  getintroduce = (text) => {
    this.setState({ introduce: text })
  }
  getposition = (text) => {
    this.setState({ position: text })
  }
  gettype = (text) => {
    this.setState({ type: text })
  }

  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <Pressable flex={1} onPress={this.blurText}>
          <Flex direction='row'>
            <Text mt='16%' ml='10%' fontSize='xl'>景点缩略图:   </Text>
            <Avatar source={{
              uri: this.state.figure
            }} size='75' mb='10%' mt='12%' ml='5%' />
            <Button h='25%' mt='16%' ml='8%' onPress={() => {
              launchImageLibrary({
                mediaType: 'photo',
                includeBase64: true,
                selectionLimit: 1
              }, resPhoto => {
                if (resPhoto.didCancel) return;
                const resBase64 = 'data:image/jpeg;base64,' + resPhoto.assets[0].base64;
                this.setState({ figure: resBase64 })
              })
            }}>上传缩略图</Button>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>景点名称:   </Text>
            <Input mt='2%' h='75%' w='50%' ref='name' onChangeText={(text) => this.getname(text)} defaultValue={this.state.name}></Input>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>景点简介:   </Text>
            <Input mt='2%' h='75%' w='50%' ref='introduce' onChangeText={(text) => this.getintroduce(text)} defaultValue={this.state.introduce}></Input>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>景点位置:   </Text>
            <Input mt='2%' h='75%' w='50%' ref='position' onChangeText={(text) => this.getposition(text)} defaultValue={this.state.position}></Input>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>景点种类:   </Text>
            <Input mt='2%' h='75%' w='50%' ref='type' onChangeText={(text) => this.gettype(text)} defaultValue={this.state.type}></Input>
          </Flex>
          <Flex direction='row' mt='10%' ml='auto' mr='auto'>
            <Button onPress={() => {
              const data = {
                id: this.servicePointId,
                figure: this.state.figure,
                introduce: this.state.introduce,
                position: this.state.position,
                name: this.state.name,
                type: this.state.type
              }
              if (data.introduction.length === 0 || data.introduction.length > 512) {
                Alert.alert('简介不符合要求');
                return;
              }
              if (data.name.length === 0 || data.name.length > 15) {
                Alert.alert('名称不符合要求');
                return;
              }
              if (data.type.length === 0 || data.type.length > 15) {
                Alert.alert('种类不符合要求');
                return;
              }
              spotAdminService.editServicePoint(data).then(res => {
                alert('修改成功！')
              }).catch(err => {
                alert(err.message)
              })
            }} size='lg'> 确认修改 </Button>
          </Flex>
        </Pressable>
      </NativeBaseProvider >
    );
  }
}
