//import { Text, View } from 'react-native'
import React, { Component, useState } from 'react'
import { Alert } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NativeBaseProvider, Flex, Image, Text, Input, Button, View, HStack, Pressable } from 'native-base';
import spotAdminService from '../../../service/spotAdmin.service';
import DocumentPicker from 'react-native-document-picker';

export default class ChangeAudio extends Component {
  constructor(props) {
    super(props);
    this.servicePointId = this.props.route.params.spid;
    this.state = {
      id: this.props.route.params.id,
      name: this.props.route.params.name,
      introduce: '',
      url: this.props.route.params.url,
      audioData: {}
    }
  }

  //点击空白失去焦点
  blurText = () => {
    this.refs.name.blur();
    this.refs.introduce.blur();
  }

  getname = (text) => {
    this.setState({ name: text })
  }

  getintroduce = (text) => {
    this.setState({ introduce: text })
  }


  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <Pressable flex={1} onPress={this.blurText}>
          <HStack space={5} justifyContent="center" mt='10' mb='10'>
            <Button onPress={()=>{
              DocumentPicker.pick().then(res=>{
                //console.log(res);
                formData = new FormData();
                file = {
                  uri: res[0].uri,
                  type: res[0].type,
                  name: res[0].name,
                  size: res[0].size
                }
                formData.append('file', file)
                spotAdminService.uploadAudio(formData).then(res=>{
                  console.log(res)
                  this.setState({url: res.content.path})
                }).catch(err=>{
                  alert(err.message)
                })
              }).catch(err=>{
                if(DocumentPicker.isCancel(err)){
                  console.log('cancel');
                }
              })
            }}>上传文件</Button>
          </HStack>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>路径:       </Text>
            <Text mt='2%' h='75%' w='50%' ref='url'>{this.state.url}</Text>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>名称:       </Text>
            <Input mt='2%' h='75%' w='50%' ref='name' onChangeText={(text) => this.getname(text)} defaultValue={this.state.name}></Input>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>简介:       </Text>
            <Input mt='2%' h='75%' w='50%' ref='introduce' onChangeText={(text) => this.getintroduce(text)} defaultValue={this.state.introduce}></Input>
          </Flex>
          <HStack space={5} justifyContent="center" mt='10'>
            <Button onPress={()=>{
              this.state.audioData = {
                name: this.state.name,
                introduce: this.state.introduce,
                url: this.state.url,
                id: this.state.id
              }
              spotAdminService.editAudio(this.state.audioData).then(res=>{
                Alert.alert('修改成功', '返回音频管理页面', [{ text: '确定', onPress: () => { navigation.goBack(); } }])
              }).catch(err=>{
                alert(err.message)
              })
            }}>修改音频信息</Button>
          </HStack>
        </Pressable>
      </NativeBaseProvider>
      

		);
  }
}