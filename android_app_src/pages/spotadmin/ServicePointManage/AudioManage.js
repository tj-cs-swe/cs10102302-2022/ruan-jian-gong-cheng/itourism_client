import React, { Component, useState, useEffect, useContext } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Box, Select, HStack } from 'native-base'
import { Alert } from 'react-native'
import { AuthContext } from '../../../context/auth.context';
import spotAdminService from '../../../service/spotAdmin.service';
import travelService from '../../../service/travel.service';
import AddAudio from './AddAudio';
import ChangeAudio from './ChangeAudio';

var isEnd = false;
var isCleaned = false;
var servicePointId = -1;

function AudiosList(props){
  const navigation = props.navi;
  const context = useContext(AuthContext);
  const content = props.content;

  const audiosList = content.map((audioInfo) => (
    <Flex key={audioInfo.id.toString()}>
      <Flex bg='blue.200' direction='row'>
        <Flex position='absolute' ml='10%' pt='5' pb='5'>{audioInfo.id}</Flex>
        <Flex position='absolute' ml='20%' pt='5' pb='5'>{audioInfo.name}</Flex>
        <Button ml='62%' pt='5' pb='5' onPress={() => navigation.navigate('ChangeAudio', {
          spid: servicePointId,
          id: audioInfo.id,
          name: audioInfo.name,
          url: audioInfo.url
          })}>修改</Button>
        <Button ml='5%' pt='5' pb='5' onPress={()=>{
          spotAdminService.deleteAudio(audioInfo.id).then(res=>{
            console.log(res)
            alert('删除成功')
          }).catch(err=>{
            console.log(err)
            alert(err.message)
          })
        }}>删除</Button>
      </Flex>
      <Flex pt='5'></Flex>
    </Flex>
  ));
  return(
    <>{audiosList}</>
  );
}

export default class AudioManage extends Component {
  constructor(props) {
    super(props);
    this.servicePointId = this.props.route.params.id;
    this.page = 0;
    this.size = 5;
    this.state = {
      content: [//测试数据
        {
          id: -1,
          name: ''
        }
      ]
    }
  }
  static contextType = AuthContext;

  componentDidMount(){
    isCleaned = false;
    servicePointId = this.servicePointId;
    travelService.getAudioList(this.servicePointId, this.page, this.size).then(res => {
      if(res.content){
        isEnd = false;
        if(res.content.length < this.size)
          isEnd = true;
        if(!isCleaned && res.content){
          this.setState({
            content: res.content
          })
        }
      }
    }).catch(err => {
      alert(err.message)
    })
  }

  componentWillUnmount(){
    isCleaned = true;
  }


  render() {
    const { navigation } = this.props;
    const { goBack } = this.props.navigation;

    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Flex ml='10%' pt='5' pb='5'>id</Flex>
          <Flex ml='8%' pt='5' pb='5'>名称</Flex>
          <Flex px='46%' pt='5' pb='5'>操作</Flex>
        </Flex>
        <Flex>
          <AudiosList content={this.state.content} navi={navigation}/>
        </Flex>

        
        <HStack space={5} justifyContent="center" mt='10'>
          <Button size='lg'
            onPress={() => {
              //需要重新发请求获取新一页景点列表
              if(this.page > 0){
                this.page -= 1;
                travelService.getAudioList(this.servicePointId, this.page, this.size).then(res => {
                  if(res.content){
                    isEnd = false;
                    if(res.content.length < this.size)
                      isEnd = true;
                    if(!isCleaned && res.content){
                      this.setState({
                        content: res.content
                      })
                    }
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >上一页</Button>
          <Button size='lg'
            onPress={() => navigation.navigate('AddAudio', {id: this.servicePointId})}
          >增添新音频</Button>
          <Button size='lg'
            onPress={() => {
              if(!isEnd){
                this.page += 1;
                travelService.getAudioList(this.servicePointId, this.page, this.size).then(res => {
                  if(res.content){
                    isEnd = false;
                    if(res.content.length < this.size)
                      isEnd = true;
                    if(!isCleaned && res.content){
                      this.setState({
                        content: res.content
                      })
                    }
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >下一页</Button>
        </HStack>
        
      </NativeBaseProvider>
    );
  }
}
