import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Pressable, Input, Avatar, Box, ScrollView } from 'native-base'
import { Alert } from 'react-native'
import { launchImageLibrary } from 'react-native-image-picker'
import spotAdminService from '../../../service/spotAdmin.service'
import { MapView, MapType, Marker } from "react-native-amap3d";

export default class AddServicePoint extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      figure: '',
      introduce: '',
      position: '',
      type: '',
      mark: null
    }
  }

  //点击空白失去焦点
  blurText = () => {
    this.refs.name.blur();
    this.refs.introduce.blur();
    this.refs.type.blur();
  }

  getname = (text) => {
    this.setState({ name: text })
  }
  getintroduce = (text) => {
    this.setState({ introduce: text })
  }
  gettype = (text) => {
    this.setState({ type: text })
  }

  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <ScrollView my={10}>
          <Pressable flex={1} onPress={this.blurText}>
            <Flex direction='row'>
              <Text mt='5' mb='5' ml='10' fontSize='xl'>景点名:      </Text>
              <Input mt='2' h='75%' w='50%' ref='name' onChangeText={(text) => this.getname(text)}></Input>
            </Flex>
            <Flex direction='row'>
              <Text mt='16' ml='10' fontSize='xl'>景点缩略图:  </Text>
              <Avatar source={{
                uri: this.state.figure
              }} size='75' mb='10' mt='12' ml='5' />
              <Button h='25%' mt='16' ml='8' onPress={() => {
                launchImageLibrary({
                  mediaType: 'photo',
                  includeBase64: true,
                  selectionLimit: 1
                }, resPhoto => {
                  if (resPhoto.didCancel) return;
                  const resBase64 = 'data:image/jpeg;base64,' + resPhoto.assets[0].base64;
                  this.setState({ figure: resBase64 })
                })
              }}>上传缩略图</Button>
            </Flex>
            <Flex direction='row'>
              <Text mt='5' mb='5' ml='10' fontSize='xl'>景点介绍:   </Text>
              <Input mt='2' h='75%' w='50%' ref='introduce' onChangeText={(text) => this.getintroduce(text)}></Input>
            </Flex>
            <Flex direction='row'>
              <Text mt='5' mb='5' ml='10' fontSize='xl'>景点位置:  {this.state.mark ? "已选择" : "未选择"} </Text>
            </Flex>
            <Flex direction='row'>
              <Text mt='5' mb='5' ml='10' fontSize='xl'>景点类别:   </Text>
              <Input mt='2' h='75%' w='50%' ref='type' onChangeText={(text) => this.gettype(text)}></Input>
            </Flex>
          </Pressable>
          <Box height={200} m={5}>
            <MapView
              mapType={MapType.Standard}
              compassEnabled={false}
              initialCameraPosition={{
                target: {
                  latitude: 31.284962,
                  longitude: 121.216342
                },
                zoom: 18,
              }}
              onPress={({ nativeEvent }) => {
                this.setState({
                  mark: nativeEvent
                })
              }}
            >
              {this.state.mark ? <Marker
                position={{
                  latitude: this.state.mark.latitude,
                  longitude: this.state.mark.longitude
                }}
                icon={require("../../../assets/images/location100.png")}>
              </Marker> : null}
            </MapView>
          </Box>
          <Flex direction='row' mt='10' ml='auto' mr='auto'>
            <Button onPress={() => {
              const data = {
                name: this.state.name,
                figure: this.state.figure,
                introduce: this.state.introduce,
                position: JSON.stringify(this.state.mark),
                type: this.state.type
              }
              if (data.position.length === 0) {
                Alert.alert('未选择位置');
                return;
              }
              if (data.type.length === 0 || data.type.length > 15) {
                Alert.alert('种类不符合要求');
                return;
              }
              if (data.name.length === 0 || data.name.length > 15) {
                Alert.alert('名称不符合要求');
                return;
              }
              if (data.figure.length === 0) {
                Alert.alert('未添加图片');
                return;
              }
              if (data.introduce.length === 0 || data.introduce.length > 512) {
                Alert.alert('简介不符合要求');
                return;
              }
              spotAdminService.addServicePoint(data).then(res => {
                Alert.alert('添加成功', '返回景区管理页面', [{ text: '确定', onPress: () => { navigation.goBack(); } }])
              }).catch(err => {
                Alert.alert(err.message);
              })
            }} size='lg'> 确认添加 </Button>
          </Flex>
        </ScrollView>
      </NativeBaseProvider>
    );
  }
}
