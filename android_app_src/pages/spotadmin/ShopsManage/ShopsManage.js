import React, { Component, useState, useEffect, useContext } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Box, Select, HStack } from 'native-base'
import { Alert } from 'react-native'
import { AuthContext } from '../../../context/auth.context'
import shopService from '../../../service/shop.service'
import spotAdminService from '../../../service/spotAdmin.service'
import AddShop from './AddShop'

var isEnd = false;
var isCleaned = false;

function ShopsList(props){
  const navigation = props.navi;
  const content = props.content;

  const shoplist = content.map((shopinfo) => (
    <Flex key={shopinfo.id.toString()}>
      <Flex bg='blue.200' direction='row'>
        <Flex position='absolute' ml='10%' pt='5' pb='5'>{shopinfo.id}</Flex>
        <Flex position='absolute' ml='30%' pt='5' pb='5'>{shopinfo.name}</Flex>
        <Button ml='70%' pt='5' pb='5' onPress={() => {
            Alert.alert("", "确认删除此商铺？", [
              { text: '确认删除', onPress: () => {
                spotAdminService.deleteShop(shopinfo.id).then(res => {
                  alert('删除成功！')
                }).catch(err => {
                  alert(err.message)
                })
                navigation.navigate('ShopsManage')
              } },
              { text: '再想想', onPress: () => { console.log('no') } }
            ])
          }}>删除商铺</Button>
      </Flex>
      <Flex pt='5'></Flex>
    </Flex>
  ));
  return(
    <>{shoplist}</>
  );
}

export default class ShopManage extends Component {
  constructor(props) {
    super(props);
    this.page = 0;
    this.size = 5;
    this.state = {
      content: []
    }
  }
  static contextType = AuthContext;

  componentDidMount(){
    isCleaned = false;
    shopService.getSceneShops(this.context.user.adminSpot.id, this.page, this.size).then(res => {
      isEnd = false;
      if(!isCleaned && res.content){
        this.setState({
          content: res.content
        })
        isEnd = res.content.length < this.size
      } else {
        isEnd = true
      }
    }).catch(err => {
      alert(err.message)
    })
  }

  componentWillUnmount(){
    isCleaned = true;
  }

  render() {
    const { navigation } = this.props;
    const { goBack } = this.props.navigation;

    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Flex ml='10%' pt='5' pb='5'>id</Flex>
          <Flex ml='20%' pt='5' pb='5'>商铺名</Flex>
          <Flex ml='33%' pt='5' pb='5'>操作</Flex>
        </Flex>
        <Flex>
          <ShopsList content={this.state.content} navi={navigation}/>
        </Flex>

        <HStack space={5} justifyContent="center" mt='10'>
          <Button size='lg'
            onPress={() => {
              //需要重新发请求获取新一页列表
              if(this.page > 0){
                this.page -= 1;
                shopService.getSceneShops(this.context.user.adminSpot.id, this.page, this.size).then(res => {
                  isEnd = false;
                  if(res.content.length < this.size)
                    isEnd = true;
                  if(!isCleaned && res.content){
                    this.setState({
                      content: res.content
                    })
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >上一页</Button>
          <Button size='lg'
            onPress={() => navigation.navigate('AddShop')}
          >增添新商铺</Button>
          <Button size='lg'
            onPress={() => {
              if(!isEnd){
                this.page += 1;
                shopService.getSceneShops(this.context.user.adminSpot.id, this.page, this.size).then(res => {
                  isEnd = false;
                  if(res.content.length < this.size)
                    isEnd = true;
                  if(!isCleaned && res.content){
                    this.setState({
                      content: res.content
                    })
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >下一页</Button>
        </HStack>
      </NativeBaseProvider>
    );
  }
}
