import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Box, Pressable, Input, Avatar, ScrollView } from 'native-base'
import { Alert } from 'react-native'
import { launchImageLibrary } from 'react-native-image-picker'
import spotAdminService from '../../../service/spotAdmin.service'
import { MapView, MapType, Marker } from "react-native-amap3d";

export default class AddShop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      figure: '',
      introduction: '',
      brief: '',
      position: '',
      mark: null
    }
  }

  //点击空白失去焦点
  blurText = () => {
    this.refs.name.blur();
    this.refs.introduction.blur();
    this.refs.brief.blur();
  }

  getname = (text) => {
    this.setState({ name: text })
  }
  getintroduction = (text) => {
    this.setState({ introduction: text })
  }
  getposition = (text) => {
    this.setState({ position: text })
  }
  getbrief = (text) => {
    this.setState({ brief: text })
  }

  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <ScrollView my={10}>
          <Pressable flex={1} onPress={this.blurText}>
            <Flex direction='row'>
              <Text mt='5' mb='5' ml='10' fontSize='xl'>商铺名:       </Text>
              <Input mt='2' h='75%' w='50%' ref='name' onChangeText={(text) => this.getname(text)}></Input>
            </Flex>
            <Flex direction='row'>
              <Text mt='16' ml='10' fontSize='xl'>宣传图:   </Text>
              <Avatar source={{
                uri: this.state.figure
              }} size='75' mb='10' mt='12' ml='5' />
              <Button h='25%' mt='16' ml='8' onPress={() => {
                launchImageLibrary({
                  mediaType: 'photo',
                  includeBase64: true,
                  selectionLimit: 1
                }, resPhoto => {
                  if (resPhoto.didCancel) return;
                  const resBase64 = 'data:image/jpeg;base64,' + resPhoto.assets[0].base64;
                  this.setState({ figure: resBase64 })
                })
              }}>上传宣传图</Button>
            </Flex>
            <Flex direction='row'>
              <Text mt='5' mb='5' ml='10' fontSize='xl'>商铺简介:   </Text>
              <Input mt='2' h='75%' w='50%' ref='brief' onChangeText={(text) => this.getbrief(text)}></Input>
            </Flex>
            <Flex direction='row'>
              <Text mt='5' mb='5' ml='10' fontSize='xl'>商铺介绍:   </Text>
              <Input mt='2' h='75%' w='50%' ref='introduction' onChangeText={(text) => this.getintroduction(text)}></Input>
            </Flex>
            <Flex direction='row'>
              <Text mt='5' ml='10' fontSize='xl'>景点位置:  {this.state.mark ? "已选择" : "未选择"} </Text>
            </Flex>
          </Pressable>
          <Box height={200} m={5}>
            <MapView
              mapType={MapType.Standard}
              compassEnabled={false}
              initialCameraPosition={{
                target: {
                  latitude: 31.284962,
                  longitude: 121.216342
                },
                zoom: 18,
              }}
              onPress={({ nativeEvent }) => {
                this.setState({
                  mark: nativeEvent
                })
              }}
            >
              {this.state.mark ? <Marker
                position={{
                  latitude: this.state.mark.latitude,
                  longitude: this.state.mark.longitude
                }}
                icon={require("../../../assets/images/location100.png")}>
              </Marker> : null}
            </MapView>
          </Box>
          <Flex direction='row' mt='5' ml='auto' mr='auto'>
            <Button onPress={() => {
              const data = {
                name: this.state.name,
                figure: this.state.figure,
                introduction: this.state.introduction,
                brief: this.state.brief,
                position: JSON.stringify(this.state.mark),
              }
              if (data.name.length === 0 || data.name.length > 15) {
                Alert.alert('名称不符合要求');
                return;
              }
              if (data.figure.length === 0) {
                Alert.alert('未添加图片');
                return;
              }
              if (data.introduction.length === 0 || data.introduction.length > 512) {
                Alert.alert('介绍不符合要求');
                return;
              }
              if (data.brief.length === 0 || data.brief.length > 150) {
                Alert.alert('简介不符合要求');
                return;
              }
              if (data.position.length === 0) {
                Alert.alert('未选择位置');
                return;
              }
              spotAdminService.addShop(data).then(res => {
                Alert.alert('修改成功', '返回商铺列表', [{ text: '确定', onPress: () => { navigation.goBack(); } }])
              }).catch(err => {
                alert(err.message)
              })
            }} size='lg'> 确认新增商铺 </Button>
          </Flex>
        </ScrollView>
      </NativeBaseProvider>
    );
  }
}
