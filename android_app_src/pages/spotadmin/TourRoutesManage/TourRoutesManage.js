import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, HStack } from 'native-base'
import { AuthContext } from '../../../context/auth.context';
import spotService from '../../../service/spot.service';
import spotAdminService from '../../../service/spotAdmin.service';
import travelService from '../../../service/travel.service';

var isEnd = false;
var isCleaned = false;

function RouteList(props) {
  const content = props.content;
  const navigation = props.navi;
  const routeslist = content.map((routeinfo) => (
    <Flex key={routeinfo.id.toString()}>
      <Flex bg='blue.200' direction='row'>
        <Flex position='absolute' ml='10%' pt='5' pb='5'>{routeinfo.id}</Flex>
        <Flex position='absolute' ml='30%' pt='5' pb='5'>{routeinfo.name}</Flex>
        <Button ml='65%' pt='5' pb='5' onPress={() => {
          spotAdminService.deleteRoute(routeinfo.id).then(res => {
            alert("路线删除成功！");
          }).catch(err => {
            console.log(err)
            alert(err.message);
          })
        }}>删除</Button>
        <Button ml='5%' pt='5' pb='5' onPress={() => navigation.navigate('ChangeTourRoute', {id: routeinfo.id})}>修改</Button>
      </Flex>
      <Flex pt='5'></Flex>
    </Flex>
  ));
  return (
    <>{routeslist}</>
  );
}


export default class TourRoutesManage extends Component {
  static contextType = AuthContext;
  constructor(props) {
    super(props);
    this.page = 0;
    this.size = 5;
    this.state = {
      content: [],
      id: 1
    }
  }
  componentDidMount(){
    isCleaned = false;
    travelService.getRouteList(this.context.user.adminSpot.id, this.page, this.size).then(res => {
      isEnd = false;
      if (!isCleaned && res.content) {
        this.setState({
          content: res.content
        })
        isEnd = res.content.length < this.size
      } else {
        isEnd = true
      }
    }).catch(err => {
      alert(err.message)
    })
  }
  componentWillUnmount() {
    isCleaned = true;
  }

  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Flex ml='10%' pt='5' pb='5'>id</Flex>
          <Flex ml='20%' pt='5' pb='5'>路线名</Flex>
          <Flex ml='34%' pt='5' pb='5'>操作</Flex>
        </Flex>
        <Flex>
          <RouteList content={this.state.content} navi={navigation} />
        </Flex>
        <HStack space={5} justifyContent="center" mt='10'>
          <Button size='lg'
            onPress={() => {
              //需要重新发请求获取新一页景区列表
              if (this.page > 0) {
                this.page -= 1;
                travelService.getRouteList(this.state.id, this.page, this.size).then(res => {
                  isEnd = false;
                  if(res.content){
                    if(res.content.length < this.size)
                      isEnd = true;
                  }
                  if (!isCleaned && res.content) {
                    this.setState({
                      content: res.content
                    })
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >上一页</Button>
          <Button size='lg'
            onPress={() => navigation.navigate('AddTourRoute', {id: this.state.id})}>添加游览路线</Button>
          <Button size='lg'
            onPress={() => {
              if (!isEnd) {
                this.page += 1;
                travelService.getRouteList(this.state.id, this.page, this.size).then(res => {
                  isEnd = false;
                  if(res.content){
                    if(res.content.length < this.size)
                      isEnd = true;
                  }
                  if (!isCleaned && res.content) {
                    this.setState({
                      content: res.content
                    })
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >下一页</Button>
        </HStack>
      </NativeBaseProvider>
    );
  }
}
