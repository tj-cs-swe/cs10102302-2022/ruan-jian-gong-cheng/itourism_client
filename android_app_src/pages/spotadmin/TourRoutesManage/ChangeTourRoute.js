import React, { Component, useState } from 'react'
import { NativeBaseProvider, Button, Text, Box, ScrollView, Input, Center, useToast, Heading, VStack, HStack } from 'native-base'
import {StyleSheet, Alert} from 'react-native'
import { MapView, MapType, Marker, Polyline } from "react-native-amap3d";
import spotAdminService from '../../../service/spotAdmin.service';

export default function ChangeTourRoute(props) {
  const [list, setList] = useState([]);
  const [coordinate, setCoordinate] = useState("");
  const [name, setName] = useState('');
  const [introduce, setIntroduce] = useState('');
  const [inputValue, setInputValue] = useState("");
  const id = props.route.params.id;
  var coord = ""
  var i = 0
  var routeData = {}

  const addItem = title => {
    if(title == '')
      return
    setList(prevList => {
      return [...prevList, title];
    });
  };

  const handleDelete = index => {
    setList(prevList => {
      const temp = prevList.filter((_, itemI) => itemI !== index);
      return temp;
    });
  };

  const getroute = () => {
    console.log(coordinate)
    var data = {
      id: id,
      name: name,
      introduce: introduce,
      route: coordinate
    }
    console.log(data)
    spotAdminService.editRoute(data).then(res=>{
      alert('修改成功！')
    }).catch(err=>{
      alert(err.message)
    })
  }

  const submit = () => {
    //list:["1", "2", ...]
    list.map((point)=>{
      coord = coord + point;
      i = i + 1;
      if(i < list.length)
        coord = coord + ';'
    })
    console.log('coord:', coord)
    setCoordinate(coord)
  }

  return (
    <ScrollView>
      <Box p={5} borderTopRadius={20}>
        <Box>
          <HStack space={2}>
              <Input my="2" flex={1} onChangeText={v => setName(v)} value={name} placeholder="输入路线名称" />
            </HStack>
            <HStack space={2}>
              <Input flex={1} onChangeText={v => setIntroduce(v)} value={introduce} placeholder="输入路线简介" />
            </HStack>
          <Heading my="2" size="sm">
            添加景点id
          </Heading>
          <VStack space={4}>
            <HStack space={2}>
              <Input flex={1} onChangeText={v => setInputValue(v)} value={inputValue} placeholder="添加景点id" />
              <Button onPress={() => {
                addItem(inputValue);
                setInputValue("");
              }} >添加</Button>
            </HStack>
            
            <VStack space={2}>
              {list.map((item, itemI) => <HStack w="100%" justifyContent="space-between" alignItems="center" key={item.title + itemI.toString()}>
                <Text width="100%" flexShrink={1} textAlign="left" mx="2" _light={{
                  color: "gray.500"
                }} >
                  {item}
                </Text>
                <Button size="sm" colorScheme="trueGray" onPress={() => handleDelete(itemI)} >删除</Button>
              </HStack>)}
            </VStack>
          </VStack>
        </Box>
        <Box py={4}>
          <Box>
            <Button align={'center'} borderRadius={'20'} onPress={submit}>完成景点添加</Button>
          </Box>
        </Box>
        <Box py={4}>
          <Box>
            <Button align={'center'} borderRadius={'20'} onPress={getroute}>确认修改</Button>
          </Box>
        </Box>
      </Box>
    </ScrollView>
  );
}