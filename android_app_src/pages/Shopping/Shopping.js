import React, { Component } from 'react'
import { Text, Flex, Box, AspectRatio, Pressable, HStack, Spinner } from 'native-base'
import {
  View,
  FlatList,
  Dimensions,
  Image,
  TouchableOpacity,
  Alert
} from "react-native";
import shopService from '../../service/shop.service';
import { AuthContext } from '../../context/auth.context'

const { width, height } = Dimensions.get("window");
const rpx = (x) => (width / 750) * x;
var sid = -1;

export default class Shopping extends Component {
  static contextType = AuthContext;
  constructor(props) {
    super(props);
    this._navListener = this.props.navigation.addListener('focus', this.fetchData);
    this._mavListener = this.props.navigation.addListener('blur', this.leave);
    this.state = {
      page: 0,
      size: 5,
      shops: [],
      showBtn: false,
      ifEnd: false
    }
  }

  componentWillUnmount() {
    this.setState({ page: 0, shops: [], ifEnd: false });
    this._navListener();
    this._mavListener();
  }

  fetchData = () => {
    console.log("hi");
    if (!this.state.ifEnd) {
      shopService.getSceneShops(this.context.spotid, this.state.page, this.state.size).then((res) => {
        if (!res.content) {
          this.setState({ ifEnd: true });
          return;
        }
        var result = [...this.state.shops, ...res.content];
        // console.log(result);
        this.setState({ shops: result });
      }).catch((err) => {
        Alert.alert(err.message);
      })

      this.setState({ page: this.state.page + 1 });
    }
  }

  leave = () => {
    this.setState({
      ifEnd: false,
      page: 0,
      shops: [],
      showBtn: false
    });
  }

  render() {
    return (
      <View height='100%'>
        <FlatList
          data={this.state.shops}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index}
          ListFooterComponent={this._ListFooterComponent}
          onScroll={this._onScroll}
          ref={(el) => (this.f1 = el)}
          onEndReached={this._onEndReached}
          onEndReachedThreshold={0.1}
        ></FlatList>

        {this.state.showBtn ? (
          <TouchableOpacity
            onPress={() => this.f1.scrollToIndex({ index: 0 })}
            activeOpacity={0.7}
            style={{
              width: rpx(150),
              height: rpx(150),
              position: "absolute",
              bottom: rpx(100),
              right: rpx(50),
              backgroundColor: "gray",
              borderRadius: rpx(75),
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Text style={{ fontSize: rpx(30), color: "white" }}>回到顶部</Text>
          </TouchableOpacity>
        ) : null}
      </View>
    )
  }

  _onScroll = (e) => {
    this.setState({ showBtn: e.nativeEvent.contentOffset.y > 50 });
  };

  _ListFooterComponent = () => (
    this.state.ifEnd ?
      <Text fontSize={20}
        color={'grey'}
        alignSelf={'center'}
        m='5%'>已经到底啦</Text> :
      <HStack space={2} justifyContent="center" my='5%'>
        <Spinner accessibilityLabel="Loading posts" />
        <Text color="primary.500" fontSize="md">
          Loading
        </Text>
      </HStack>
  );

  _onEndReached = () => {
    if (!this.state.ifEnd) {
      shopService.getSceneShops(this.context.spotid, this.state.page, this.state.size).then((res) => {
        if (!res.content) {
          this.setState({ ifEnd: true });
          return;
        }
        var result = [...this.state.shops, ...res.content];
        this.setState({ shops: result });
      }).catch((err) => {
        Alert.alert(err.message);
      })

      this.setState({ page: this.state.page + 1 });
    }
  };

  _renderItem = ({ item }) => {
    return (
      <Pressable onPress={() => {
        const { navigate } = this.props.navigation;
        navigate('Shop', { shid: item.id });
      }}>
        <Flex
          backgroundColor={'white'}
          direction='row'
          p='3%'
          align={'center'}
          m='2%'
          borderRadius={10}>
          <AspectRatio w="40%" ratio={4 / 3}>
            <Image source={{ uri: item.figure }}
              alt="商铺图片"
              borderRadius={10}></Image>
          </AspectRatio>
          <Box direction='column' mx='3' w='60%'>
            <Text fontSize={25}>{item.name}</Text>
            <Text fontSize={18} flexGrow={1}>{item.brief}</Text>
          </Box>
        </Flex>
      </Pressable>
    );
  };
}