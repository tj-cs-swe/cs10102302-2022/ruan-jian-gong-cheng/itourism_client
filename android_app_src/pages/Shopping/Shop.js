import React, { Component } from 'react'
import { NativeBaseProvider, HStack, Spinner, Text, Flex, Box, Image, AspectRatio, Pressable, Center, ScrollView } from 'native-base'
import {
  View,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Alert
} from "react-native";
import shopService from '../../service/shop.service';

const { width, height } = Dimensions.get("window");
const rpx = (x) => (width / 750) * x;
const itemWidth = (width) / 2;

export default class Shop extends Component {
  constructor(props) {
    super(props);
    const shid = this.props.route.params.shid;
    this.state = {
      shid,
      name: '',
      intro: '',
      pic: '',
      commodities: [],
      showBtn: false,
      ifEnd: false,
      page: 0,
      size: 5
    }
  }

  componentDidMount() {
    shopService.getShop(this.state.shid).then(res => {
      this.setState({
        name: res.content.name,
        intro: res.content.introduction,
        pic: res.content.figure
      })
    })

    if (!this.state.ifEnd) {
      shopService.getShopItems(this.state.shid, this.state.page, this.state.size).then((res) => {
        if (!res.content) {
          this.setState({ ifEnd: true })
          return;
        }
        var result = [...this.state.commodities, ...res.content];
        this.setState({ commodities: result });
      }).catch((err) => {
        Alert.alert(err.message);
      })

      this.setState({ page: this.state.page + 1 });
    }
  }

  componentWillUnmount() {
    this.setState({ page: 0, commodities: [], ifEnd: false });
  }

  _onScroll = (e) => {
    this.setState({ showBtn: e.nativeEvent.contentOffset.y > 200 });
  };

  // 头部组件
  _ListHeaderComponent = () => (
    <Box>
      {this.state.pic.length == 0 ? null :
        <AspectRatio w="90%" ratio={4 / 3} mt='5%' mx='5%'>
          <Image source={{ uri: this.state.pic }}
            alt="商店图片"
            borderRadius={5}></Image>
        </AspectRatio>}
      <Text fontSize={30} my='4%' mx='5%'>{this.state.name}</Text>
      <Text fontSize={20} mx='5%'>店铺介绍：{this.state.intro}</Text>
      {/* 商品显示部分 */}
      <Box backgroundColor={'#rgb(176,224,230)'}
        mt='5%'
        mb='3%'
        w='50%'
        borderRadius={40}
        alignSelf='center'>
        <Text fontSize={25} my='5%' alignSelf='center'> 商品列表</Text>
      </Box>
    </Box>
  );

  _ListFooterComponent = () => (
    this.state.ifEnd ?
      <Text fontSize={20}
        color={'grey'}
        alignSelf={'center'}
        m='5%'>已经到底啦</Text> : <HStack space={2} justifyContent="center">
        <Spinner accessibilityLabel="Loading posts" />
        <Text color="primary.500" fontSize="md">
          Loading
        </Text>
      </HStack>
  );

  _onEndReached = () => {
    if (!this.state.ifEnd) {
      shopService.getShopItems(this.state.shid, this.state.page, this.state.size).then((res) => {
        if (!res.content) {
          this.setState({ ifEnd: true })
          return;
        }
        var result = [...this.state.commodities, ...res.content];
        this.setState({ commodities: result });
      }).catch((err) => {
        Alert.alert(err.message);
      })

      this.setState({ page: this.state.page + 1 });
    }
  };

  _renderItem = ({ item }) => {
    return (
      <Box w={itemWidth}>
        <Pressable onPress={() => {
          const { navigate } = this.props.navigation;
          navigate('Commodity', { cid: item.id });
        }}
          mt={3}
          mx={3}>
          <Flex
            backgroundColor={'white'}
            borderRadius={10}
            borderWidth={2}
            borderColor={'#rgb(135,206,235)'}>
            <AspectRatio w="80%" ratio={1 / 1} mt='8%' mb='2%' alignSelf='center'>
              <Image source={{ uri: item.figure }}
                alt="商品图片"></Image>
            </AspectRatio>
            <Box mx='7%'>
              <Text fontSize={20}>{item.name}</Text>
              <Text fontSize={15}>单价：{item.price}元</Text>
              <Text fontSize={15} mb='4%'>已售出{item.sales}个</Text>
            </Box>
          </Flex>
        </Pressable>
      </Box>
    );
  };

  render() {
    return (
      <NativeBaseProvider>
        <Box backgroundColor={'white'} h='100%'>
          <FlatList
            data={this.state.commodities}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index}
            ListHeaderComponent={this._ListHeaderComponent}
            ListFooterComponent={this._ListFooterComponent}
            onScroll={this._onScroll}
            ref={(el) => (this.f1 = el)}
            onEndReached={this._onEndReached}
            onEndReachedThreshold={0.1}
            numColumns={2}
          ></FlatList>
        </Box>

        {/* 回到顶部按钮 */}
        {this.state.showBtn ? (
          <TouchableOpacity
            onPress={() => this.f1.scrollToIndex({ index: 0 })}
            activeOpacity={0.7}
            style={{
              width: rpx(150),
              height: rpx(150),
              position: "absolute",
              bottom: rpx(100),
              right: rpx(50),
              backgroundColor: "gray",
              borderRadius: rpx(75),
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Text style={{ fontSize: rpx(30), color: "white" }}>回到顶部</Text>
          </TouchableOpacity>
        ) : null}
      </NativeBaseProvider>
    )
  }
}