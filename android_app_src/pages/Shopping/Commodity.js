import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Box, Image, AspectRatio, ScrollView, Toast } from 'native-base'
import {
  Dimensions,
  Alert
} from "react-native";
import shopService from '../../service/shop.service';
import { AuthContext } from '../../context/auth.context';

const { width, height } = Dimensions.get("window");

export default class Commodity extends Component {
  static contextType = AuthContext;
  constructor(props) {
    super(props);
    const cid = this.props.route.params.cid;
    this.state = {
      cid,
      name: '',
      intro: '',
      pic: '',
      price: 100,
      sales: 0,
      amount: 0,
    }
  }

  componentDidMount() {
    shopService.getItems(this.state.cid).then(res => {
      this.setState({
        name: res.content.name,
        intro: res.content.introduction,
        pic: res.content.figure,
        price: res.content.price,
        sales: res.content.sales
      })
    }).catch(err => {
      Alert.alert(err.message);
    })
  }

  componentWillUnmount() {
    this.setState({ amount: 0 });
  }

  purchase = () => {
    if (!this.context.user) {
      const { navigate } = this.props.navigation;
      navigate('Login');
      return;
    }
    if (this.state.amount > 0) {
      var data = {
        id: this.state.cid,
        amount: this.state.amount
      }

      shopService.buyItems(data).then(res => {
        const { navigate } = this.props.navigation;
        navigate('Pay', { html: res.content.html });
        console.log(res.content.html);
      }).catch(err => {
        Alert.alert(err.message);
      })
    }
    else {
      Toast.show({
        title: '请选择购买数量',
        status: 'warning'
      });
    }
  }

  render() {
    return (
      <NativeBaseProvider>
        <Box backgroundColor={'white'} h='100%'>
          <ScrollView>
            {this.state.pic.length == 0 ? null :
              <AspectRatio w="80%"
                ratio={4 / 3}
                alignSelf='center'
                mt={0.1 * width}>
                <Image source={{ uri: this.state.pic }}
                  alt="商品图片"
                  borderRadius={10}></Image>
              </AspectRatio>}
            <Box mx='3'>
              <Text fontSize={30} alignSelf={'center'} my={3}>{this.state.name}</Text>
              <Flex direction='row' justifyContent={'space-between'} mx={0.1 * width}>
                <Text fontSize={20}>单价：{this.state.price}元</Text>
                <Text fontSize={20}>已售出{this.state.sales}个</Text>
              </Flex>
              <Text fontSize={20} mx={0.1 * width} my={3}>简介：{this.state.intro}</Text>
            </Box>
          </ScrollView>
          <Flex direction='row' h={0.1 * width}
            ml={0.15 * width}
            my={0.1 * width}>
            <Flex direction='row'>
              <Button
                disabled={this.state.amount ? 0 : 1}
                backgroundColor={this.state.amount ? 'rgb(8,145,178)' : 'rgba(8,145,178,0.7)'}
                w={0.1 * width}
                borderLeftRadius={20}
                borderRightRadius={0}
                onPress={() => {
                  if (this.state.amount > 0) {
                    this.setState({ amount: this.state.amount - 1 });
                  }
                }}>-</Button>
              {/* <TextArea
                isDisabled={1}
                borderRadius={0}
                value={this.state.amount + ''}
                w={0.12 * width}
                h={0.1 * width}
                fontSize={15}
                color='black'></TextArea> */}
              <Box borderRadius={0}
                w={0.12 * width}
                h={0.1 * width}
                alignItems={'center'}
                justifyContent={'center'}>
                <Text fontSize={20}>{this.state.amount}</Text>
              </Box>
              <Button
                w={0.1 * width}
                borderLeftRadius={0}
                borderRightRadius={20}
                onPress={() => {
                  this.setState({ amount: this.state.amount + 1 });
                }}>+</Button>
            </Flex>
            <Button ml={0.1 * width} w={0.3 * width}
              onPress={this.purchase}>立即购买</Button>
          </Flex>
        </Box>
      </NativeBaseProvider>
    )
  }
}