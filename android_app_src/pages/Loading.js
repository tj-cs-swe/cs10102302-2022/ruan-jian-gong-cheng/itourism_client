import React from 'react'

import { Box, Spinner, VStack, Text } from 'native-base'

export default function Loading() {
  return (
    <Box height={'100%'} justifyContent='center'>
      <VStack space={3}>
        <Spinner size='lg'></Spinner>
        <Text textAlign={'center'}>Loading</Text>
      </VStack>
    </Box>
  )
}