import React, { Component, useState, useContext, useEffect } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, FlatList, HStack, Spinner, Box, AspectRatio, VStack, AlertDialog } from 'native-base'
import { TouchableOpacity, Dimensions } from 'react-native'
import { AuthContext } from '../../context/auth.context';
import shopService from '../../service/shop.service';
import shopAdminService from '../../service/shopAdmin.service';
const { width, height } = Dimensions.get("window");
const rpx = (x) => (width / 750) * x;
const size = 5;
var isCleaned = false;

export default function CommodityManage(props) {
  const context = useContext(AuthContext);
  const [commodities, setCommodities] = useState([]);
  const [page, setPage] = useState(0);
  const [ifEnd, setIfEnd] = useState(false);
  const [showBtn, setShowBtn] = useState(false);
  const [showDlg, setShowDlg] = useState(false);
  const [showInfo, setShowInfo] = useState({ name: '' });

  const cancelRef = React.useRef(null);

  var f1;

  useEffect(() => {
    isCleaned = false;
    
    if (!ifEnd) {
      shopService.getShopItems(context.user.shop.id, page, size).then((res) => {
        if (!isCleaned) {
          if (!res.content) {
            setIfEnd(true);
            return;
          }
          var result = [...commodities, ...res.content];
          setCommodities(result);
        }
      });

      if (!isCleaned) {
        setPage(page + 1);
      }
    }

    return (() => { isCleaned = true; })
  }, []);

  const _ListFooterComponent = () => (
    ifEnd ?
      <Text fontSize={20}
        color={'grey'}
        alignSelf={'center'}
        m='5%'>已经到底啦</Text> :
      <HStack space={2} justifyContent="center" m='5%'>
        <Spinner accessibilityLabel="Loading posts" size={'sm'} />
        <Text color="primary.500" fontSize="lg">Loading</Text>
      </HStack>
  );

  const _onEndReached = () => {
    if (!ifEnd) {
      shopService.getShopItems(context.user.shop.id, page, size).then((res) => {
        if (!isCleaned) {
          if (!res.content) {
            setIfEnd(true);
            return;
          }
          var result = [...commodities, ...res.content];
          setCommodities(result);
        }
      });

      if (!isCleaned) {
        setPage(page + 1);
      }
    }
  };

  const _onScroll = (e) => {
    if (!isCleaned) {
      setShowBtn(e.nativeEvent.contentOffset.y > 200);
    }
  };

  const onClose = () => { if (!isCleaned) { setShowDlg(false); } };

  const deleteCommdodity = () => {
    shopAdminService.deleteItem(showInfo.id).then(res => {
      if (!isCleaned) {
        setCommodities(prevList => {
          const temp = prevList.filter((item) => item.id !== showInfo.id);
          return temp;
        });

        setShowDlg(false);
      }
    })
  };

  const _renderItem = ({ item }) => {
    return (
      <Box mx='5%' my='3%' bgColor={'white'} borderRadius={10}>
        <HStack space={2} p='5%'>
          <AspectRatio w='40%' ratio={1 / 1} alignSelf={'center'}>
            <Image source={{ uri: item.figure }}
              alt='商品图片'
              alignSelf={'center'}></Image>
          </AspectRatio>
          <VStack space={3} px='3%'>
            <Text fontSize={25} flexGrow={1}>{item.name}</Text>
            <HStack space={2} justifyContent={'space-between'}>
              <Button onPress={() => {
                const { navigate } = props.navigation;
                navigate('EditCommodity', { cid: item.id });
              }}>编辑商品</Button>
              <Button colorScheme="red" onPress={() => {
                if (!isCleaned) {
                  setShowDlg(true);
                  setShowInfo(item);
                }
              }}>删除商品</Button>
            </HStack>
          </VStack>
        </HStack>
      </Box >
    );
  };

  return (
    <NativeBaseProvider>
      <FlatList
        data={commodities}
        renderItem={_renderItem}
        keyExtractor={(item, index) => index}
        ListFooterComponent={_ListFooterComponent}
        onScroll={_onScroll}
        ref={(el) => (f1 = el)}
        onEndReached={_onEndReached}
        onEndReachedThreshold={0.1}
      ></FlatList>

      <AlertDialog leastDestructiveRef={cancelRef} isOpen={showDlg} onClose={onClose}>
        <AlertDialog.Content>
          <AlertDialog.CloseButton />
          <AlertDialog.Header>删除商品</AlertDialog.Header>
          <AlertDialog.Body>
            <Text fontSize={15}>确认要删除商品"{showInfo.name}"吗？</Text>
          </AlertDialog.Body>
          <AlertDialog.Footer>
            <Button.Group space={2}>
              <Button variant="unstyled" colorScheme="coolGray" onPress={onClose} ref={cancelRef}>
                取消
              </Button>
              <Button colorScheme="danger" onPress={deleteCommdodity}>
                确认
              </Button>
            </Button.Group>
          </AlertDialog.Footer>
        </AlertDialog.Content>
      </AlertDialog>

      {/* 回到顶部按钮 */}
      {showBtn ? (
        <TouchableOpacity
          onPress={() => f1.scrollToIndex({ index: 0 })}
          activeOpacity={0.7}
          style={{
            width: rpx(150),
            height: rpx(150),
            position: "absolute",
            bottom: rpx(100),
            right: rpx(50),
            backgroundColor: "gray",
            borderRadius: rpx(75),
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Text style={{ fontSize: rpx(30), color: "white" }}>回到顶部</Text>
        </TouchableOpacity>
      ) : null}
    </NativeBaseProvider>
  )
}
