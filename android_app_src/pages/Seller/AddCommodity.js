import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, VStack, HStack, Input, ScrollView, View, AspectRatio, TextArea, Toast, Alert, IconButton } from 'native-base'
import { launchImageLibrary } from 'react-native-image-picker'
import { AuthContext } from '../../context/auth.context'
import shopAdminService from '../../service/shopAdmin.service';

export default class AddCommodity extends Component {
  static contextType = AuthContext;
  constructor(props) {
    super(props);
    this.state = {
      ifUpload: false,
      pic: '',
      name: '',
      intro: '无',
      price: '',
      ifSubmit: false
    }
  }

  number = /(^[1-9]\d*(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/;

  addCom = () => {
    if (this.state.name === '' || this.state.price === '' || this.state.pic === '' || this.state.intro === '') {
      Toast.show({
        title: "信息不能为空",
        status: "warning"
      });
      return;
    }

    var p = parseFloat(this.state.price);
    var data = {
      name: this.state.name,
      figure: this.state.pic,
      introduction: this.state.intro,
      price: p,
      shop: { id: this.context.user.shop.id }
    }
    if (data.name.length > 10) {
      Toast.show({
        title: "名称不符合要求",
        status: "warning"
      });
      return;
    }
    if (data.introduction.length > 512) {
      Toast.show({
        title: "介绍不符合要求",
        status: "warning"
      });
      return;
    }
    console.log(data);
    shopAdminService.addItem(data).then(res => {
      this.setState({ ifSubmit: true });
    }).catch(err => {
      console.log(err.message);
    })
  }

  render() {
    return (
      <NativeBaseProvider>
        {this.state.ifSubmit ?
          <Alert w='100%' status={'success'} borderRadius={10}>
            <VStack space={2} flexShrink={1} w="100%">
              <HStack flexShrink={1} space={2} justifyContent="space-between">
                <HStack space={2} flexShrink={1}>
                  <Alert.Icon mt="1" />
                  <Text fontSize="md" color="coolGray.800">
                    商品添加成功！
                  </Text>
                </HStack>
              </HStack>
            </VStack>
          </Alert>
          : null}
        <ScrollView>
          <VStack space={6} alignItems={'center'}>
            <HStack space={2} justifyContent={'center'} alignItems={'center'} mt='10%'>
              <Text fontSize={20}>名称：</Text>
              <Input w='50%' onChangeText={(text) => {
                this.setState({ name: text });
              }}
                backgroundColor={"white"}
                fontSize={20}></Input>
            </HStack>
            <HStack space={2} justifyContent={'center'} alignItems={'center'} mt='5%'>
              <Text fontSize={20}>价格：</Text>
              <Input w='50%'
                backgroundColor={"white"}
                keyboardType={'numeric'}
                value={this.state.price}
                onBlur={() => {
                  if (this.state.price.length == 0) {
                    Toast.show({
                      title: "价格不能为空",
                      status: "warning",
                    });
                  }
                  else {
                    if (!this.number.test(this.state.price)) {
                      this.setState({ price: '' });
                      Toast.show({
                        title: "价格格式错误",
                        status: "warning",
                      });
                    }
                  }
                }}
                onChangeText={(num) => {
                  this.setState({ price: num });
                }}
                fontSize={20}></Input>
            </HStack>
            <HStack space={2} justifyContent={'center'} my='5%'>
              <Text fontSize={20}>介绍：</Text>
              <TextArea w='50%'
                backgroundColor={"white"}
                onChangeText={(text) => {
                  this.setState({ intro: text });
                }}
                fontSize={15}></TextArea>
            </HStack>
            {this.state.ifUpload ?
              <AspectRatio w='40%' ratio={1 / 1} alignSelf={'center'}>
                <Image source={{ uri: this.state.pic }}
                  alt='商品图片'
                  alignSelf={'center'}></Image>
              </AspectRatio>
              : null}
            <HStack space={1}>
              <Button w='40%' onPress={() => {
                launchImageLibrary({
                  mediaType: 'photo',
                  includeBase64: true,
                  selectionLimit: 1
                }, resPhoto => {
                  if (resPhoto.didCancel) return;
                  const resBase64 = 'data:image/jpeg;base64,' + resPhoto.assets[0].base64;
                  this.setState({ pic: resBase64, ifUpload: true });
                })
              }}>上传图片</Button>
            </HStack>
            <Button w='70%' my='10%' onPress={this.addCom}>添加商品</Button>
          </VStack>
        </ScrollView>
      </NativeBaseProvider>
    )
  }
}
