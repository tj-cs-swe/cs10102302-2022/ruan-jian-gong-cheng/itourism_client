import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, VStack, HStack, Input, ScrollView, View, AspectRatio, TextArea, Toast, Alert, IconButton } from 'native-base'
import { launchImageLibrary } from 'react-native-image-picker'
import { AuthContext } from '../../context/auth.context'
import shopAdminService from '../../service/shopAdmin.service';
import shopService from '../../service/shop.service';

var isCleaned = false;

export default class EditShop extends Component {
  static contextType = AuthContext;
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      brief: '',
      intro: '',
      figure: '',
      ifSubmit: false
    }
  }

  componentDidMount() {
    isCleaned = false;
    if (!isCleaned) {
      shopService.getShop(this.context.user.shop.id).then(res => {
        this.setState({
          name: res.content.name,
          brief: res.content.brief,
          intro: res.content.introduction,
          figure: res.content.figure
        })
      })
    }
  }

  componentWillUnmount() {
    isCleaned = true;
  }

  SaveEdit = () => {

    var data = {
      id: this.context.user.shop.id,
      name: this.state.name,
      brief: this.state.brief,
      intro: this.state.intro,
      figure: this.state.figure
    }
    if (data.name.length === 0 || data.name.length > 15) {
      Toast.show({ title: '名称不符合要求' });
      return;
    }
    if (data.figure.length === 0) {
      Toast.show({ title: '未添加图片' });
      return;
    }
    if (data.intro.length === 0 || data.intro.length > 512) {
      Toast.show({ title: '介绍不符合要求' });
      return;
    }
    if (data.brief.length === 0 || data.brief.length > 150) {
      Toast.show({ title: '简介不符合要求' });
      return;
    }
    //更新商店信息
    shopAdminService.editShop(data).then(res => {
      this.setState({ ifSubmit: true });
    }).catch(err => {
      console.log(err);
      Toast.show({
        title: "编辑失败！"
      })
    })
  }

  render() {
    return (
      <NativeBaseProvider>
        {this.state.ifSubmit ?
          <Alert w='100%' status={'success'} borderRadius={10}>
            <VStack space={2} flexShrink={1} w="100%">
              <HStack flexShrink={1} space={2} justifyContent="space-between">
                <HStack space={2} flexShrink={1}>
                  <Alert.Icon mt="1" />
                  <Text fontSize="md" color="coolGray.800">
                    编辑已保存！
                  </Text>
                </HStack>
              </HStack>
            </VStack>
          </Alert>
          : null}

        <ScrollView>
          <VStack space={2}>
            <HStack space={2} justifyContent={'center'} alignItems={'center'} mt='10%'>
              <Text fontSize={20}>店铺名称：</Text>
              <Input w='50%'
                backgroundColor={'white'}
                fontSize={20}
                value={this.state.name}
                onChangeText={(text) => {
                  this.setState({ name: text })
                }}></Input>
            </HStack>
            <HStack space={2} justifyContent={'center'} alignItems={'center'} mt='10%'>
              <Text fontSize={20}>店铺简介：</Text>
              <Input w='50%'
                backgroundColor={'white'}
                fontSize={20}
                value={this.state.brief}
                onChangeText={(text) => {
                  this.setState({ brief: text })
                }}></Input>
            </HStack>
            <HStack space={2} justifyContent={'center'} mt='10%'>
              <Text fontSize={20}>店铺介绍：</Text>
              <TextArea w='50%'
                backgroundColor={"white"}
                fontSize={17}
                value={this.state.intro}
                onChangeText={(text) => {
                  this.setState({ intro: text });
                }}
              ></TextArea>
            </HStack>

            {this.state.figure.length ?
              <AspectRatio w='50%' ratio={1 / 1} alignSelf={'center'} mt='10%'>
                <Image source={{ uri: this.state.figure }}
                  alt='商品图片'
                  alignSelf={'center'}></Image>
              </AspectRatio> : null}

            <HStack space={2} justifyContent={'center'} mt='5%'>
              <Button w='40%' onPress={() => {
                launchImageLibrary({
                  mediaType: 'photo',
                  includeBase64: true,
                  selectionLimit: 1
                }, resPhoto => {
                  if (resPhoto.didCancel) return;
                  const resBase64 = 'data:image/jpeg;base64,' + resPhoto.assets[0].base64;
                  this.setState({ pic: resBase64 });
                  console.log(resBase64);
                })
              }}>修改图片</Button>
            </HStack>
            <Button w='60%' my='10%' alignSelf={'center'} onPress={this.SaveEdit}>保存修改</Button>
          </VStack>
        </ScrollView>
      </NativeBaseProvider>
    )
  }
}
