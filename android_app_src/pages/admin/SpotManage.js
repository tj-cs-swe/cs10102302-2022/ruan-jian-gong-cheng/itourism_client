import React, { Component, useState, useEffect } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Box, Select, HStack } from 'native-base'
import { Alert } from 'react-native'
import adminService from '../../service/admin.service'

var isEnd = false;
var isCleaned = false;

function SpotList(props) {
  const content = props.content;

  const spotslist = content.map((spotinfo) => (
    <Flex key={spotinfo.id.toString()}>
      <Flex bg='blue.200' direction='row'>
        <Flex position='absolute' ml='10%' pt='5' pb='5'>{spotinfo.id}</Flex>
        <Flex position='absolute' ml='30%' pt='5' pb='5'>{spotinfo.name}</Flex>
        <Button ml='75%' pt='5' pb='5' onPress={() => {
          adminService.deleteSpot(spotinfo.id).then(res => {
            alert("景区删除成功！");
          }).catch(err => {
            alert(err.message);
          })
        }}>删除景区</Button>
      </Flex>
      <Flex pt='5'></Flex>
    </Flex>
  ));
  return (
    <>{spotslist}</>
  );
}

export default class SpotManage extends Component {
  constructor(props) {
    super(props);
    this.page = 0;
    this.size = 5;
    this.state = {
      content: []
    }
  }

  componentDidMount() {
    isCleaned = false;
    adminService.getSpotList(this.page, this.size).then(res => {
      isEnd = false;
      if (res.content.length < this.size)
        isEnd = true;
      if (!isCleaned && res.content) {
        this.setState({
          content: res.content
        })
      }
    }).catch(err => {
      alert(err.message)
    })
  }

  componentWillUnmount() {
    isCleaned = true;
  }

  render() {
    const { navigation } = this.props;
    const { goBack } = this.props.navigation;

    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Flex ml='10%' pt='5' pb='5'>id</Flex>
          <Flex ml='20%' pt='5' pb='5'>景区名</Flex>
          <Flex ml='38%' pt='5' pb='5'>操作</Flex>
        </Flex>
        <Flex>
          <SpotList content={this.state.content} />
        </Flex>

        <HStack space={5} justifyContent="center" mt='10'>
          <Button size='lg'
            onPress={() => {
              //需要重新发请求获取新一页景区列表
              if (this.page > 0) {
                this.page -= 1;
                adminService.getSpotList(this.page, this.size).then(res => {
                  isEnd = false;
                  if (res.content.length < this.size)
                    isEnd = true;
                  if (!isCleaned && res.content) {
                    this.setState({
                      content: res.content
                    })
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >上一页</Button>
          <Button size='lg'
            onPress={() => navigation.navigate('AddSpot')}
          >增添新景区</Button>
          <Button size='lg'
            onPress={() => {
              if (!isEnd) {
                this.page += 1;
                adminService.getSpotList(this.page, this.size).then(res => {
                  isEnd = false;
                  if (res.content.length < this.size)
                    isEnd = true;
                  if (!isCleaned && res.content) {
                    this.setState({
                      content: res.content
                    })
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >下一页</Button>
        </HStack>
      </NativeBaseProvider>
    );
  }
}
