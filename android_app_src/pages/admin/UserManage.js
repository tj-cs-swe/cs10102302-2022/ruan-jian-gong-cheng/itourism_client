import React, { Component, useState, useEffect } from 'react'
import {
  Button,
  Text,
  Flex,
  Box,
  Select,
  Input,
  HStack,
  ScrollView
} from 'native-base'
import adminService from '../../service/admin.service';
import { Alert } from 'react-native'

var isCleaned = false;

function UserList(props) {
  const content = props.content
  let tempPermission = ''
  let tempId = ''

  const userlist = content.map((userInfo) =>
    <Flex key={userInfo.id} ml="5%" mr="5%" mt="2%" mb="2%" bg='blue.200' rounded="xl">
      <Flex direction='row' m="2%">

        <Text style={{flex: 1}} fontSize="lg" textAlign="center">{userInfo.id}</Text>
        <Text style={{flex: 2}} fontSize="lg" textAlign="center">{userInfo.username}</Text>

        <Box style={{flex: 2}}>
          <Select bg='blue.100' fontSize='xs' placeholder={
            (userInfo.permissions[0].name === 'ROLE_ban') ? '封禁' : (
                (userInfo.permissions[0].name === 'ROLE_user') ? '普通用户' : (
                    (userInfo.permissions[0].name === 'ROLE_seller') ? '店家' : (
                        (userInfo.permissions[0].name === 'ROLE_spot_admin') ? '景区管理员' : '系统管理员'
                    )
                )
            )
          }
            onValueChange={(text) => {
              tempPermission = text
            }}>
            <Select.Item label='封禁' value='11' />
            <Select.Item label='普通用户' value='1' />
            <Select.Item label='店家' value='2' />
            <Select.Item label='景区管理员' value='3' />
            <Select.Item label='系统管理员' value='4' />
          </Select>
        </Box>

        <Button style={{flex: 1}} ml="1%" onPress={() => {
          const data = {
            users_id: userInfo.id,
            permissions_id: (tempPermission === '' ? userInfo.permissions[0].id : tempPermission),
            manage_id: tempId
          }
          if(data.manage_id.length === 0 && (data.permissions_id === '2' || data.permissions_id === '3')){
            Alert.alert("需要分配景区或商店");
            return;
          }
          adminService.permissionUser(data).then(res => {
            tempPermission = ''
            tempId = ''
            alert("修改成功！")
          }).catch(err => {
            alert(err.message)
          })
        }}>确认</Button>
      </Flex>
      <Flex direction="row" m="2%">
        <Text fontSize="lg" style={{flex: 2}}> 分配的景区或商店id </Text>
        <Box style={{flex: 1}}>
          <Input borderColor="blue.400" onChangeText={(text) => { tempId = text }}/>
        </Box>
      </Flex>

    </Flex>
  );
  return (
    <>{userlist}</>
  );
}

export default class UserManage extends Component {
  constructor(props) {
    super(props);
    this.size = 5
    this.page = 0
    this.state = {
      isEnd: false,
      content: []
    }
  }

  componentDidMount() {
    isCleaned = false;
    this.fetchData(false)
  }

  componentWillUnmount() {
    isCleaned = true;
  }

  fetchData = (inc) => {
    let page = inc ? this.page + 1 : this.page
    adminService.getUserList(page, this.size).then(res => {
      if (isCleaned) {
        return
      }
      if (!res.content) {
        this.setState({ isEnd: true })
        return
      }
      this.setState({
        content: res.content,
        isEnd: res.content.length < this.size
      })
      this.page = page
    }).catch(err => {
      // console.log(err);
      alert(err.message)
    })
  }

  render() {
    return (
        <ScrollView>
        <Flex>
          <UserList content={this.state.content} />
        </Flex>
        <HStack space={5} justifyContent="center" mt="5%" mb="5%">
          <Button size='lg'
            onPress={() => {
              //需要重新发请求获取新一页用户列表
              if (this.page <= 0) return
              this.page -= 1
              this.fetchData(false)
            }}
          >上一页</Button>
          <Button size='lg' disabled={this.state.isEnd}
            onPress={() => {
              this.fetchData(true)
            }}
          >下一页</Button>
        </HStack>
      </ScrollView>
    );
  }
}
