import { Alert } from 'react-native'
import React, { Component } from 'react'
import { NativeBaseProvider, Text, Box, Input, Icon, Button, Pressable } from 'native-base'
import Ionicons from 'react-native-vector-icons/Ionicons'
import authService from '../service/auth.service'

export default class Forget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //基本信息
      email: '',
      code: '',
      pwd: '',
      isSent: false, //是否已发送验证码
      sentTime: 60,  //发送验证码间隔
      showPwd: false, //控制密码显示方式
      showCfPwd: false
    }
  }

  //点击空白失去焦点
  blurText = () => {
    this.refs.email.blur();
    this.refs.code.blur();
    this.refs.pwd.blur();
  }

  //邮箱格式正则表达式
  emailCode = /^([a-zA-Z\d])(\w|\-)+@[a-zA-Z\d]+\.[a-zA-Z]{2,4}$/;

  //切换密码显示方式
  setShow = (choice) => {
    if (choice == 'pwd')
      this.setState({ showPwd: !this.state.showPwd });
    else
      this.setState({ showCfPwd: !this.state.showCfPwd });
  }

  //已发送验证码
  setIsSent = () => {
    if (this.emailCode.test(this.state.email)) {
      this.setState({ isSent: true });

      //显示倒计时
      let id = setInterval(
        () => {
          //获取原状态
          let sentTime = this.state.sentTime;
          //显示减一秒
          sentTime -= 1;
          if (sentTime <= 0) {  //重置计时
            sentTime = 60;
            this.setState({ isSent: false });
            //跳出循环
            clearInterval(id);
          }
          this.setState({ sentTime })
        }, 1000 //每隔一秒
      );

      const data = {
        email: this.state.email
      }

      authService.getVerifyCode(data).then(res => {
        Alert.alert('发送成功', '已发送验证码！')
      }).catch(err => {
        Alert.alert('发送失败', err)
      })
    }
    else {
      Alert.alert('发送失败', '邮箱格式有误！')
    }
  }

  onEmailChanged = (newEmail) => {
    this.setState({ email: newEmail });
  }

  onPwdChanged = (newPwd) => {
    this.setState({ pwd: newPwd });
  }

  onCodeChanged = (newCode) => {
    this.setState({ code: newCode });
  }

  //解决组件销毁后设置state的问题
  componentWillUnmount() {
    this.setState = (state, callback) => {
      return;
    }
  }

  confirmPwd = () => {
    if (this.state.email != '' && this.state.code != '' && this.state.pwd != '') {
      if (this.emailCode.test(this.state.email)) {
        const data = {
          email: this.state.email,
          captcha: this.state.code,
          password: this.state.pwd
        }
        // console.log(data)
        authService.forgetPassword(data).then(res => {
          const { navigate } = this.props.navigation;
          Alert.alert('重置成功', '返回登录页！', [{ text: '确定', onPress: () => { navigate('Login'); } }])
        }).catch(err => {
          Alert.alert(err.message);
        })
      }
      else {
        Alert.alert('重置失败', '邮箱格式有误！')
      }
    }
    else {
      Alert.alert('重置失败', '输入信息不可为空！')
    }
  }

  render() {
    return (
      <NativeBaseProvider>
        <Pressable flex={1} onPress={this.blurText}>
          <Box flex={1} alignItems="center" bg='#fff' justifyContent="center">
            <Text fontSize='38' fontWeight='bold' color='#000' position='relative' top='-10'>找回密码</Text>
            <Input  //邮箱输入框
              ref='email'
              onChangeText={(text) => this.onEmailChanged(text)}
              _light={{ bg: 'coolGray.100' }}
              _dark={{ bg: 'coolGray.800' }}
              placeholder='邮箱'
              w='75%'
              size='xl'
              my={'2'}
              InputLeftElement={<Icon as={<Ionicons name='mail' />} size={5} ml='5' mr='3' color="muted.400" />}
            />

            <Input  //验证码输入框+发送验证码
              ref='code'
              onChangeText={(text) => this.onCodeChanged(text)}
              _light={{ bg: 'coolGray.100' }}
              _dark={{ bg: 'coolGray.800' }}
              placeholder='验证码'
              w='75%'
              size='xl'
              my={'2'}
              InputLeftElement={<Icon as={<Ionicons name='mail-open' />} size={5} ml='5' mr='3' color="muted.400" />}
              InputRightElement={<Button
                margin='1'
                padding='2'
                bg='#58812F'
                opacity={this.state.isSent ? '0.6' : '1.0'}
                isDisabled={this.state.isSent ? true : false}
                onPress={() => this.setIsSent()}
              >{this.state.isSent ? this.state.sentTime.toString() + 's后重新发送' : '发送验证码'}</Button>}
            />

            <Input  //密码输入框
              ref='pwd'
              onChangeText={(text) => this.onPwdChanged(text)}
              _light={{ bg: 'coolGray.100' }}
              _dark={{ bg: 'coolGray.800' }}
              placeholder='密码'
              w='75%'
              size='xl'
              my={'2'}
              InputLeftElement={<Icon as={<Ionicons name='lock-closed' />} size={5} ml='5' mr='3' color="muted.400" />}
              type={this.state.showPwd ? 'text' : 'password'}
              InputRightElement={<Icon as={<Ionicons name={this.state.showPwd ? 'eye' : 'eye-off'} />} onPress={() => this.setShow('pwd')} size={5} ml='5' mr='3' color="muted.400" />}
            />

            <Button
              position='relative'
              top='5'
              w='75%'
              bg='#58812F'
              h='12'
              size='lg'
              onPress={this.confirmPwd}>重置密码</Button>
          </Box>
        </Pressable>
      </NativeBaseProvider>
    )
  }
}