import { Alert, View } from 'native-base';
import React, { Component } from 'react'
import { WebView } from 'react-native-webview'

export default class Commodity extends Component {
  constructor(props) {
    super(props);
    const html = this.props.route.params.html;
    this.state = {
      html
    }
  }

  render() {
    return(
      <View w='100%' h='100%'>
      <WebView source={{ html: this.state.html }}
        sharedCookiesEnabled={true}
        startInLoadingState={true}
        onError={() => {
          Alert.alert('网络连接失败！');
        }}
      /></View>
    )
  }
}