import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image } from 'native-base'
import { AuthContext } from '../context/auth.context'
//系统管理员
import UserManage from './admin/UserManage';
import SpotManage from './admin/SpotManage';
//景区管理员
import SpotInfoManage from './spotadmin/SpotManage/SpotInfoManage';
import ServicePointsManage from './spotadmin/ServicePointManage/ServicePointsManage';
import TourRoutesManage from './spotadmin/TourRoutesManage/TourRoutesManage';
import ShopsManage from './spotadmin/ShopsManage/ShopsManage';

export default class Management extends Component {
  static contextType = AuthContext;
  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        {
          (this.context.user.permissions[0].name == "ROLE_admin") ? (//系统管理员页面
            <Flex px='10' mt='50%'>
              <Button onPress={() => navigation.navigate('UserManage')} mb='5' mt='5' size='lg'>用户管理</Button>
              <Button onPress={() => navigation.navigate('SpotManage')} mb='5' mt='5' size='lg'>景区管理</Button>
            </Flex>
          ) : ((this.context.user.permissions[0].name == "ROLE_spot_admin") ? (//景区管理员页面
            <Flex px='10' mt='15%'>
              <Button onPress={() => navigation.navigate('SpotInfoManage')} mb='5' mt='5' size='lg'>景区管理</Button>
              <Button onPress={() => navigation.navigate('ServicePointsManage')} mb='5' mt='5' size='lg'>景点管理</Button>
              <Button onPress={() => navigation.navigate('TourRoutesManage')} mb='5' mt='5' size='lg'>游览路线管理</Button>
              <Button onPress={() => navigation.navigate('ShopsManage')} mb='5' mt='5' size='lg'>商铺管理</Button>
              {/* <Button onPress={() => navigation.navigate('CommentsManage')} mb='5' mt='5' size='lg'>评论管理</Button> */}
            </Flex>
          ) :
            (<Flex px='10' mt='45%'>
              <Button onPress={() => navigation.navigate('EditShop')} mb='5' mt='5' size='lg'>编辑商铺</Button>
              <Button onPress={() => navigation.navigate('AddCommodity')} mb='5' mt='5' size='lg'>上架商品</Button>
              <Button onPress={() => navigation.navigate('CommodityManage')} mb='5' mt='5' size='lg'>管理商品</Button>
            </Flex>
            ))
        }
      </NativeBaseProvider>
    );
  }
}
