import React, { useContext } from 'react'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { AuthContext } from '../context/auth.context'

import Home from './Home/Home.js'
import Iguide from './Iguide/Iguide.js'
import Shopping from './Shopping/Shopping.js'
import My from './My/My.js'

import Management from './Management.js';

export default function BottomTab() {
  const context = useContext(AuthContext);
  const Tab = createBottomTabNavigator();
  return (
    <>
      {
        (!context.user || context.user.permissions[0].name == "ROLE_user") ?
          (// 以下为普通用户/游客分栏
            <Tab.Navigator
              screenOptions={
                ({ route }) => ({
                  tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    if (route.name === 'Home') {
                      iconName = focused ? 'home' : 'home-outline';
                    }
                    else if (route.name === 'Iguide') {
                      iconName = focused ? 'map' : 'map-outline';
                    }
                    else if (route.name === 'Shopping') {
                      iconName = focused ? 'cart' : 'cart-outline';
                    }
                    else if (route.name === 'My') {
                      iconName = focused ? 'person' : 'person-outline';
                    }
                    return <Ionicons name={iconName} size={size} color={color} />;
                  }
                })
              }
            >
              <Tab.Screen name="Home" component={Home} options={{
                tabBarLabel: '首页',
                headerShown: false
              }} />
              <Tab.Screen name="Iguide" component={Iguide} options={{
                tabBarLabel: '导览',
                headerShown: false
              }} />
              <Tab.Screen name="Shopping" component={Shopping} options={{
                tabBarLabel: '购物',
                headerShown: false
              }} />
              <Tab.Screen name="My" component={My} options={{
                tabBarLabel: '我的',
                headerShown: false
              }} />
            </Tab.Navigator>
          ) : (// 以下为系统管理员分栏
            <Tab.Navigator
              screenOptions={
                ({ route }) => ({
                  tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    if (route.name === 'Management') {
                      iconName = focused ? 'build' : 'build-outline';
                    }
                    else if (route.name === 'My') {
                      iconName = focused ? 'person' : 'person-outline';
                    }
                    return <Ionicons name={iconName} size={size} color={color} />;
                  }
                })
              }
            >
              <Tab.Screen name="Management" component={Management} options={{
                tabBarLabel: '管理',
                headerShown: false
              }} />
              <Tab.Screen name="My" component={My} options={{
                tabBarLabel: '我的',
                headerShown: false
              }} />
            </Tab.Navigator>
          )
      }
    </>
  )
}