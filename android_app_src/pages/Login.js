import React, { Component } from 'react'
import {
  TouchableOpacity,
  StyleSheet,
  TextInput,
  View,
  Text,
  Alert,
  Button,
  Image,
} from 'react-native';
import authService from '../service/auth.service';
import { AuthContext } from '../context/auth.context'

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      code: '',
      passwordShow: true,
      key: '',
      image: ' '
    }
  }

  static contextType = AuthContext;

  init = () => {
    this.setState({ username: '' });
    this.setState({ password: '' });
    this.setState({ code: '' });
    this.setState({ passwordShow: false });
  }

  onUserChanged = (newUsername) => {
    this.setState({ username: newUsername });
  }

  onPasswordChanged = (newPassword) => {
    this.setState({ password: newPassword });
  }

  onCodeChanged = (newCode) => {
    this.setState({ code: newCode });
  }

  passwordSwitch = () => {
    if (this.state.passwordShow) {
      this.setState({ passwordShow: false })
    }
    else {
      this.setState({ passwordShow: true })
    }
  }

  login = () => {
    if (this.state.username == '' || this.state.password == '') {
      Alert.alert("登录失败", "用户名或密码为空");
    }
    else if (this.state.code == '') {
      Alert.alert("登录失败", "未输入验证码！");
    }
    else {
      var data = {
        username: this.state.username,
        password: this.state.password,
        key: this.state.key,
        captcha: this.state.code
      }
      authService.signIn(data).then(res => {
        this.refs.username.blur();
        this.refs.password.blur();
        this.refs.code.blur();
        this.init();
        // 加载当前用户信息
        this.context.loadUser(res.content.token);
        const { navigate } = this.props.navigation;
        navigate('Index');
      }).catch((err) => {
        Alert.alert(err.message);
        this.getImage();
      })
    }
  }

  register = () => {
    const { navigate } = this.props.navigation;
    navigate('Register');
  }

  forget = () => {
    const { navigate } = this.props.navigation;
    navigate('Forget');
  }

  getImage() {
    authService.getVerifyImg().then(res => {
      this.setState({ key: res.content.uuid });
      this.setState({ image: res.content.image });
    }
    ).catch((err) => { });
  }

  componentDidMount() {
    this.getImage();
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>登录</Text>
        <View style={styles.infoBox}>
          <TextInput
            ref="username"
            value={this.state.username}
            onChangeText={this.onUserChanged}
            style={styles.infoInput}
            autoCapitalize='none'
            underlineColorAndroid={'transparent'}
            placeholderTextColor={'#bbb'}
            placeholder={'用户名'}
            selectionColor={'#aaa'}
          />
        </View>
        <View style={styles.infoBox}>
          <TextInput
            ref="password"
            value={this.state.password}
            onChangeText={this.onPasswordChanged}
            style={styles.infoInput}
            autoCapitalize='none'
            underlineColorAndroid={'transparent'}
            placeholderTextColor={'#bbb'}
            placeholder={'密码'}
            secureTextEntry={this.state.passwordShow}
          />
        </View>
        <View flexDirection={'row'}>
          <View style={styles.codeBox}>
            <TextInput
              ref="code"
              value={this.state.code}
              onChangeText={this.onCodeChanged}
              style={styles.codeInput}
              autoCapitalize='none'
              underlineColorAndroid={'transparent'}
              placeholderTextColor={'#bbb'}
              placeholder={'验证码'}
              selectionColor={'#aaa'}
            />
          </View>
          <TouchableOpacity onPress={() => { this.getImage(); }}>
            <Image
              source={{ uri: this.state.image }}
              style={{ width: 150, height: 50, marginLeft: 10 }}></Image>
          </TouchableOpacity>
        </View>
        <View flexDirection={'row'} style={{ alignSelf: 'flex-end', marginRight: '20%' }}>
          <TouchableOpacity onPress={this.forget}>
            <Text style={styles.fgText}>忘记密码?</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={this.login}
          style={styles.button}>
          <Text style={styles.lrText}>登录</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.register}
          style={styles.button}>
          <Text style={styles.lrText}>注册</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  infoInput: {
    width: 225,
    height: 40,
    fontSize: 18,
    color: '#000',
  },
  codeInput: {
    width: 125,
    height: 40,
    fontSize: 18,
    color: '#000',
  },
  infoBox: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 280,
    height: 50,
    borderRadius: 8,
    backgroundColor: '#eee',
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 20,
  },
  codeBox: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 120,
    height: 50,
    borderRadius: 8,
    backgroundColor: '#eee',
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 20,
  },
  button: {
    height: 40,
    width: '70%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: '#58812F',
    marginTop: 10,
    marginBottom: 10
  },
  codeButton: {
    height: 50,
    width: 150,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: '#58812F',
    marginLeft: 10,
    fontSize: 18,
  },
  lrText: {
    color: '#fff',
    fontSize: 18,
  },
  fgText: {
    color: '#b0c4de',
    fontSize: 18,
    textAlign: 'right',
    textDecorationLine: 'underline',
  },
  title: {
    color: '#000',
    fontSize: 40,
    marginBottom: 20,
  }
});
