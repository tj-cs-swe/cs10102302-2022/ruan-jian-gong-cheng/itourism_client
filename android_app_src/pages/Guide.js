import { Alert, View } from 'native-base';
import React, { Component } from 'react'
import { WebView } from 'react-native-webview'

export default class Guide extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View w='100%' h='100%'>
        <WebView source={{
          uri: `http://uri.amap.com/navigation?to=${this.props.route.params.longitude},${this.props.route.params.latitude},${this.props.route.params.name}&mode=bus&coordinate=gaode`
        }}
          sharedCookiesEnabled={true}
          startInLoadingState={true}
          onError={() => {
            Alert.alert('网络连接失败！');
          }}
        /></View>
    )
  }
}