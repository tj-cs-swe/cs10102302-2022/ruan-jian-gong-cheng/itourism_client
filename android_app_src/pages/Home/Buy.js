import React, { Component, useState } from 'react'
import { Button, Text, Box, ScrollView, Input, Center, useToast, Heading, VStack, HStack, Toast } from 'native-base'
import { Calendar } from 'react-native-calendars';
import { Alert } from 'react-native'

import spotService from '../../service/spot.service'

export default function Buy(props) {
  const [list, setList] = useState([]);
  const [inputValue, setInputValue] = useState("");
  const [date, setDate] = useState('');
  const [ifChooseDate, setIfChooseDate] = useState(false);
  const toast = useToast();

  const { sid, name } = props.route.params;

  const idNumber = /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;

  const addItem = title => {
    if (!idNumber.test(title)) {
      toast.show({
        title: "请输入合法身份证号",
        status: "warning",
      });
      return;
    } else if (list.length >= 5) {
      toast.show({
        title: "最多购买五张票",
        status: "warning",
      });
      return;
    }

    setList(prevList => {
      return [...prevList, title];
    });
  };

  const handleDelete = index => {
    setList(prevList => {
      const temp = prevList.filter((_, itemI) => itemI !== index);
      return temp;
    });
  };

  const submit = () => {
    if (list.length == 0) {
      toast.show({
        title: "请输入身份证号",
        status: "warning",
      });
      return;
    }

    if(date==''){
      Toast.show({
        title: "请选择日期"
      })
      return;
    }

    var data = {
      spotId: sid,
      amount: list.length,
      idCard: list,
      useTime: date + " 23:59:59"
    }

    // console.log(data);

    spotService.buyTicket(data).then(res => {
      const { navigate } = props.navigation;
      navigate('Pay', { html: res.content.html });
    }).catch(err => {
      Alert.alert(err.message);
    })
  }

  return (
    <ScrollView bgColor='blue.500'>
      <Text fontSize={25} m={5} color={'white'}>当前景区：{name}</Text>
      <Box p={5} bgColor={'white'} borderTopRadius={20}>
        <Box py={4}>
          <Heading mb="2" size="sm">
            请添加身份证号
          </Heading>
          <VStack space={4}>
            <HStack space={2}>
              <Input flex={1} onChangeText={v => setInputValue(v)} value={inputValue} placeholder="添加身份证号" />
              <Button onPress={() => {
                addItem(inputValue);
                setInputValue("");
              }} >添加</Button>
            </HStack>
            <VStack space={2}>
              {list.map((item, itemI) => <HStack w="100%" justifyContent="space-between" alignItems="center" key={item.title + itemI.toString()}>
                <Text width="100%" flexShrink={1} textAlign="left" mx="2" _light={{
                  color: "gray.500"
                }} >
                  {item}
                </Text>
                <Button size="sm" colorScheme="trueGray" onPress={() => handleDelete(itemI)} >删除</Button>
              </HStack>)}
            </VStack>
          </VStack>
        </Box>
        <Box py={4}>
          <Heading mb="2" size="sm">
            请选择日期
          </Heading>
          <Text fontSize={'md'} py={2}>已选日期：{date}</Text>
          <Box py={4}>
            <Calendar
              minDate={(new Date()).toLocaleDateString()}
              onDayPress={date => {
                setDate(date.dateString);
              }}
              monthFormat={'yyyy - MM'}
            />
          </Box>
          <Box>
            <Button align={'center'} borderRadius={'20'} onPress={submit}>购买</Button>
          </Box>
        </Box>
      </Box>
    </ScrollView>


  );
}