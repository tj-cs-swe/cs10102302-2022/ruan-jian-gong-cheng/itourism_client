import React, { Component } from 'react'
import { Dimensions, Alert } from 'react-native'
import { NativeBaseProvider, Modal, Button, Image, Text, Box, AspectRatio, ScrollView, VStack, View, Spinner, Heading, Icon, Pressable, FlatList, Divider, HStack, Toast } from 'native-base'
import { AuthContext } from '../../context/auth.context.js'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Feather from 'react-native-vector-icons/Feather'
import SideMenu from 'react-native-side-menu'
import Buy from './Buy.js'
import spotService from '../../service/spot.service'
import adminService from '../../service/admin.service.js'
import travelService from '../../service/travel.service.js'

const { width, height } = Dimensions.get('window');
var isCleaned = false;

export default class Home extends Component {
  static contextType = AuthContext;
  constructor(props) {
    super(props);
    this.size = 10;
    this._navListener = this.props.navigation.addListener('focus', this.fetchData);
    this._mavListener = this.props.navigation.addListener('blur', this.leave);
    this.state = {
      isLoading: true,
      name: '',
      introduction: '',
      picture: '',
      left: 0,
      price: 0,
      isVisible: false,
      spots: [],
      page: 0,
      ifEnd: false,
      spotScore: 0,
      spotColor: [0, 0, 0, 0, 0],
      userScore: 0,
      userColor: [0, 0, 0, 0, 0],
      ifScore: false,
      existScore: true,
      showDlg: false
    }
  }

  componentDidMount() {
    isCleaned = false;
  }

  fetchData = () => {
    if (!isCleaned) {
      if (!this.state.ifEnd) {
        adminService.getSpotList(this.state.page, this.size).then(res => {
          if (!res.content) {
            this.context.updateIsExist(false);
            this.context.updateSpotid(0);
            this.setState({ ifEnd: true, isLoading: false })
            return;
          }
          var result = [...this.state.spots, ...res.content]

          var now_id = result[0].id
          result.map((spot) => {
            if (spot.id === this.context.spotid) {
              now_id = this.context.spotid
            }
          })

          this.context.updateIsExist(true)
          this.context.updateSpotid(now_id)
          this.setState({ spots: result })

          spotService.getSpotInfo(now_id).then(res => {
            var tmp = [0, 0, 0, 0, 0];
            var score = res.content.score;
            for (var i = 0; i < score; i++) {
              tmp[i] = 1;
            }

            this.setState({
              name: res.content.name,
              introduction: res.content.introduction,
              picture: res.content.figure,
              price: res.content.ticketPrice,
              left: res.content.leftTickets,
              spotScore: res.content.score,
              spotColor: tmp,
              ifScore: res.content.ifScore,
              existScore: res.content.existScore,
              isLoading: false
            })
          }
          ).catch(err => {
            Alert.alert(err.message);
          })
        })

        this.setState({ page: this.state.page + 1 });
      }
    }
  }

  leave = () => {
    this.setState({
      ifEnd: false,
      page: 0,
      spots: [],
      isVisible: false
    });
  }

  componentWillUnmount() {
    isCleaned = true;
    this.setState({ isLoading: true });
  }

  _renderItem = ({ item }) => {
    return (
      <Pressable p={2} bgColor={'white'} onPress={() => {
        this.context.updateSpotid(item.id);
        spotService.getSpotInfo(item.id).then(res => {
          var tmp = [0, 0, 0, 0, 0];
          var score = res.content.score;
          for (var i = 0; i < score; i++) {
            tmp[i] = 1;
          }
          this.setState({
            name: res.content.name,
            introduction: res.content.introduction,
            picture: res.content.figure,
            price: res.content.ticketPrice,
            left: res.content.leftTickets,
            spotScore: res.content.score,
            spotColor: tmp,
            ifScore: res.content.ifScore,
            existScore: res.content.existScore,
            isLoading: false
          })
        }
        ).catch(err => {
          Alert.alert(err);
        })
      }}>
        <HStack justifyContent={'space-between'} alignItems={'center'}>
          <Text fontSize={17} maxW={width * 0.3}>{item.name}</Text>
          <Icon as={AntDesign} name='right' size={4}></Icon>
        </HStack>
      </Pressable>
    )
  }

  _ListHeaderComponent = () => {
    return (
      <View alignItems={'center'} bgColor={'white'}>
        <Text fontSize={22} my={3} fontWeight={'bold'}>切换景区</Text>
        <Divider mb={1} w='90%' alignSelf={'center'}></Divider>
      </View>
    )
  }

  _ItemSeparatorComponent = () => {
    return (
      <View backgroundColor={'white'}>
        <Divider my={1} w='90%' alignSelf={'center'}></Divider>
      </View>
    )
  }

  _onEndReached = () => {
    if (!this.state.ifEnd) {
      adminService.getSpotList(this.state.page, this.size).then(res => {
        if (!res.content) {
          this.setState({ ifEnd: true })
          return;
        }
        var result = [...this.state.spots, ...res.content];
        this.setState({ spots: result });
      })

      this.setState({ page: this.state.page + 1 });
    }
  }

  render() {
    const { navigation } = this.props;
    var list = <FlatList
      data={this.state.spots}
      renderItem={this._renderItem}
      keyExtractor={(item, index) => index}
      ListHeaderComponent={this._ListHeaderComponent}
      ItemSeparatorComponent={this._ItemSeparatorComponent}
      onEndReached={this._onEndReached}
      onEndReachedThreshold={0.1}
    ></FlatList>

    return (
      this.state.isLoading ?
        <View h='100%' alignItems={'center'} justifyContent={'center'}>
          <VStack space={5}>
            <Spinner size={'lg'}></Spinner>
            <Heading color="primary.500" fontSize="lg">
              Loading
            </Heading>
          </VStack>
        </View>
        :
        (this.context.isExist ?
          <SideMenu
            menu={list}
            isOpen={this.state.isVisible}
            openMenuOffset={width * 0.4}
            menuPosition='left'
            edgeHitWidth={width}
            bounceBackOnOverdraw={false}
            autoClosing={false}
          >
            <ScrollView>
              {/* 删除提示窗 */}
              <Modal isOpen={this.state.showDlg}
                onClose={() => { this.setState({ showDlg: false }); }}
                size={'md'}>
                <Modal.Content>
                  <Modal.Header>景区评价</Modal.Header>
                  <Modal.Body>
                    <View>
                      <Text>请选择你的评分</Text>
                      <HStack justifyContent={'center'} alignItems={'center'}>
                        {this.state.userColor.map((color, index) => {
                          return (
                            color ?
                              <Icon key={index} as={AntDesign} name='star' color={'yellow.300'} size={8}
                                onPress={() => {
                                  var result = [0, 0, 0, 0, 0];
                                  for (var i = 0; i <= index; i++) {
                                    result[i] = 1;
                                  }
                                  this.setState({ userColor: result, userScore: index + 1 });
                                }}></Icon>
                              :
                              <Icon key={index} as={AntDesign} name='staro' color={'yellow.300'} size={8}
                                onPress={() => {
                                  var result = [0, 0, 0, 0, 0];
                                  for (var i = 0; i <= index; i++) {
                                    result[i] = 1;
                                  }
                                  this.setState({ userColor: result, userScore: index + 1 });
                                }}></Icon>
                          )
                        })}
                        <Text fontSize='2xl' color={'yellow.300'} ml={1}>{this.state.userScore}</Text>
                      </HStack>
                    </View>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button.Group space={2}>
                      <Button variant="ghost" colorScheme="blueGray" onPress={() => {
                        var result = [0, 0, 0, 0, 0];
                        this.setState({
                          showDlg: false,
                          userScore: 0,
                          userColor: result
                        });
                      }}>取消</Button>
                      <Button onPress={() => {
                        if (this.state.userScore) {
                          travelService.spotScore(this.context.spotid, this.state.userScore).then((res) => {
                            this.setState({
                              showDlg: false,
                              ifScore: true,
                              existScore: true
                            });

                            spotService.getSpotInfo(this.context.spotid).then(res => {
                              var tmp = [0, 0, 0, 0, 0];
                              var score = res.content.score;
                              for (var i = 0; i < score; i++) {
                                tmp[i] = 1;
                              }
                              this.setState({
                                spotScore: res.content.score,
                                spotColor: tmp,
                                ifScore: res.content.ifScore,
                                existScore: res.content.existScore,
                                userColor: [0, 0, 0, 0, 0],
                                userScore: 0
                              })
                            }
                            ).catch(err => {
                              Alert.alert(err);
                            })
                          })
                        }
                        else {
                          Toast.show({
                            title: "评分不能为0"
                          });
                        }
                      }}>确认</Button>
                    </Button.Group>
                  </Modal.Footer>
                </Modal.Content>
              </Modal>

              <View h='100%' backgroundColor={'white'} justifyContent={'space-between'}>
                <Box>
                  <AspectRatio w="100%" ratio={4 / 3}>
                    <Image source={{ uri: this.state.picture }}
                      alt="景区图片" width={'100%'} resizeMode='cover'
                    ></Image>
                  </AspectRatio>
                  <Pressable w={30} h={30} alignSelf={'flex-end'} mr={5} mt={5}
                    justifyContent={'center'}
                    alignItems={'center'}
                    onPress={() => {
                      this.setState({ isVisible: true });
                    }}>
                    {({ isPressed }) => {
                      return <Icon as={Feather} name={'menu'} size={7}
                        opacity={isPressed ? 0.2 : 1.0}></Icon>
                    }}
                  </Pressable>
                  <Box alignItems='center'>
                    <Text fontSize='2xl'
                      fontWeight='bold'
                      mb='3%'>{this.state.name}</Text>
                    <Text fontSize='md' color='gray.500'>{this.state.introduction}</Text>
                  </Box>
                  <Box alignItems='center' my={3}>
                    {this.state.existScore ?

                      <HStack alignItems={'center'}>
                        <Text fontSize='md'>景区评分：</Text>
                        {
                          this.state.spotColor.map((color, index) => (
                            color ?
                              <Icon key={index} as={AntDesign} name='star' color={'yellow.300'} size={6}></Icon>
                              :
                              <Icon key={index} as={AntDesign} name='staro' color={'yellow.300'} size={6}></Icon>
                          ))
                        }
                        <Text fontSize='xl' color={'yellow.300'} ml={1}>{this.state.spotScore}</Text>
                      </HStack>
                      :
                      <HStack alignItems={'center'}>
                        <Text fontSize='md'>景区评分：暂无评分</Text>
                      </HStack>}
                    {!this.state.ifScore ?
                      <Pressable my={3}
                        w={0.25 * width}
                        h={0.08 * width}
                        justifyContent={'center'}
                        alignItems={'center'}
                        borderWidth={2}
                        borderRadius={5}
                        borderColor='gray.400'
                        bgColor={'gray.200'}
                        onPress={() => {
                          if (!this.context.user) {
                            Toast.show({
                              title: "您还没登录"
                            })
                          }
                          else {
                            this.setState({ showDlg: true });
                          }
                        }}>
                        <Text fontSize='md'>我要评分</Text>
                      </Pressable>
                      :
                      null
                    }
                  </Box>
                </Box>
                <Box>
                  <VStack space={3} alignItems={'center'}>
                    <Box alignItems='center'>
                      <Text fontSize={'sm'}>当前余票：{this.state.left}张</Text>
                      <Text fontSize={'sm'}>票价：{this.state.price}元/张</Text>
                    </Box>
                    <Button borderRadius={'20'}
                      w='60%'
                      mb={10}
                      onPress={() => {
                        if (!this.context.user) {
                          navigation.navigate('Login');
                          return;
                        }
                        navigation.navigate('Buy', { sid: this.context.spotid, name: this.state.name })
                      }}>购买门票</Button>
                  </VStack>
                </Box>
              </View >
            </ScrollView>
          </SideMenu >
          :
          <View h='100%' justifyContent={'center'} alignItems={'center'} px='15%'>
            <Text fontSize={35} color='gray.500'>当前不存在景区，请等待管理员添加</Text>
          </View>
        )
    )
  }
}
