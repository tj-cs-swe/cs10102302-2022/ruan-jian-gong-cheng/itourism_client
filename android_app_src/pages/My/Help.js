import React, { Component } from 'react'
import {NativeBaseProvider, Button, Text, Flex, Image, Box, Heading, ScrollView} from 'native-base'

export default class Help extends Component {
  render() {
    return (
        <ScrollView>
            <Box w="95%" alignSelf="center" m="5%" bg="gray.200" rounded="xl">
                <Heading alignSelf="center" m="5%">
                    ITourism
                </Heading>
                <Text fontSize="xl" m="5%">
                    欢迎使用智慧文旅，在这里你可以浏览各大特色景区，在景区内使用导航功能，在各个景点下打卡评论，
                    还能线上购买景区内的各类纪念品。{"\n"}{"\n"}
                    开始享受你的美好旅程吧~
                </Text>
            </Box>
        </ScrollView>
    );
  }
}
