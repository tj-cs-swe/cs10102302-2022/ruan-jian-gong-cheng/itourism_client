import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Input, Pressable, Select, Avatar } from 'native-base'
import { Alert } from 'react-native'
import { AuthContext } from '../../context/auth.context'
import userService from '../../service/user.service'
import { launchImageLibrary } from 'react-native-image-picker'
import travelService from '../../service/travel.service'

export default class ChangeInfo extends Component {
  static contextType = AuthContext;
  constructor(props) {
    super(props);
    this.state = {
      gender: '',
      age: '',
      avatar: ''
    }
  }

  componentDidMount() {
    this.setState({
      gender: this.context.user.gender ? this.context.user.gender : '保密',
      age: this.context.user.age ? this.context.user.age : '保密',
      avatar: this.context.user.avatar
    })
  }

  //点击空白失去焦点
  blurText = () => {
    this.refs.mygender.blur();
    this.refs.myage.blur();
  }

  getgender = (text) => {
    this.setState({ gender: text })
  }
  getage = (text) => {
    this.setState({ age: text })
  }

  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <Pressable flex={1} onPress={this.blurText}>
          <Flex direction='row'>
            <Text mt='16%' ml='10%' fontSize='xl'>头像:   </Text>
            <Avatar source={{
              uri: "http://124.71.194.255:8086/static/image/" + this.state.avatar
            }} size='75' mb='10%' mt='12%' ml='10%' />
            <Button h='25%' mt='16%' ml='15%' onPress={() => {
              launchImageLibrary({
                mediaType: 'photo',
                selectionLimit: 1
              }, resPhoto => {
                if (resPhoto.didCancel) return;
                const formData = new FormData();
                const file = {
                  uri: resPhoto.assets[0].uri,
                  type: resPhoto.assets[0].type,
                  name: resPhoto.assets[0].fileName,
                  size: resPhoto.assets[0].fileSize
                }
                formData.append('file', file);
                var path;
                travelService.uploadFile(formData).then(res => {
                  path = res.content.path;
                  this.setState({ avatar: path });
                  // console.log('头像已变化！路径为：', this.state.avatar)
                })
              })
            }}>上传头像</Button>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>姓名:   </Text>
            <Text mt='5%' mb='5%' fontSize='xl'>{this.context.user.username}</Text>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>性别:   </Text>
            <Select mt='1.5%' minWidth='60%' ref='mygender' defaultValue={this.context.user.gender ? this.context.user.gender : '保密'}
              onValueChange={(text) => this.getgender(text)}>
              <Select.Item label='保密' value='保密' />
              <Select.Item label='男' value='男' />
              <Select.Item label='女' value='女' />
            </Select>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>年龄:   </Text>
            <Input mt='2%' h='75%' w='50%' ref='myage' onChangeText={(text) => this.getage(text)} defaultValue={this.context.user.age ? this.context.user.age.toString() : '保密'}></Input>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>号码:   </Text>
            <Text mt='5%' mb='5%' fontSize='xl'>{this.context.user.phone ? this.context.user.phone : "保密"}</Text>
          </Flex>
          <Flex direction='row' mt='10%' ml='auto' mr='auto'>
            <Button onPress={() => {
              if (! /^[0-9]+$/.test(this.state.age)) {
                alert("年龄格式有误!");
                return;
              }
              if (parseInt(this.state.age) > 150) {
                alert("年龄范围错误!");
                return;
              }
              const data = {
                age: this.state.age,
                gender: this.state.gender,
                avatar: this.state.avatar
              }
              //发送请求，修改数据库
              userService.editProfile(data).then(res => {
                this.context.updateUser({
                  id: this.context.user.id,
                  username: this.context.user.username,
                  email: this.context.user.email,
                  phone: this.context.user.phone,
                  avatar: this.state.avatar,
                  age: this.state.age,
                  gender: this.state.gender,
                  password: this.context.user.password,
                  permissions: this.context.user.permissions,
                  adminSpot: this.context.user.adminSpot
                })
                Alert.alert('修改成功', '返回个人信息页面', [{ text: '确定', onPress: () => { navigation.goBack(); } }])
              }).catch(err => {
                Alert.alert(err.message);
              })
            }} size='lg'> 确认修改 </Button>
          </Flex>
        </Pressable>
      </NativeBaseProvider >
    );
  }
}
