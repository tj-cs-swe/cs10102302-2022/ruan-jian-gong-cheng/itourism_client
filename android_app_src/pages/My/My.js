import React, { Component, useState } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NativeBaseProvider, Box, Button, Flex, Image, Text, VStack, HStack, ScrollView, Avatar } from 'native-base';
import { AuthContext } from '../../context/auth.context'

import MyInfo from './MyInfo.js';
import MyOrder from './MyOrder.js';
import Help from './Help.js';

export default class My extends Component {
  static contextType = AuthContext;
  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        {this.context.user ? (
          <ScrollView px='3'>
            <HStack space={8} py={8} px={5} borderRadius={20} >
              <Avatar source={{
                uri: "http://124.71.194.255:8086/static/image/" + this.context.user.avatar
              }} size='75' />
              <VStack space={2} >
                <Text fontSize='xl'>{this.context.user.username}</Text>
                <Text fontSize='md' color='gray'>景区欢迎您</Text>
              </VStack>
            </HStack>
            <Flex direction='column' px='10%'>
              <Button onPress={() => navigation.navigate('MyOrder')} mb='5%' mt='5%' size='lg'>我的订单</Button>
              <Button onPress={() => navigation.navigate('MyInfo')} mb='5%' mt='5%' size='lg'>个人信息</Button>
              <Button onPress={() => navigation.navigate('Help')} mb='5%' mt='5%' size='lg'>帮助与反馈</Button>
            </Flex>
          </ScrollView>
        ) : (
          <>
            <Text mt='16%' mx='auto' fontSize='2xl'>{'未登录'}</Text>

            <Button mt='20%' mx={5} onPress={() => navigation.navigate('Login')}>点击登录</Button>
          </>
        )

        }
      </NativeBaseProvider>
    )
  }
}
