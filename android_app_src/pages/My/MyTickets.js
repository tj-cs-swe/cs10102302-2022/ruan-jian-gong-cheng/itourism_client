import React, { Component, useState, useEffect, useContext } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Box, Select, HStack } from 'native-base'
import { NavigationContainer } from '@react-navigation/native'
import { Alert } from 'react-native'
import { AuthContext } from '../../context/auth.context'
import spotService from '../../service/spot.service'
import userService from '../../service/user.service'

import TicketInfo from './TicketInfo';
import Commodity from '../Pay'

var isEnd = false;
var isCleaned = false;

function timestampToTime(timestamp) {
  var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
  var Y = date.getFullYear() + '-';
  var M = (date.getMonth()+1 < 10 ? (date.getMonth()+1) : date.getMonth()+1) + '-';
  var D = date.getDate() + ' ';
  var h = date.getHours() + ':';
  var m = date.getMinutes() + ':';
  var s = date.getSeconds();
  return Y+M+D+h+m+s;
}

function TicketsList(props){
  const navigation = props.navi;
  const content = props.content;

  const ticketsList = content.map((ticketInfo) => (
    <Flex key={ticketInfo.id.toString()}>
      <Flex bg='blue.200' direction='row'>
        <Flex position='absolute' ml='5%' pt='5' pb='5'>{ticketInfo.price * ticketInfo.amount}</Flex>
        <Flex position='absolute' ml='15%' pt='5' pb='5'>{timestampToTime(ticketInfo.createTime)}</Flex>
        <Flex position='absolute' ml='50%' pt='5' pb='5'>{
          ticketInfo.orderState == "Paid" ? "已支付" : (
            ticketInfo.orderState == "Pending" ? "待支付" : (
              ticketInfo.orderState == "Finish" ? "已完成" : (
                ticketInfo.orderState == "Refund" ? "已退款" : "待出票"
              )
            )
          )
        }</Flex>
        <Flex ml="70%">
        {
          ticketInfo.orderState == "Paid" ? 
          <Flex direction='row'>
          <Button ml='5%' pt='5' pb='5' onPress={() => navigation.navigate('TicketInfo', {
            id: ticketInfo.id,
            amount: ticketInfo.amount,
            price: ticketInfo.price,
            createTime: ticketInfo.createTime,
            orderState: ticketInfo.orderState,
            spotname: ticketInfo.spot.name
            })}>详情</Button>
            <Button ml='5%' pt='5' pb='5' onPress={() => {
              userService.returnTicket(ticketInfo.id).then(res=>{
                alert('退票成功')
              }).catch(err=>{
                alert(err.message)
              })
            }}>退票</Button>
          </Flex>
          : 
          <Button ml='9%' pt='5' pb='5'
          onPress={() =>
          ticketInfo.orderState == "Paid" ? (
            //完成退票
            userService.returnTicket(ticketInfo.id).then(res=>{
              alert('退票成功')
            }).catch(err=>{
              alert(err.message)
            })
          ):(
            ticketInfo.orderState == "Pending" ? (
              //完成支付
              userService.payOrder(ticketInfo.id).then(res=>{
                navigation.navigate('Pay', { html: res.content.html });
              }).catch(err=>{
                Alert.alert(err.message);
              })
            ) : (
              navigation.navigate('TicketInfo', {
                id: ticketInfo.id,
                amount: ticketInfo.amount,
                price: ticketInfo.price,
                createTime: ticketInfo.createTime,
                orderState: ticketInfo.orderState,
                spotname: ticketInfo.spot.name
                })
            )
          )
        }>{ticketInfo.orderState == "Paid" ? "退票" : (
          ticketInfo.orderState == "Pending" ? "支付" : "详情")}</Button>
        }
        </Flex>
      </Flex>
      <Flex pt='5'></Flex>
    </Flex>
  ));
  return(
    <>{ticketsList}</>
  );
}

export default class MyTickets extends Component {
  constructor(props) {
    super(props);
    this.page = 0;
    this.size = 5;
    this.state = {
      content: []
    }
  }
  static contextType = AuthContext;
  componentDidMount(){
    isCleaned = false;
    userService.getMyTicket(this.page, this.size).then(res => {
      if(res.content){
        isEnd = false;
        if(res.content.length < this.size)
          isEnd = true;
        if(!isCleaned && res.content){
          this.setState({
            content: res.content
          })
        }
      }
    }).catch(err => {
      alert(err.message)
    })
  }

  componentWillUnmount(){
    isCleaned = true;
  }


  render() {
    const { navigation } = this.props;
    const { goBack } = this.props.navigation;

    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Flex ml='3%' pt='5' pb='5'>总价格</Flex>
          <Flex ml='7%' pt='5' pb='5'>购买时间</Flex>
          <Flex ml='14%' pt='5' pb='5'>订单状态</Flex>
          <Flex px='14%' pt='5' pb='5'>操作</Flex>
        </Flex>
        <Flex>
          <TicketsList content={this.state.content} navi={navigation}/>
        </Flex>

        
        <HStack space={5} justifyContent="center" mt='10'>
          <Button size='lg'
            onPress={() => {
              //需要重新发请求获取新一页景点列表
              if(this.page > 0){
                this.page -= 1;
                userService.getMyTicket(this.page, this.size).then(res => {
                  if(res.content){
                    isEnd = false;
                    if(res.content.length < this.size)
                      isEnd = true;
                    if(!isCleaned && res.content){
                      this.setState({
                        content: res.content
                      })
                    }
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >上一页</Button>
          <Button size='lg'
            onPress={() => {
              if(!isEnd){
                this.page += 1;
                userService.getMyTicket(this.page, this.size).then(res => {
                  if(res.content){
                    isEnd = false;
                    if(res.content.length < this.size)
                      isEnd = true;
                    if(!isCleaned && res.content){
                      this.setState({
                        content: res.content
                      })
                    }
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >下一页</Button>
        </HStack>
        
      </NativeBaseProvider>
    );
  }
}
