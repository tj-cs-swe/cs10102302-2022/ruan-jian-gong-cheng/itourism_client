import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Input, Pressable, Select} from 'native-base'
import { Alert } from 'react-native'
import userService from '../../service/user.service'

export default class ChangePwd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldpwd: '',
      newpwd: '',
      cknewpwd: '',
      passwordMsg: '',
    }
  }

  //密码强度正则表达式
  //弱密码：8-12个字符，至少1个大写字母，1个小写字母和1个数字，不包括其他字符
  lowPassword = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,12}$/;
  //中密码：8-12个字符，包含至少一个特殊字符
  midPassword = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,12}$/;
  //强密码：13-16个字符，包含至少一个特殊字符
  highPassword = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{13,16}$/;
  //密码输入提示
  helpPassword = '密码不得少于8位，且需包含大写字母、小写字母和数字';
  levelPassword = [this.helpPassword, '弱密码', '中密码', '强密码'];
  errorPassword = '不得包含非法字符！'

  //点击空白失去焦点
  blurText = () => {
    this.refs.oldpwd.blur();
    this.refs.newpwd.blur();
    this.refs.cknewpwd.blur();
  }

  getOldpwd = (text) => {
    this.setState({ oldpwd: text })
  }

  getNewpwd = (text) => {
    if (this.lowPassword.test(text)) {
      this.setState({ passwordMsg: this.levelPassword[1]})
    }
    else if (this.midPassword.test(text)) {
      this.setState({ passwordMsg: this.levelPassword[2]})
    }
    else if (this.highPassword.test(text)) {
      this.setState({ passwordMsg: this.levelPassword[3]})
    }
    else if (text.search(' ') !== -1 || text.search('\\.') !== -1) {
      this.setState({ passwordMsg: this.errorPassword})
    }
    else {
      this.setState({ passwordMsg: this.levelPassword[0]})
    }
    this.setState({ newpwd: text })
  }

  getckNewpwd = (text) => {
    this.setState({ cknewpwd: text })
  }

  //确认修改密码
  confirmChangePwd = () => {
    if (this.state.newpwd.length === 0 || this.state.oldpwd.length === 0) {
      Alert.alert('修改失败','输入为空！')
      return
    }
    if (this.state.newpwd === this.state.oldpwd) {
      Alert.alert('修改失败','新旧密码一致！')
      return
    }
    if (this.state.newpwd !== this.state.cknewpwd) {
      Alert.alert('修改失败','确认密码不一致！')
      return
    }
    if (this.state.passwordMsg === this.errorPassword || this.state.passwordMsg === this.levelPassword[0]) {
      Alert.alert('修改失败','密码格式有误！')
      return
    }
    if (this.state.passwordMsg === this.levelPassword[1]) {
      Alert.alert('修改失败','请不要使用弱密码！')
      return
    }

    const { navigate } = this.props.navigation;
    //发送请求
    const data = {
      old_password: this.state.oldpwd,
      new_password: this.state.newpwd,
      check_password: this.state.cknewpwd
    }
    userService.changePassword(data).then(res => {
      Alert.alert('修改成功', '返回个人信息页面', [{ text: '确定', onPress: () => { navigate('MyInfo'); } }])
    }).catch(err => {
      Alert.alert(err.message);
    })
  }

  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <Pressable flex={1} onPress={this.blurText}>
          <Flex direction='row' mt='5%'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>原密码:   </Text>
            <Input type='password' ml='5%' mt='2%' h='75%' w='50%' ref='oldpwd' onChangeText={(text)=>this.getOldpwd(text)}></Input>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>新密码:   </Text>
            <Input type='password' ml ='5%' mt='2%' h='75%' w='50%' ref='newpwd' onChangeText={(text)=>this.getNewpwd(text)}></Input>
          </Flex>
          <Flex direction='row' alignSelf="center">
            <Text color='#666'>{this.state.passwordMsg}</Text>
          </Flex>
          <Flex direction='row'>
            <Text mt='5%' mb='5%' ml='10%' fontSize='xl'>再次确认:   </Text>
            <Input type='password' mt='2%' h='75%' w='50%' ref='cknewpwd' onChangeText={(text)=>this.getckNewpwd(text)}></Input>
          </Flex>
          <Flex direction='row' mt='10%' ml='auto' mr='auto'>
            <Button size='lg' onPress={this.confirmChangePwd}> 确认修改 </Button>
          </Flex>
        </Pressable>
      </NativeBaseProvider>
    );
  }
}
