import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Avatar, HStack } from 'native-base'
import { AuthContext } from '../../context/auth.context'

export default class MyInfo extends Component {
  static contextType = AuthContext;
  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Text mt='16%' ml='10%' fontSize='2xl'>头像:</Text>
          <Avatar source={{
            uri: "http://124.71.194.255:8086/static/image/" + this.context.user.avatar
          }}  size='75' mb='10%' mt='12%' ml='10%' />
        </Flex>
        <Flex direction='row'>
          <Text mb='2%' ml='10%' fontSize='2xl'>姓名:  {this.context.user.username}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2%' ml='10%' fontSize='2xl'>性别:  {this.context.user.gender == null ? "保密" : this.context.user.gender}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2%' ml='10%' fontSize='2xl'>年龄:  {this.context.user.age == null ? "保密" : this.context.user.age}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2%' ml='10%' fontSize='2xl'>号码:  {this.context.user.phone == null ? "保密" : this.context.user.phone}</Text>
        </Flex>
        <HStack space={5} justifyContent="center" mt='10'>
          <Button onPress={() => navigation.navigate('ChangeInfo')} size='lg'>修改信息</Button>
          <Button onPress={() => navigation.navigate('ChangePwd')} size='lg'>修改密码</Button>
          <Button onPress={async () => {
            navigation.reset({ index: 0, routes: [{ name: 'Login' },] });
            await this.context.signOutUser();
          }} size='lg'>退出登录</Button>
        </HStack>
      </NativeBaseProvider>
    );
  }
}
