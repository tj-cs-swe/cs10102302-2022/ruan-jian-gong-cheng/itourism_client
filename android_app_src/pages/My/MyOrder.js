import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image } from 'native-base'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'

import MyShopping from './MyShopping.js'
import MyTickets from './MyTickets.js'

export default function MyOrder(){
  const Tab = createMaterialTopTabNavigator();
  return (
    <Tab.Navigator>
      <Tab.Screen name="MyTickets" component={MyTickets} options={{
        tabBarLabel: '已购门票'
      }} />
      <Tab.Screen name="MyShopping" component={MyShopping} options={{
        tabBarLabel: '已购商品'
      }} />
    </Tab.Navigator>
  );
}