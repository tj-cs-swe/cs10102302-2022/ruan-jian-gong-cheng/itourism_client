import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Avatar } from 'native-base'
import { Alert } from 'react-native'

function timestampToTime(timestamp) {
  var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
  var Y = date.getFullYear() + '-';
  var M = (date.getMonth()+1 < 10 ? (date.getMonth()+1) : date.getMonth()+1) + '-';
  var D = date.getDate() + ' ';
  var h = date.getHours() + ':';
  var m = date.getMinutes() + ':';
  var s = date.getSeconds();
  return Y+M+D+h+m+s;
}

export default class TicketInfo extends Component {
  constructor(props) {
    super(props);
    this.ticketId = this.props.route.params.id;
    this.amount = this.props.route.params.amount;
    this.price = this.props.route.params.price;
    this.createTime = this.props.route.params.createTime;
    this.orderState = this.props.route.params.orderState;
    this.spotname = this.props.route.params.spotname;
    // this.state = {
    //   //临时数据
    //   amount: 2,
    //   price: 666,
    //   createTime: 1651739078000,
    //   orderState: 'Paid',
    //   idcards: 
    //   ['111111111111111111',
    //   '222222222222222222'],
    //   spotname: '哈哈哈'
    // }
  }

  render() {
    const { navigation } = this.props;
    //let list = this.state.idcards;
    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>订单id:  {this.ticketId}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>购票数:  {this.amount}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>单价:  {this.price}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>购票时间:  {timestampToTime(this.createTime)}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>订单状态:  {this.orderState == "Paid" ? "已支付" : (
            this.orderState == "Pending" ? "待支付" : (
              this.orderState == "Finish" ? "已过期" : "已退款"
            )
          )}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>景区名称:  {this.spotname}</Text>
        </Flex>
        {/* <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>身份证信息: {"\n"}
            {list.map((idcard)=>{
              return idcard + '\n'
            })}
          </Text>
        </Flex> */}
      </NativeBaseProvider>
    );
  }
}
