import React, { Component } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Avatar } from 'native-base'
import { Alert } from 'react-native'
//等通过票id查信息接口;

function timestampToTime(timestamp) {
  var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
  var Y = date.getFullYear() + '-';
  var M = (date.getMonth()+1 < 10 ? (date.getMonth()+1) : date.getMonth()+1) + '-';
  var D = date.getDate() + ' ';
  var h = date.getHours() + ':';
  var m = date.getMinutes() + ':';
  var s = date.getSeconds();
  return Y+M+D+h+m+s;
}

export default class ShoppingInfo extends Component {
  constructor(props) {
    super(props);
    this.orderId = this.props.route.params.id;
    this.amount = this.props.route.params.amount;
    this.price = this.props.route.params.price;
    this.createTime = this.props.route.params.createTime;
    this.orderState = this.props.route.params.orderState;
    this.ordername = this.props.route.params.ordername;
    // this.state = {
    //   //临时数据
    //   amount: 2,
    //   price: 666,
    //   createTime: 1651739078000,
    //   orderState: 'Paid',
    //   ordername: '哈哈哈'
    // }
  }
/*
  componentDidMount() {
    //通过props的id取到对应的景点信息
    spotAdminService.getServicePointInfo(this.servicePointId).then(res => {
      this.setState({
        name: res.content.name,
        figure: res.content.figure,
        introduce: res.content.introduce,
        position: res.content.position,
        type:res.content.type
      })
    }).catch(err => {
      alert(err.message)
    })
  }
*/
  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>订单id:  {this.orderId}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>数量:  {this.amount}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>单价:  {this.price}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>下单时间:  {timestampToTime(this.createTime)}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>订单状态:  {this.orderState == "Paid" ? "已支付" : (
            this.orderState == "Pending" ? "待支付" : "已退款"
          )}</Text>
        </Flex>
        <Flex direction='row'>
          <Text mb='2' ml='10%' fontSize='2xl'>商品名称:  {this.ordername}</Text>
        </Flex>
      </NativeBaseProvider>
    );
  }
}
