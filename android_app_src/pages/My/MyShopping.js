import React, { Component, useState, useEffect, useContext } from 'react'
import { NativeBaseProvider, Button, Text, Flex, Image, Box, Select, HStack } from 'native-base'
import { NavigationContainer } from '@react-navigation/native'
import { Alert } from 'react-native'
import userService from '../../service/user.service'
import { AuthContext } from '../../context/auth.context'

import ShoppingInfo from './ShoppingInfo';
import shopService from '../../service/shop.service'

var isEnd = false;
var isCleaned = false;

function timestampToTime(timestamp) {
  var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
  var Y = date.getFullYear() + '-';
  var M = (date.getMonth()+1 < 10 ? (date.getMonth()+1) : date.getMonth()+1) + '-';
  var D = date.getDate() + ' ';
  var h = date.getHours() + ':';
  var m = date.getMinutes() + ':';
  var s = date.getSeconds();
  return Y+M+D+h+m+s;
}

function ShoppingList(props){
  const navigation = props.navi;
  const content = props.content;

  const shoppingList = content.map((shoppingInfo) => (
    <Flex key={shoppingInfo.id.toString()}>
      <Flex bg='blue.200' direction='row'>
        <Flex position='absolute' ml='5%' pt='5' pb='5'>{shoppingInfo.price * shoppingInfo.amount}</Flex>
        <Flex position='absolute' ml='15%' pt='5' pb='5'>{timestampToTime(shoppingInfo.createTime)}</Flex>
        <Flex position='absolute' ml='50%' pt='5' pb='5'>{
          shoppingInfo.orderState == "Paid" ? "已支付" : (
            shoppingInfo.orderState == "Pending" ? "待支付" : (
              shoppingInfo.orderState == "Finish" ? "已完成" : (
                shoppingInfo.orderState == "Refund" ? "已退款" : "运输中"
              )
            )
          )
        }</Flex>
        <Flex ml="70%">
        {//Paid两个按钮：退票 和 详情
          shoppingInfo.orderState == "Paid" ? 
          <Flex direction='row'>
            <Button ml='5%' pt='5' pb='5' onPress={() => navigation.navigate('ShoppingInfo', {
              id: shoppingInfo.id,
              amount: shoppingInfo.amount,
              price: shoppingInfo.price,
              createTime: shoppingInfo.createTime,
              orderState: shoppingInfo.orderState,
              ordername: shoppingInfo.commodity.name
            })}>详情</Button>
            <Button ml='5%' pt='5' pb='5' onPress={() => {
              userService.refund(shoppingInfo.id).then(res=>{
                alert('退货成功')
              }).catch(err=>{
                alert(err.message)
              })
            }}>退货</Button>
          </Flex>
          : (shoppingInfo.orderState == "Pending" ? //Pending一个按钮：支付
          <Flex direction='row'>
            <Button ml='10%' pt='5' pb='5' onPress={() => {
              //完成支付
              userService.payOrder(shoppingInfo.id).then(res=>{
                navigation.navigate('Pay', { html: res.content.html });
              }).catch(err=>{
                Alert.alert(err.message);
              })
            }}>支付</Button>
          </Flex> :(shoppingInfo.orderState == "Transiting" ? //Transiting一个按钮：完成
          <Flex direction='row'>
            <Button ml='10%' pt='5' pb='5' onPress={() => {
              userService.finishOrder(shoppingInfo.id).then(res=>{
                alert('已完成订单！')
              }).catch(err=>{
                alert(err.message)
              })
            }}>完成</Button>
          </Flex> : //Refund一个按钮：详情
          <Flex direction='row'>
          <Button ml='10%' pt='5' pb='5' onPress={() => navigation.navigate('ShoppingInfo', {
              id: shoppingInfo.id,
              amount: shoppingInfo.amount,
              price: shoppingInfo.price,
              createTime: shoppingInfo.createTime,
              orderState: shoppingInfo.orderState,
              ordername: shoppingInfo.commodity.name
            })}>详情</Button>
        </Flex>))
        }
        </Flex>
      </Flex>
      <Flex pt='5'></Flex>
    </Flex>
  ));
  return(
    <>{shoppingList}</>
  );
}

export default class MyShopping extends Component {
  constructor(props) {
    super(props);
    this.page = 0;
    this.size = 5;
    this.state = {
      content: []
    }
  }
  static contextType = AuthContext;
  componentDidMount(){
    isCleaned = false;
    userService.getMyOrder(this.page, this.size).then(res => {
      if(res.content){
        isEnd = false;
        if(res.content.length < this.size)
          isEnd = true;
        if(!isCleaned && res.content){
          this.setState({
            content: res.content
          })
        }
      }
    }).catch(err => {
      alert(err.message)
    })
  }

  componentWillUnmount(){
    isCleaned = true;
  }


  render() {
    const { navigation } = this.props;
    const { goBack } = this.props.navigation;

    return (
      <NativeBaseProvider>
        <Flex direction='row'>
          <Flex ml='3%' pt='5' pb='5'>总价格</Flex>
          <Flex ml='7%' pt='5' pb='5'>购买时间</Flex>
          <Flex ml='14%' pt='5' pb='5'>订单状态</Flex>
          <Flex px='22%' pt='5' pb='5'>操作</Flex>
        </Flex>
        <Flex>
          <ShoppingList content={this.state.content} navi={navigation}/>
        </Flex>

        
        <HStack space={5} justifyContent="center" mt='10'>
          <Button size='lg'
            onPress={() => {
              //需要重新发请求获取新一页景点列表
              if(this.page > 0){
                this.page -= 1;
                userService.getMyOrder(this.page, this.size).then(res => {
                  if(res.content){
                    isEnd = false;
                    if(res.content.length < this.size)
                      isEnd = true;
                    if(!isCleaned && res.content){
                      this.setState({
                        content: res.content
                      })
                    }
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >上一页</Button>
          <Button size='lg'
            onPress={() => {
              if(!isEnd){
                this.page += 1;
                userService.getMyOrder(this.page, this.size).then(res => {
                  if(res.content){
                    isEnd = false;
                    if(res.content.length < this.size)
                      isEnd = true;
                    if(!isCleaned && res.content){
                      this.setState({
                        content: res.content
                      })
                    }
                  }
                }).catch(err => {
                  alert(err.message)
                })
              }
            }}
          >下一页</Button>
        </HStack>
        
      </NativeBaseProvider>
    );
  }
}
