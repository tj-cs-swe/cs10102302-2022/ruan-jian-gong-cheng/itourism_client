import { Modal, PermissionsAndroid } from 'react-native'
import React, { useState, useEffect, useContext } from 'react'
import { MapView, MapType, Circle, Marker } from "react-native-amap3d";
import { NativeBaseProvider, Button, Box, View, VStack, Text, HStack, Avatar, Spinner, Heading, Alert } from 'native-base';

import spotService from '../../service/spot.service';
import travelService from '../../service/travel.service';
import { AuthContext } from '../../context/auth.context';

export default function Iguide(props) {
  const context = useContext(AuthContext)
  const [isLoading, setLoading] = useState(true)
  const [isVisible, setIsVisible] = useState(false)
  const [content, setContent] = useState([])
  const [pointid, setPointid] = useState(-1)
  const [nearest, setNearest] = useState([])
  const [pos, setPos] = useState({
    latitude: 30.285,
    longitude: 120.216
  });
  const [spotinfo, setSpotinfo] = useState({});
  const [myLocation, setMyLocation] = useState(null);
  let mapRef = null;

  useEffect(() => {
    const perssionAndInit = async () => {
      const result = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
      ]);
    }
    perssionAndInit();
  }, [])

  useEffect(() => {
    return props.navigation.addListener('focus', fetchData)
  }, [context.spotid])

  const fetchData = () => {
    if (!context.isExist) return

    setLoading(true)
    spotService.getSpotInfo(context.spotid)
      .then(res => {
        var coord = JSON.parse(res.content.position);
        mapRef?.moveCamera({
          target: {
            latitude: coord.latitude,
            longitude: coord.longitude
          },
          zoom: 15
        }, 10);
        setPos({ latitude: coord.latitude, longitude: coord.longitude })
        return fetchServicePoint()
      })
      .then(() => {
        setLoading(false)
      })
      .catch(err => {
        alert(err.message)
      })
  }

  const fetchServicePoint = () => {
    travelService.getServicePointAll(context.spotid)
      .then(res => {
        if (!res.content) {
          setContent([])
          return
        }
        res.content.map((point) => {
          point.position = JSON.parse(point.position)
        })
        setContent(res.content)
      })
      .catch(err => {
        alert(err.message)
      })
  }

  const nearServicePoint = () => {
    if (!content) return;
    if (nearest.length !== 0) {
      setNearest([]);
      return;
    }
    if (!myLocation) {
      alert("正在获取定位...");
      return;
    }
    var servicePointDis = []
    content.map((point) => {
      var coord = point.position;
      var distance = (coord.latitude - myLocation.latitude) * (coord.latitude - myLocation.latitude) +
        (coord.longitude - myLocation.longitude) * (coord.longitude - myLocation.longitude);
      servicePointDis.push({
        dis: distance,
        id: point.id
      })
    });
    servicePointDis.sort((a, b) => {
      return a["dis"] - b["dis"];
    });
    let nearSp = [];
    servicePointDis.slice(0, 4).map((s) => {
      nearSp.push(s.id);
    });
    setNearest(nearSp);
  };

  const refreshSpot = () => {
    mapRef?.moveCamera({
      target: {
        latitude: pos.latitude,
        longitude: pos.longitude
      },
      zoom: 15
    }, 10);
  };

  const point_mark = content.map((point) => (
    <Marker key={point.id.toString()} position={{
      latitude: point.position.latitude,
      longitude: point.position.longitude
    }} icon={(nearest.indexOf(point.id) !== -1) ?
      require("../../assets/images/location100.png") :
      require("../../assets/images/location_normal.png")}
      onPress={() => {
        travelService.getServicePointInfo(point.id).then(res => {
          setIsVisible(true)
          setSpotinfo(res.content)
          setPointid(point.id)
        }).catch(err => {
          alert(err.message)
        })
      }}>
    </Marker>
  ))

  // 正在加载
  if (isLoading) {
    return (
      <View h='100%' alignItems={'center'} justifyContent={'center'}>
        <VStack space={5}>
          <Spinner size={'lg'} />
          <Heading color="primary.500" fontSize="lg">
            Loading
          </Heading>
        </VStack>
      </View>
    )
  }

  // 已经没有景区存在了
  if (!context.isExist) {
    return (
      <View h='100%' justifyContent={'center'} alignItems={'center'} px='15%'>
        <Text fontSize={35} color='gray.500'>当前不存在景区，请等待管理员添加</Text>
      </View>
    )
  }

  return (
    <>
      <MapView
          mapType={MapType.Standard}
          myLocationButtonEnabled
          myLocationEnabled
          onLocation={({ nativeEvent }) => {
            setMyLocation(nativeEvent.coords);
          }}
          ref={(r) => {
            mapRef = r;
          }}
          initialCameraPosition={{
            target: {
              latitude: pos.latitude,
              longitude: pos.longitude,
            },
            zoom: 15
          }}
      >
        <>{point_mark}</>
      </MapView>
      <HStack space={5} justifyContent="center">
        <Button alignSelf={'center'} w='27%' onPress={() => {
          const { navigate } = props.navigation;
          navigate('GetRouteList');
        }}>导览路线</Button>
        <Button alignSelf={'center'} w='27%' onPress={() => {
          refreshSpot()
        }}>定位景区</Button>
        <Button alignSelf={'center'} w='27%' onPress={() => {
          nearServicePoint()
        }}>附近景点</Button>
      </HStack>
      <Modal animationType='slide'
             transparent={true}
             visible={isVisible}>
        <View h='100%' justifyContent={'flex-end'}>
          <View h='50%' backgroundColor={'white'} borderTopRadius={30}>
            <VStack space={2} m='5%'>
              <HStack space={10} justifyContent={'center'}>
                <Avatar source={{
                  uri: spotinfo.figure
                }} size='75' mb='10%' mt='12%' />
                <Text fontSize={30} mt='15%'>{spotinfo.name}</Text>
              </HStack>
              <HStack justifyContent="center">
                <Text fontSize={20}>类型: {spotinfo.type}</Text>
              </HStack>
              <HStack space={5} justifyContent={'center'}>
                <Button w='30%'
                        alignSelf={'center'}
                        onPress={() => {
                          setIsVisible(false);
                          const { navigate } = props.navigation;
                          navigate('ServicePoint', { spid: pointid });
                        }}>景点详情</Button>
                <Button w='30%'
                        alignSelf={'center'}
                        onPress={() => { setIsVisible(false); }}>关闭</Button>
              </HStack>
            </VStack>
          </View>
        </View>
      </Modal>
    </>
  )
}
