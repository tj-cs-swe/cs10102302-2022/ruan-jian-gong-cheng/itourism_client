import React, { Component, useState, useEffect } from 'react'
import { NativeBaseProvider, Button, Text, Box, ScrollView, Avatar, Input, Center, useToast, Heading, VStack, HStack, View } from 'native-base'
import { StyleSheet, Alert, Modal } from 'react-native'
import { MapView, MapType, Marker, Polyline } from "react-native-amap3d";
import travelService from '../../service/travel.service';

export default function GuideRoute(props) {
  const [list, setList] = useState([]);
  const [coordinate, setCoordinate] = useState([]);
  const [mark, setMark] = useState(null);
  const [inputValue, setInputValue] = useState("");
  const [isFound, setIsFound] = useState(false);
  const [isVisible, setIsVisible] = useState(false);
  const [spotinfo, setSpotinfo] = useState({})
  const [coord_id, setCoord_id] = useState([])
  const [spotid, setSpotid] = useState(0)

  const route = props.route.params.route;

  var coord = []
  //var coord_id = []
  var i = 0
  var data = 0;

  useEffect(() => {
    //route:"1;2"
    var spots = route.split(';')
    spots.map((point) => {
      travelService.getServicePointInfo(Number(point)).then(res => {
        coord[i] = JSON.parse(res.content.position);
        //coord_id[i] = {pos: JSON.parse(res.content.position), id: Number(point)}
        var demo_coord_id = coord_id
        demo_coord_id[i] = {pos: JSON.parse(res.content.position), id: Number(point)}
        setCoord_id(demo_coord_id)
        i = i + 1
        if (i == spots.length)
          setCoordinate(coord)
      }).catch(err => {
        alert(err.message)
      })
    })
  }, [])

  const findId = (position) => {
    coord_id.map((point) => {
      if (point.pos.latitude == position.latitude && point.pos.longitude == position.longitude) {
        data = point.id
        setSpotid(data)
      }
    })
  }

  return (
    <>
      <Box p={5} borderTopRadius={20}>
        <Box py={4}>
          <Box>
            <Button align={'center'} borderRadius={'20'} onPress={() => setIsFound(true)}>生成游览路线</Button>
          </Box>
        </Box>
      </Box>

      <NativeBaseProvider>
        <MapView mapType={MapType.Standard}
          initialCameraPosition={{
            target: {
              latitude: 31.284962,
              longitude: 121.216342,
            },
            zoom: 15,
          }}
          onPress={({ nativeEvent }) => {
            // console.log(nativeEvent)
          }}>
          {
            isFound ? <Polyline
              width={10}
              color='rgba(255, 0, 0, 0.5)'
              points={coordinate}></Polyline>
              : null
          }
          {
            coordinate.map((point) => (
              isFound ? <Marker key={point.latitude * 100000} position={{
                latitude: point.latitude,
                longitude: point.longitude
              }} icon={require("../../assets/images/location100.png")}
                onPress={() => {
                  setIsVisible(true)
                  findId({
                    latitude: point.latitude,
                    longitude: point.longitude
                  });
                  travelService.getServicePointInfo(data).then(res => {
                    setSpotinfo(res.content)
                  }).catch(err => {
                    alert(err.message)
                  })
                }}>
              </Marker> : null
            ))
          }
        </MapView>
        <Modal animationType='slide'
          transparent={true}
          visible={isVisible}>
          <View h='100%' justifyContent={'flex-end'}>
            <View h='50%' backgroundColor={'white'} borderTopRadius={30}>
              <VStack space={2} m='5%'>
                <HStack space={10} justifyContent={'center'}>
                  <Avatar source={{
                    uri: spotinfo.figure
                  }} size='75' mb='10%' mt='12%' />
                  <Text fontSize={30} mt='15%'>{spotinfo.name}</Text>
                </HStack>
                <HStack space={5} justifyContent={'center'}>
                  <Button w='30%'
                    alignSelf={'center'}
                    onPress={() => {
                      setIsVisible(false);
                      const { navigate } = props.navigation;
                      navigate('ServicePoint', { spid: spotid });
                    }}>景点详情</Button>
                  <Button w='30%'
                    alignSelf={'center'}
                    onPress={() => { setIsVisible(false); }}>关闭</Button>
                </HStack>
              </VStack>
            </View>
          </View>
        </Modal>
      </NativeBaseProvider>
    </>
  );
}