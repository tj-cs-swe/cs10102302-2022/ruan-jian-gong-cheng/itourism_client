import React, { Component } from 'react'
import { NativeBaseProvider, View, Button, Text, Image, VStack, HStack, ScrollView, AspectRatio, FlatList, Box, Spinner, TextArea, Divider, Avatar, Icon, Toast, Modal } from 'native-base'
import { Dimensions, TouchableOpacity } from "react-native";
import AntDesign from 'react-native-vector-icons/AntDesign'
import { launchImageLibrary } from 'react-native-image-picker'
import DocumentPicker from 'react-native-document-picker'
import TravelService from '../../service/travel.service'
import travelService from '../../service/travel.service';
import { AuthContext } from '../../context/auth.context'
import AudioList from './AudioList';


const { width, height } = Dimensions.get("window");
const rpx = (x) => (width / 750) * x;

var isCleaned = false;

export default class ServicePoint extends Component {
  static contextType = AuthContext;
  constructor(props) {
    super(props);
    this.spid = this.props.route.params.spid;
    this.maxPics = 3;
    this.currentPics = 0;
    this.deleteNum = -1;
    this.state = {
      name: '',
      intro: '',
      figure: '',
      comments: [],
      showBtn: false,
      showDel: false,
      ifEnd: false,
      page: 0,
      size: 5,
      content: '',
      pics: [],
      position: null,
      canPress: false
    }
  }

  componentDidMount() {
    isCleaned = false;
    if (!isCleaned) {
      travelService.getServicePointInfo(this.spid).then(res => {
        this.setState({
          name: res.content.name,
          intro: res.content.introduce,
          figure: res.content.figure,
          position: JSON.parse(res.content.position)
        })
      })

      if (!this.state.ifEnd) {
        travelService.getCommentList(this.spid, this.state.page, this.state.size).then(res => {
          if (!res.content) {
            this.setState({ ifEnd: true })
            return;
          }
          var result = [...this.state.comments, ...res.content];
          this.setState({ comments: result });
        })

        this.setState({ page: this.state.page + 1 });
      }
    }
  }

  componentWillUnmount() {
    isCleaned = true;
  }

  _comment = () => {
    if (!this.context.user) {
      Toast.show({
        title: "您还没登录"
      });
      return;
    }

    if (!this.state.canPress) {
      this.setState({ canPress: true });

      var data = {
        contain: this.state.content,
        pictures: this.state.pics,
        servicePoint: { id: this.spid }
      }

      travelService.addComment(data).then(res => {
        console.log("add success");
        this.setState({
          content: '',
          pics: [],
          comments: [],
          page: 0,
          ifEnd: false,
          canPress: false
        });

        if (!this.state.ifEnd) {
          travelService.getCommentList(this.spid, this.state.page, this.state.size).then(res => {
            if (!res.content) {
              this.setState({ ifEnd: true })
              return;
            }
            var result = [...res.content];
            this.setState({ comments: result });
          })

          this.setState({ page: this.state.page + 1 });
        }

        Toast.show({
          title: "评论成功"
        });
      }).catch(err => {
        console.log(err);
        Toast.show({
          title: "评论失败！"
        });
      })
    }
    else {
      Toast.show({
        title: "点击频率过快，请稍后再试"
      })
    }
  }

  _delete = (id) => {
    travelService.deleteComment(id).then(res => {
      this.setState({
        comments: [],
        page: 0,
        ifEnd: false
      });

      if (!this.state.ifEnd) {
        travelService.getCommentList(this.spid, this.state.page, this.state.size).then(res => {
          if (!res.content) {
            this.setState({ ifEnd: true })
            return;
          }
          var result = [...this.state.comments, ...res.content];
          this.setState({ comments: result });
        })

        this.setState({ page: this.state.page + 1 });
      }

      Toast.show({
        title: "删除成功"
      });
    }).catch(err => {
      console.log(err);
      Toast.show({
        title: "删除失败！"
      });
    })
  }

  _like = (id) => {
    if (!this.context.user) {
      Toast.show({
        title: "您还没登录"
      });
      return;
    }
    travelService.likeComment(id).then(res => {
      var result = [...this.state.comments];
      var tar = result.filter((item) => {
        return item.id == id;
      })
      var index = result.indexOf(tar[0]);
      result[index].ifLike = true;
      result[index].likeNum++;
      this.setState({ comments: result });
    }).catch(err => {
      console.log(err);
      Toast.show({
        title: "点赞失败！"
      });
    })
  }

  _cancelLike = (id) => {
    if (!this.context.user) {
      Toast.show({
        title: "您还没登录"
      });
      return;
    }
    travelService.cancelLikeComment(id).then(res => {
      var result = [...this.state.comments];
      var tar = result.filter((item) => {
        return item.id == id;
      })
      var index = result.indexOf(tar[0]);
      result[index].ifLike = false;
      result[index].likeNum--;
      this.setState({ comments: result });
    }).catch(err => {
      console.log(err);
      Toast.show({
        title: "取消点赞失败！"
      });
    })
  }

  _dislike = (id) => {
    if (!this.context.user) {
      Toast.show({
        title: "您还没登录"
      });
      return;
    }
    travelService.dislikeComment(id).then(res => {
      var result = [...this.state.comments];
      var tar = result.filter((item) => {
        return item.id == id;
      })
      var index = result.indexOf(tar[0]);
      result[index].ifDislike = true;
      this.setState({ comments: result });
    }).catch(err => {
      console.log(err);
      Toast.show({
        title: "点踩失败！"
      });
    })
  }

  _cancelDislike = (id) => {
    if (!this.context.user) {
      Toast.show({
        title: "您还没登录"
      });
      return;
    }
    travelService.cancelDislikeComment(id).then(res => {
      var result = [...this.state.comments];
      var tar = result.filter((item) => {
        return item.id == id;
      })
      var index = result.indexOf(tar[0]);
      result[index].ifDislike = false;
      this.setState({ comments: result });
    }).catch(err => {
      console.log(err);
      Toast.show({
        title: "取消点踩失败！"
      });
    })
  }

  _renderItem = ({ item }) => {
    return (
      <HStack backgroundColor={"white"}
        w='100%'
        px='5%'>
        <VStack w='20%'>
          <View justifyContent={'flex-start'} flex={1} mb={5}>
            <Avatar source={{ uri: "http://124.71.194.255:8086/static/image/" + item.user.avatar }} size='lg'></Avatar>
          </View>
          {(!item.ifLike && !item.ifDislike) ?
            <HStack justifyContent={'center'} alignItems={'flex-end'} flex={1}>
              <TouchableOpacity
                style={{
                  height: 30,
                  width: 30,
                  justifyContent: 'center',
                  alignItems: 'center'
                }} onPress={() => {
                  this._like(item.id);
                }}>
                <Icon as={AntDesign} name='like2' size={5}></Icon>
              </TouchableOpacity>
              <Text fontSize={20} color={'gray.500'} mr={1}>{item.likeNum}</Text>
              <TouchableOpacity
                style={{
                  height: 30,
                  width: 30,
                  justifyContent: 'center',
                  alignItems: 'center'
                }} onPress={() => {
                  this._dislike(item.id);
                }}>
                <Icon as={AntDesign} name='dislike2' size={5}></Icon>
              </TouchableOpacity>
            </HStack>
            : (
              item.ifLike ?
                <HStack justifyContent={'center'} alignItems={'flex-end'} flex={1}>
                  <TouchableOpacity
                    style={{
                      height: 30,
                      width: 30,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }} onPress={() => {
                      this._cancelLike(item.id);
                    }}>
                    <Icon as={AntDesign} name='like1' size={5}></Icon>
                  </TouchableOpacity>
                  <Text fontSize={20} color={'gray.500'}>{item.likeNum}</Text>
                </HStack>
                : (
                  item.ifDislike ?
                    <HStack justifyContent={'center'} alignItems={'flex-end'} flex={1}>
                      <TouchableOpacity
                        style={{
                          height: 30,
                          width: 30,
                          justifyContent: 'center',
                          alignItems: 'center'
                        }} onPress={() => {
                          this._cancelDislike(item.id);
                        }}>
                        <Icon as={AntDesign} name='dislike1' size={5}></Icon>
                      </TouchableOpacity>
                    </HStack> : null
                )
            )
          }
        </VStack >
        <View px='5%' justifyContent={'space-between'}>
          <VStack>
            <HStack alignItems={'center'}>
              <Text fontSize={23} color='blue.400' flex={3}>{item.user.username}</Text>
              <HStack alignItems={'center'} flex={1}>
                {item.top ?
                  <Icon as={AntDesign} name='totop' size={5}></Icon>
                  : null
                }
                {(this.context.user && item.user.id == this.context.user.id) ?
                  <TouchableOpacity
                    style={{
                      height: 30,
                      width: 30,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }} onPress={() => {
                      this.deleteNum = item.id;
                      this.setState({ showDel: true });
                    }}>
                    <Icon as={AntDesign} name='delete' size={5}></Icon>
                  </TouchableOpacity>
                  : null}
              </HStack>
            </HStack>
            <Text fontSize={20} w={0.7 * width} flexGrow={1}>{item.contain}</Text>
            {
              item.pictures ?
                <HStack space={3} mt='5%'>
                  {item.pictures.map((p, index) =>
                    <AspectRatio w='28%' ratio={1 / 1} key={index}>
                      <Image source={{ uri: "http://124.71.194.255:8086/static/image/" + p.path }} alt={'上传图片'}></Image>
                    </AspectRatio>)}
                </HStack> : null
            }
          </VStack>
          <Text fontSize={15} color={'gray.500'} mt={3}>{item.time}</Text>
        </View>
      </HStack >
    )
  }

  _ListHeaderComponent = () => (
    <VStack space={5} backgroundColor={'white'}>
      {this.state.figure.length == 0 ? null :
        <AspectRatio w="100%" ratio={4 / 3} alignSelf={'center'}>
          <Image source={{ uri: this.state.figure }}
            alt="景点图片"></Image>
        </AspectRatio>}
      <HStack>
        <Text fontSize={30} mx='10%'>{this.state.name}</Text>
        <Button width={'40%'} margin="auto" onPress={() => {
          const { navigate } = this.props.navigation;
          navigate('Guide', {
            latitude: this.state.position ? this.state.position.latitude : 0,
            longitude: this.state.position ? this.state.position.longitude : 0,
            name: this.state.name
          });
        }}>到这去</Button>

      </HStack>
      <Text fontSize={20} mx='10%'>景点介绍：{this.state.intro}</Text>
      <Divider mt={8} mb={2} w='90%' alignSelf={'center'} />
      <HStack space={5} justifyContent={'center'}>
        <TextArea fontSize={17}
          placeholder='发一条友善的评论...'
          w={0.5 * width}
          h={0.25 * width}
          borderRadius={5}
          value={this.state.content}
          onChangeText={(text) => {
            this.setState({ content: text })
          }}
          InputRightElement={
            <Icon as={AntDesign} name='picture' size={6} alignSelf={'flex-start'} m='4%'
              onPress={() => {
                if (!this.context.user) {
                  Toast.show({
                    title: "您还没登录"
                  });
                  return;
                }
                else if (this.currentPics >= this.maxPics) {
                  Toast.show({
                    title: "图片不能超过三张"
                  });
                  return;
                }

                launchImageLibrary({
                  mediaType: 'photo',
                  selectionLimit: 1,

                }, resPhoto => {
                  if (resPhoto.didCancel) return;

                  const formData = new FormData();
                  const file = {
                    uri: resPhoto.assets[0].uri,
                    type: resPhoto.assets[0].type,
                    name: resPhoto.assets[0].fileName,
                    size: resPhoto.assets[0].fileSize
                  }
                  formData.append('file', file);
                  var path;

                  travelService.uploadFile(formData).then(res => {
                    this.currentPics++;
                    path = res.content.path;

                    var result = [...this.state.pics];
                    result.push({ path: path });
                    this.setState({ pics: result });
                  })
                })
              }} />
          } />
        <TouchableOpacity style={{
          height: 0.25 * width,
          width: 0.25 * width,
          backgroundColor: '#rgb(0,161,241)',
          borderRadius: 5,
          alignItems: 'center',
          justifyContent: 'center'
        }}
          disabled={this.state.canPress}
          onPress={() => {
            if (this.state.content.length) {
              this._comment();
            }
            else {
              Toast.show({
                title: "请输入评论内容"
              });
            }
          }}>
          <Text color={'white'} fontSize={20}>发  表</Text>
          <Text color={'white'} fontSize={20}>评  论</Text>
        </TouchableOpacity>
      </HStack>
      <HStack space={3} px='5%'>
        {this.state.pics.map((pic, index) =>
          <AspectRatio w='30%' ratio={1 / 1} key={index}>
            <Image alt='评论图片' source={{ uri: "http://124.71.194.255:8086/static/image/" + pic.path }}></Image>
          </AspectRatio>
        )}
      </HStack>
      <Divider mb={4} w='90%' alignSelf={'center'}></Divider>
    </VStack >
  )

  _ListFooterComponent = () => (
    <View backgroundColor={'white'}>
      <Divider w='90%' mt={4} alignSelf={'center'}></Divider>
      {this.state.ifEnd ?
        <Box alignItems={'center'} h={70}>
          <Text fontSize={20}
            color={'grey'}
            alignSelf={'center'}
            m='5%'>没有更多啦</Text>
        </Box>
        :
        <HStack space={2}
          justifyContent={'center'}
          alignItems={'center'}
          h={70}>
          <Spinner accessibilityLabel="Loading posts" />
          <Text color="primary.500" fontSize="lg">
            Loading
          </Text>
        </HStack>}

    </View>
  );

  _ItemSeparatorComponent = () => (
    <View backgroundColor={'white'}>
      <Divider my={4} w='90%' alignSelf={'center'}></Divider>
    </View>
  )

  _onScroll = (e) => {
    this.setState({ showBtn: e.nativeEvent.contentOffset.y > 800 });
  };

  _onEndReached = () => {
    if (!this.state.ifEnd) {
      travelService.getCommentList(this.spid, this.state.page, this.state.size).then(res => {
        if (!res.content) {
          this.setState({ ifEnd: true })
          return;
        }
        var result = [...this.state.comments, ...res.content];
        this.setState({ comments: result });
      })

      this.setState({ page: this.state.page + 1 });
    }
  };

  render() {
    const { navigation } = this.props;
    return (
      <NativeBaseProvider>
        <Button bgColor='#rgb(0,161,241)' onPress={() => navigation.navigate('AudioList', { id: this.spid })}>查看音频列表</Button>
        <FlatList
          data={this.state.comments}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index}
          ListHeaderComponent={this._ListHeaderComponent}
          ListFooterComponent={this._ListFooterComponent}
          ItemSeparatorComponent={this._ItemSeparatorComponent}
          onScroll={this._onScroll}
          ref={(el) => (this.f1 = el)}
          onEndReached={this._onEndReached}
          onEndReachedThreshold={0.1}
        ></FlatList>

        {/* 删除提示窗 */}
        <Modal isOpen={this.state.showDel}
          onClose={() => { this.setState({ showDel: false }); }}
          size={'md'}>
          <Modal.Content maxH="212">
            <Modal.CloseButton />
            <Modal.Header>删除评论</Modal.Header>
            <Modal.Body>
              <ScrollView>
                <Text>确认要删除评论吗？</Text>
              </ScrollView>
            </Modal.Body>
            <Modal.Footer>
              <Button.Group space={2}>
                <Button variant="ghost" colorScheme="blueGray" onPress={() => {
                  this.setState({ showDel: false });
                }}>取消</Button>
                <Button onPress={() => {
                  this.setState({ showDel: false });
                  this._delete(this.deleteNum);
                }}>确认</Button>
              </Button.Group>
            </Modal.Footer>
          </Modal.Content>
        </Modal>

        {/* 回到顶部按钮 */}
        {this.state.showBtn ? (
          <TouchableOpacity
            onPress={() => this.f1.scrollToIndex({ index: 0 })}
            activeOpacity={0.7}
            style={{
              width: rpx(150),
              height: rpx(150),
              position: "absolute",
              bottom: rpx(100),
              right: rpx(50),
              backgroundColor: "gray",
              borderRadius: rpx(75),
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <Text style={{ fontSize: rpx(30), color: "white" }}>回到顶部</Text>
          </TouchableOpacity>
        ) : null}
      </NativeBaseProvider>
    )
  }
}
