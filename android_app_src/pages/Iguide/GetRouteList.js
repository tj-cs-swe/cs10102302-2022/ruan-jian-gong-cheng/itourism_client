import React, { Component } from 'react'
import {
  NativeBaseProvider,
  Text,
  Flex,
  HStack,
  Heading,
  Box,
  Icon,
  IconButton,
  Spinner
} from 'native-base'
import travelService from '../../service/travel.service';
import {Alert, FlatList} from "react-native";
import Entypo from "react-native-vector-icons/Entypo";
import {AuthContext} from "../../context/auth.context";

export default class GetRouteList extends Component {
  static contextType = AuthContext;
  constructor(props) {
    super(props)
    this.size = 5
    this.spot_id = 1
    this.mounted = true
    this.state = {
      page: 0,
      content: [],
    }
  }

  componentDidMount(){
    this.mounted = true
    this.spot_id = this.context.spotid;
    this.fetchData()
  }

  componentWillUnmount(){
    this.mounted = false
    this.setState({ page: 0, content: [], isEnd: false });
  }

  render() {
    return (
      <NativeBaseProvider>
        <FlatList
            data={this.state.content}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index}
            ListFooterComponent={this._ListFooterComponent}
            ref={(el) => (this.f1 = el)}
            onEndReached={this._onEndReached}
            onEndReachedThreshold={0.1}
        />
      </NativeBaseProvider>
    );
  }

  _ListFooterComponent = () => (
      this.state.isEnd ?
          <Text fontSize={20}
                color={'grey'}
                alignSelf={'center'}
                m='5%'>已经到底啦</Text> :
          <HStack space={2} justifyContent="center" my='5%'>
            <Spinner accessibilityLabel="Loading posts" />
            <Text color="primary.500" fontSize="md">
              Loading
            </Text>
          </HStack>
  );

  _renderItem = ({ item }) => {
    return (
      <Box key={item.id.toString()}>
        <Flex pt='5'/>
        <Box alignSelf="center" width="90%" bg="blue.200" p="5" rounded="xl" _text={{
          letterSpacing: "lg"
        }}>
          <Flex direction="row">
            <Heading style={{flex: 10}} size="md">
              {item.name}
            </Heading>
            <IconButton style={{flex: 1}} icon={
              <Icon as={Entypo} name="dots-three-horizontal" size={25}/>}
                    onPress={() => {
                      const { navigation } = this.props;
                      navigation.navigate('GuideRoute', {route: item.route});
                    }}/>
          </Flex>
        </Box>
      </Box>
    );
  }

  _onEndReached = () => {
    if (this.state.isEnd) {
      return
    }
    this.fetchData()
  }

  fetchData = () => {
    travelService.getRouteList(this.spot_id, this.state.page, this.size)
        .then(res => {
            if (!this.mounted) {
                return
            }
            if(!res.content){
                this.setState({ isEnd: true })
                return
            }
            let result = [...this.state.content, ...res.content]
            this.setState({
                content: result,
                isEnd: res.content.length < this.size,
                page: this.state.page + 1})
        })
        .catch(err => {
            Alert.alert(err.message)
        })
  }
}
