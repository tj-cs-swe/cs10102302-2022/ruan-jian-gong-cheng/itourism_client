import React, { Component } from 'react'
import {NativeBaseProvider, Button, Text, Flex, Box, Heading, ScrollView} from 'native-base'
import travelService from '../../service/travel.service';

export default class AudioInfo extends Component {
  constructor(props) {
    super(props);
    this.id = this.props.route.params.id;
    this.state = {
      name: '',
      url: '',
      introduce: ''
    }
  }

  componentDidMount(){
    travelService.getAudio(this.id).then(res => {
      this.setState({
        name: res.content.name,
        url: res.content.url,
        introduce: res.content.introduce
      })
    }).catch(err => {
      alert(err.message)
    })
  }

  render() {
    return (
      <ScrollView>
        <Box w="95%" alignSelf="center" m="5%" bg="gray.200" rounded="xl">
          <Heading alignSelf="center" m="5%">
              { this.state.name }
          </Heading>
          <Text fontSize="20" m="5%">
              介绍：{ this.state.introduce }
          </Text>
        </Box>
      </ScrollView>
    );
  }
}
