import React, { Component } from 'react'
import {
  NativeBaseProvider,
  Flex,
  Box,
  HStack,
  Heading,
  IconButton,
  Icon, Text, Spinner
} from 'native-base'
import { AuthContext } from '../../context/auth.context';
import travelService from '../../service/travel.service';

import Sound from 'react-native-sound';
import Entypo from "react-native-vector-icons/Entypo";
import { Alert, FlatList } from "react-native";

export default class AudioList extends Component {
  constructor(props) {
    super(props);
    this.servicePointId = this.props.route.params.id;
    this.size = 5
    this.mounted = true
    this.state = {
      page: 0,
      content: [],
      isEnd: false
    }
  }
  static contextType = AuthContext;

  componentDidMount() {
    this.mounted = true
    this.fetchData()
  }

  componentWillUnmount() {
    this.mounted = false
    this.setState({ page: 0, content: [], isEnd: false });
  }

  render() {
    return (
      <NativeBaseProvider>
        <FlatList
          data={this.state.content}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index}
          ListFooterComponent={this._ListFooterComponent}
          ref={(el) => (this.f1 = el)}
          onEndReached={this._onEndReached}
          onEndReachedThreshold={0.1}
        />
      </NativeBaseProvider>
    );
  }

  _ListFooterComponent = () => (
    this.state.isEnd ?
      <Text fontSize={20}
        color={'grey'}
        alignSelf={'center'}
        m='5%'>已经到底啦</Text> :
      <HStack space={2} justifyContent="center" my='5%'>
        <Spinner accessibilityLabel="Loading posts" />
        <Text color="primary.500" fontSize="md">
          Loading
        </Text>
      </HStack>
  );

  _renderItem = ({ item }) => {
    return (
      <Box key={item.id.toString()}>
        <Flex pt='5' />
        <Box alignSelf="center" width="90%" bg="blue.200" p="8" rounded="xl" _text={{
          letterSpacing: "lg"
        }}>
          <Flex direction="row">
            <Heading style={{ flex: 10 }} size="md">
              {item.name}
            </Heading>
            <IconButton style={{ flex: 1 }} icon={
              <Icon as={Entypo} name="controller-play" size={30} />}
              onPress={() => {
                const audio = new Sound(('http://124.71.194.255:8086/static/audio/' + item.url), null, (e) => {
                  audio.play(() => audio.release());
                });
              }} />
            <IconButton style={{ flex: 1 }} icon={
              <Icon as={Entypo} name="dots-three-horizontal" size={30} />}
              onPress={() => {
                const { navigation } = this.props;
                navigation.navigate('AudioInfo', { id: item.id })
              }} />
          </Flex>
        </Box>
      </Box>
    )
  }

  _onEndReached = () => {
    if (this.state.isEnd) {
      return
    }
    this.fetchData()
  };

  fetchData = () => {
    travelService.getAudioList(this.servicePointId, this.state.page, this.size)
      .then(res => {
        if (!this.mounted) {
          return
        }
        if (!res.content) {
          this.setState({ isEnd: true })
          return
        }
        let result = [...this.state.content, ...res.content]
        this.setState({
          content: result,
          isEnd: res.content.length < this.size,
          page: this.state.page + 1
        })
      })
      .catch(err => {
        Alert.alert(err.message)
      })
  }
}
