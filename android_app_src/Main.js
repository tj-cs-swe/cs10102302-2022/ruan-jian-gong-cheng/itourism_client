import React, { useContext } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NativeBaseProvider } from 'native-base';
// 用户界面
import BottomTab from './pages/BottomTab'
import Login from './pages/Login';
import Register from './pages/Register';
import Buy from './pages/Home/Buy';
import ServicePoint from './pages/Iguide/ServicePoint';
import Shop from './pages/Shopping/Shop'
import Commodity from './pages/Shopping/Commodity';
import Pay from './pages/Pay'
import Guide from './pages/Guide';
import Forget from './pages/Forget';
import MyInfo from './pages/My/MyInfo';
import MyOrder from './pages/My/MyOrder';
import MyStep from './pages/My/MyStep';
import Help from './pages/My/Help';
import ChangeInfo from './pages/My/ChangeInfo';
import ChangePwd from './pages/My/ChangePwd';
import TicketInfo from './pages/My/TicketInfo';
import ShoppingInfo from './pages/My/ShoppingInfo';
import AudioList from './pages/Iguide/AudioList';
import AudioInfo from './pages/Iguide/AudioInfo';
import GetRouteList from './pages/Iguide/GetRouteList';
import GuideRoute from './pages/Iguide/GuideRoute';
// 系统管理员界面
import UserManage from './pages/admin/UserManage';
import SpotManage from './pages/admin/SpotManage';
import AddSpot from './pages/admin/AddSpot';
// 景区管理员界面
import SpotInfoManage from './pages/spotadmin/SpotManage/SpotInfoManage';
import ServicePointsManage from './pages/spotadmin/ServicePointManage/ServicePointsManage';
import TourRoutesManage from './pages/spotadmin/TourRoutesManage/TourRoutesManage';
import ShopsManage from './pages/spotadmin/ShopsManage/ShopsManage';
import CommentsManage from './pages/spotadmin/CommentsManage/CommentsManage';
import ChangeSpotInfo from './pages/spotadmin/SpotManage/ChangeSpotInfo';
import AddServicePoint from './pages/spotadmin/ServicePointManage/AddServicePoint';
import ServicePointInfo from './pages/spotadmin/ServicePointManage/ServicePointInfo';
import ChangeServicePointInfo from './pages/spotadmin/ServicePointManage/ChangeServicePointInfo';
import AudioManage from './pages/spotadmin/ServicePointManage/AudioManage';
import AddShop from './pages/spotadmin/ShopsManage/AddShop';
import CommentInfo from './pages/spotadmin/CommentsManage/CommentInfo';
import AddAudio from './pages/spotadmin/ServicePointManage/AddAudio';
import ChangeAudio from './pages/spotadmin/ServicePointManage/ChangeAudio';
import AddTourRoute from './pages/spotadmin/TourRoutesManage/AddTourRoute';
import ChangeTourRoute from './pages/spotadmin/TourRoutesManage/ChangeTourRoute';
// 商家界面
import AddCommodity from './pages/Seller/AddCommodity';
import CommodityManage from './pages/Seller/CommodityManage';
import EditCommodity from './pages/Seller/EditCommodity';
import EditShop from './pages/Seller/EditShop';

import { AuthContext } from './context/auth.context';
import Loading from './pages/Loading';

import { AMapSdk } from 'react-native-amap3d'
AMapSdk.init('7db77c12ff0077cdd7911c3b1d23c5ae');

const Stack = createNativeStackNavigator();

export default function Main() {

  const { loading } = useContext(AuthContext);

  return (
    <NativeBaseProvider>
      {loading ? <Loading></Loading> : (
        <NavigationContainer>
          <Stack.Navigator initialRouteName='Index'>
            <Stack.Screen name='Login' component={Login} options={{
              headerShown: false
            }} />
            <Stack.Screen name='Register' component={Register} options={{
              headerShown: false
            }} />
            <Stack.Screen name='Forget' component={Forget} options={{
              headerShown: false
            }} />
            <Stack.Screen name='Index' component={BottomTab} options={{
              headerShown: false
            }} />
            <Stack.Screen name='Buy' component={Buy} options={{
              headerTitle: '填写购票信息'
            }} />
            <Stack.Screen name='Shop' component={Shop} options={{
              headerTitle: '店铺详情'
            }} />
            <Stack.Screen name='Commodity' component={Commodity} options={{
              headerTitle: '商品详情'
            }} />
            <Stack.Screen name='Pay' component={Pay} options={{
              headerTitle: '支付页'
            }} />
            <Stack.Screen name='Guide' component={Guide} options={{
              headerShown: false
            }} />
            <Stack.Screen name='ServicePoint' component={ServicePoint} options={{
              headerTitle: '景点详情'
            }} />
            <Stack.Screen name='MyInfo' component={MyInfo} options={{
              headerTitle: '个人信息'
            }} />
            <Stack.Screen name='MyOrder' component={MyOrder} options={{
              headerTitle: '我的订单'
            }} />
            <Stack.Screen name='MyStep' component={MyStep} options={{
              headerTitle: '我的足迹'
            }} />
            <Stack.Screen name='Help' component={Help} options={{
              headerTitle: '帮助与反馈'
            }} />
            <Stack.Screen name='ChangeInfo' component={ChangeInfo} options={{
              headerTitle: '修改信息'
            }} />
            <Stack.Screen name='ChangePwd' component={ChangePwd} options={{
              headerTitle: '修改密码'
            }} />
            <Stack.Screen name='TicketInfo' component={TicketInfo} options={{
              headerTitle: '票务信息'
            }} />
            <Stack.Screen name='ShoppingInfo' component={ShoppingInfo} options={{
              headerTitle: '商品信息'
            }} />
            <Stack.Screen name='AudioList' component={AudioList} options={{
              headerTitle: '音频列表'
            }} />
            <Stack.Screen name='AudioInfo' component={AudioInfo} options={{
              headerTitle: '音频详情'
            }} />
            <Stack.Screen name='GetRouteList' component={GetRouteList} options={{
              headerTitle: '导览路线'
            }} />
            <Stack.Screen name='GuideRoute' component={GuideRoute} options={{
              headerTitle: '路线导航'
            }} />
            {/* 以下为系统管理员页面 */}
            <Stack.Screen name='UserManage' component={UserManage} options={{
              headerTitle: '用户管理'
            }} />
            <Stack.Screen name='SpotManage' component={SpotManage} options={{
              headerTitle: '景区管理'
            }} />
            <Stack.Screen name='AddSpot' component={AddSpot} options={{
              headerTitle: '添加新景区'
            }} />
            {/* 以下为景区管理员页面 */}
            <Stack.Screen name='SpotInfoManage' component={SpotInfoManage} options={{
              headerTitle: '景区管理'
            }} />
            <Stack.Screen name='ServicePointsManage' component={ServicePointsManage} options={{
              headerTitle: '景点管理'
            }} />
            <Stack.Screen name='TourRoutesManage' component={TourRoutesManage} options={{
              headerTitle: '游览路线管理'
            }} />
            <Stack.Screen name='ShopsManage' component={ShopsManage} options={{
              headerTitle: '商铺管理'
            }} />
            <Stack.Screen name='CommentsManage' component={CommentsManage} options={{
              headerTitle: '评论管理'
            }} />
            <Stack.Screen name='ChangeSpotInfo' component={ChangeSpotInfo} options={{
              headerTitle: '修改景区信息'
            }} />
            <Stack.Screen name='AddServicePoint' component={AddServicePoint} options={{
              headerTitle: '新增景点'
            }} />
            <Stack.Screen name='ServicePointInfo' component={ServicePointInfo} options={{
              headerTitle: '景点信息'
            }} />
            <Stack.Screen name='ChangeServicePointInfo' component={ChangeServicePointInfo} options={{
              headerTitle: '修改景点信息'
            }} />
            <Stack.Screen name='AudioManage' component={AudioManage} options={{
              headerTitle: '音频管理'
            }} />
            <Stack.Screen name='AddShop' component={AddShop} options={{
              headerTitle: '新增商铺'
            }} />
            <Stack.Screen name='CommentInfo' component={CommentInfo} options={{
              headerTitle: '评论详情'
            }} />
            <Stack.Screen name='AddAudio' component={AddAudio} options={{
              headerTitle: '添加音频'
            }} />
            <Stack.Screen name='ChangeAudio' component={ChangeAudio} options={{
              headerTitle: '修改音频'
            }} />
            <Stack.Screen name='AddTourRoute' component={AddTourRoute} options={{
              headerTitle: '添加游览路线'
            }} />
            <Stack.Screen name='ChangeTourRoute' component={ChangeTourRoute} options={{
              headerTitle: '修改游览路线'
            }} />
            {/* 以下为商家页面 */}
            <Stack.Screen name='EditShop' component={EditShop} options={{
              headerTitle: '编辑商铺信息'
            }} />
            <Stack.Screen name='AddCommodity' component={AddCommodity} options={{
              headerTitle: '填写商品信息'
            }} />
            <Stack.Screen name='CommodityManage' component={CommodityManage} options={{
              headerTitle: '商品管理'
            }} />
            <Stack.Screen name='EditCommodity' component={EditCommodity} options={{
              headerTitle: '编辑商品'
            }} />
          </Stack.Navigator>
        </NavigationContainer>
      )}
    </NativeBaseProvider>
  )
}
