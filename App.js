import React, { useContext } from 'react';
import type { Node } from 'react';

import Main from "./android_app_src/Main"
import {AuthContextProvider } from './android_app_src/context/auth.context';

const App: () => Node = () => {
  return (
    <AuthContextProvider>
      <Main></Main>
    </AuthContextProvider>
  );
};

export default App;
